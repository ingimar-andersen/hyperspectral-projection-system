function saveArrayAsMat(array, outputFilepath)
    if ~endsWith(outputFilepath, '.mat')
       outputFilepath = strcat(outputFilepath, '.mat'); 
    end
    save(outputFilepath, 'array');
end