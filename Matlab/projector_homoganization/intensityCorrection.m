function [resultIm,XYused] = intensityCorrection(imLeft,imRight,bitDepth,lossProcentage,autoModeOn,width,height,oldXY)

% 2018-11-01 - TKK
%
% Caculates a normalized intesity correction image to homogonize the
% projector output, based on a Left and Right image aquired on a white
% surface.
%
% imLeft,imRight  , Grayscale images
% lossProcentage [0 1]  ~0..3 = 30percent loss accepted
% resultIm [0 1]


% LOAD a left and right image taken on white background
im{1} = imLeft;
im{2} = imRight;

%%
XYused = zeros(8,2);
% Specify the desired and used DMD resuloution%
%width = 912;
%height = 1140;

% Warp L and R image, based on four corner selections (TopLeft,TopRight,BotLeft,BotRight)
for qq=1:2
     imtemp = im{qq};
    
     if (exist('oldXY','var') == 1)
         xf = oldXY(qq*4-3:qq*4,1);
         yf = oldXY(qq*4-3:qq*4,2);         
     else
       
         if autoModeOn == 0
            % MANUAL MODE --- 
            figure(1)
            imshow(imtemp)
            hold on
            text(size(imtemp,2)/2,size(imtemp,1)/2,'Click the four corners in the order:','Color','white','FontSize',15,'HorizontalAlignment','center')
            text(size(imtemp,2)/2,size(imtemp,1)/2+150,'TopLeft   TopRight   BotRight   BotLeft','Color','white','FontSize',15,'HorizontalAlignment','center')
            
            ylim([1 size(imtemp,1)])
            xlim([1 size(imtemp,2)])
            axis equal
            
            [x,y] = ginputred(4);

            xp = zeros(4,1);
            yp = zeros(4,1);
            wiev = 25;
            close 1
            for ii=1:4
                figure(2)
                imagesc(imtemp(round(y(ii))-wiev:round(y(ii))+wiev,round(x(ii))-wiev:round(x(ii))+wiev,:))
                [xp(ii),yp(ii)] = ginputred(1);
            end
            close 2

            xf = x+xp-wiev;
            yf = y+yp-wiev;
            % MANUAL MODE --- 

         else 

            % % AUTO MODE --- 

            BW = imtemp > 15;

            ime = edge(BW,'Canny');

            [H, T, R] = hough(ime);

            cord = houghpeaks(H,4,'Threshold',0.2*max(H(:)));

            figure(qq+10);
            imshow(imtemp)
            hold on
            [row,col] = size(imtemp);
            l = zeros(3,4);
            for i=1:size(cord,1)
               l(1,i) = cos(deg2rad(T(cord(i,2))));
               l(2,i) = sin(deg2rad(T(cord(i,2))));
               l(3,i) = -R(cord(i,1));
               DrawImageLine(row,col,l(:,i)');
            end

            coreslost= [1,1,1,2,2,3;
                        2,3,4,3,4,4];

            calCord = zeros(2,6);
            for iii=1:6
                cossing = cross(l(:,coreslost(1,iii)),l(:,coreslost(2,iii)));
                cossing = cossing/cossing(3);
                if cossing(1) < size(imtemp,2) && cossing(2) < size(imtemp,1)
                    calCord(:,iii) = cossing(1:2);
                end
            end


            calCord = calCord(:,calCord(1,:) > 0);
            hold on
            scatter(calCord(1,:),calCord(2,:))


            [~,ysort] = sort(calCord(2,:));
            calCord = calCord(:,ysort);


            [~,topSort] = sort(calCord(1,1:2));
            calCord(:,1:2) = calCord(:,topSort);

            [~,botSort] = sort(calCord(1,3:4));
            calCord(:,3:4) = calCord(:,botSort+2);


            x = calCord(1,:);
            y = calCord(2,:);

            xp = zeros(4,1);
            yp = zeros(4,1);
            wiev = 25;

            for ii=1:4
                figure(2)
                imagesc(imtemp(round(y(ii))-wiev:round(y(ii))+wiev,round(x(ii))-wiev:round(x(ii))+wiev,:))
                [xp(ii),yp(ii)] = ginputred(1);
            end


            xf = x+xp'-wiev;
            yf = y+yp'-wiev;

            % % % AUTO MODE --- 

         end
         
     end
    
     
    widthDouble = double(width);
    heightDouble = double(height);
    myH = homography(xf(1),yf(1),xf(2),yf(2),xf(3),yf(3),xf(4),yf(4),0,0,widthDouble,0,widthDouble,heightDouble,0,heightDouble);
    tform = projective2d(myH);
    imWarp{qq} = imwarp(imtemp,tform,'OutputView',imref2d([height,width]));
    
    XYused(qq*4-3:qq*4,1) = xf;
    XYused(qq*4-3:qq*4,2) = yf;
end
%%
% Show L Warp for inspection
figure(1)
imagesc(imWarp{2})
axis equal

% Calculate the avarage of Rwarp and Lwarp
figure(2)
imAvg = mean(cat(3,imWarp{1},imWarp{2}),3);

%(imWarp{1}+imWarp{2})/2;
imagesc(imAvg)
imageMaxValue = 2^bitDepth;

% Inspect the intesity histogram, to set new image max
bin = 1:1:imageMaxValue;
figure(3)
%histogram(imAvg,imageMaxValue)
histogram(imAvg,bin)
xlim([1 imageMaxValue])
[histPoint,~] = ginputred(1);

imAvg(imAvg>histPoint) = histPoint;
imAvg(imAvg<30) = 30;
% Show normalized image
myMax = max(max(imAvg));
imNorm = double(imAvg)/double(myMax);
figure(4)
imagesc(imNorm)
set(gcf,'color','w');
colorbar


% Calculate image intensity correction 
% corMax = 1/(1-lossProcentage); % Specify the alloed intesity loss
% imCor = ones(size(imNorm))./imNorm;
% imCor = imgaussfilt(imCor,1);
% imCor(imCor>corMax) = 0;
% imCor = imCor./corMax;
% figure(4)
% imagesc(imCor)
% colorbar��



corMax = (1-lossProcentage); % Specify the alloed intesity loss
imCor = ones(size(imNorm))-imNorm+corMax;

imCor = imgaussfilt(imCor,1);
imCor(imCor>1) = 1;

%imCor(imNorm<corMax) = 0;

figure(5)
imagesc(imCor)
colorbar
set(gcf,'color','w');

% Show result and apply cropping mask 
%figure(5)
%imagesc(double(imAvg).*imCor)
%[x,y] = ginputred(4);
%mask = double(poly2mask(x,y,size(imCor,1),size(imCor,2)));

resultIm = imCor;
%resultIm = imCor.*mask;
%imagesc(resultIm)
end
