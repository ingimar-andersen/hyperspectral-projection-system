function [homoImage,usedXY] = getHomoImage(leftImPath,rightImPath,paramPath,lossPercentage,autoModeEnable,width, height, oldXY)

% LOAD a left and right image taken on white background
%imLeft = rgb2gray(imread(leftImPath));
%imRight = rgb2gray(imread(rightImPath));
imLeft = imread(leftImPath);
imRight = imread(rightImPath);

info = imfinfo(leftImPath);
bitDepth = info.BitDepth;

% OLD METHOD USING STEREO PARAMS FROM MATLAB
% Load camera stereo calibration parametes
%load(paramPath);
% Correct for radial distortion 
% imLeft = undistortImage(imLeft,stereoParams.CameraParameters1);
% imRight = undistortImage(imRight,stereoParams.CameraParameters2);


% Load camera stereo calibration parametes to use for distorton correction
[~, ~, p1, p2, distCoeffs1,distCoeffs2]  = loadCalibrationParametersFromFolder(paramPath);

cameraParams1 = cameraParameters('IntrinsicMatrix',p1(:,1:3)','RadialDistortion',distCoeffs1(1:2),'TangentialDistortion',[0 0]);
cameraParams2 = cameraParameters('IntrinsicMatrix',p2(:,1:3)','RadialDistortion',distCoeffs2(1:2),'TangentialDistortion',[0 0]);


% Correct for radial distortion 
imLeft = undistortImage(imLeft,cameraParams1);
imRight = undistortImage(imRight,cameraParams2);


 if (exist('oldXY','var') == 1)
    [homoImage,usedXY] = intensityCorrection(imLeft,imRight,bitDepth,lossPercentage,autoModeEnable, width, height, oldXY);      
 else
    [homoImage,usedXY] = intensityCorrection(imLeft,imRight,bitDepth,lossPercentage,autoModeEnable, width, height);
 end

close all
end