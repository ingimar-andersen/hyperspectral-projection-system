function [Cross]=CrossOp(a)

if ~isvector(a)
    error('Inpu must be vector')
end

Cross=[ 0 -a(3) a(2);
        a(3) 0 -a(1);
        -a(2) a(1) 0];
