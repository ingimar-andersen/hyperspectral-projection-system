function [homoImage, transformL_out, transformR_out] = getHomoImage_checkerboard(leftImPath,leftCheckerPath,rightImPath,rightCheckerPath,checkerboardPatternPath,paramPath,lossPercentage, showFigures, maxPercentile, transformL, transformR)

% LOAD a left and right image taken on white background
imLeft = imread(leftImPath);
imRight = imread(rightImPath);

info = imfinfo(leftImPath);
bitDepth = info.BitDepth;

% LOAD a left and right image taken on white background with a checkerborad
% projected
checkerboardLeft = imread(leftCheckerPath);
checkerboardRight = imread(rightCheckerPath);

if size(imLeft,3) == 3
    imLeft = rgb2gray(imLeft);
    imRight = rgb2gray(imRight);
end

if size(checkerboardLeft) == 3
    checkerboardLeft = rgb2gray(checkerboardLeft);
    checkerboardRight = rgb2gray(checkerboardRight);   
end

% LOAD the pattern used for the checkerborad projection
checkerboardPattern = imread(checkerboardPatternPath);

% Load camera stereo calibration parametes to use for distorton correction
[~, ~, p1, p2, distCoeffs1,distCoeffs2]  = loadCalibrationParametersFromFolder(paramPath);

cameraParams1 = cameraParameters('IntrinsicMatrix',p1(:,1:3)','RadialDistortion',distCoeffs1(1:2),'TangentialDistortion',distCoeffs1(3:4));
cameraParams2 = cameraParameters('IntrinsicMatrix',p2(:,1:3)','RadialDistortion',distCoeffs2(1:2),'TangentialDistortion',distCoeffs1(3:4));


% Correct for radial distortion 
imLeft = undistortImage(imLeft,cameraParams1);
imRight = undistortImage(imRight,cameraParams2);
checkerboardLeft = undistortImage(checkerboardLeft,cameraParams1);
checkerboardRight = undistortImage(checkerboardRight,cameraParams2);

if (~exist('transformationL','var') || ~exist('transformR','var'))
    [homoImage, transformL_out, transformR_out] = intensityCorrection_checkerboard(imLeft,checkerboardLeft,imRight,checkerboardRight,checkerboardPattern,bitDepth,lossPercentage, maxPercentile, showFigures);      
else
    [homoImage, transformL_out, transformR_out] = intensityCorrection_checkerboard(imLeft,checkerboardLeft,imRight,checkerboardRight,checkerboardPattern,bitDepth,lossPercentage, maxPercentile, showFigures, transformL, transformR);      
end
close all

end