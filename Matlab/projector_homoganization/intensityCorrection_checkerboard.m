
function [resultIm, transformL_out, transformR_out] = intensityCorrection_checkerboard(imLeft,checkerboardLeft,imRight,checkerboardRight,checkerboardpattern,bitDepth,lossProcentage,maxPercentile, showFigures, transformL, transformR)

% 2018-11-01 - TKK
%
% Caculates a normalized intesity correction image to homogonize the
% projector output, based on a Left and Right image aquired on a white
% surface.
%
% imLeft,imRight  , Grayscale images
% lossProcentage [0 1]  ~0..3 = 30percent loss accepted
% resultIm [0 1]


% LOAD a left and right image taken on white background
im{1} = imLeft;
im{2} = imRight;
im_checkerboard{1} = checkerboardLeft;
im_checkerboard{2} = checkerboardRight;
%%

% Find checkerboards if transformation is not given (intended for faster
% recursive homogenization)
if (~exist('transformL','var') || ~exist('transformR','var'))
    [PatternimagePoints,boardSize_pattern] = detectCheckerboardPoints(checkerboardpattern);

    % Warp L and R image, based on four corner selections (TopLeft,TopRight,BotLeft,BotRight)
    for qq=1:2
        imtemp = im{qq};
        checkerboard = im_checkerboard{qq};

        [imagePoints,boardSize] = detectCheckerboardPoints(checkerboard);

        if boardSize ~= boardSize_pattern
           error('Checkerboard size mismatch between projected image and acquired image') 
        end
        myH = hest(PatternimagePoints',imagePoints');
         %Output transforms
        if qq == 1
            transformL_out = myH;
        elseif qq ==2
            transformR_out = myH;
        end
        
        tform = projective2d(myH');

        imWarp{qq} = imwarp(imtemp,tform,'OutputView',imref2d([size(checkerboardpattern,1),size(checkerboardpattern,2)]));

    end
else
    transformL_out = projective2d(transformL);
    transformR_out = projective2d(transformR);
    imWarp{1} = imwarp(im{1},transformL,'OutputView',imref2d([size(checkerboardpattern,1),size(checkerboardpattern,2)]));
    imWarp{2} = imwarp(im{2},transformR,'OutputView',imref2d([size(checkerboardpattern,1),size(checkerboardpattern,2)]));
end

%%

imAvg = mean(cat(3,imWarp{1},imWarp{2}),3);

if showFigures == 1
    
    figure('Name','Checkerboard pattern, with detected corners')
    imagesc(checkerboardpattern)
    hold on
    scatter(PatternimagePoints(:,1),PatternimagePoints(:,2))

    figure('Name','Checkerboard (R), with detected corners')
    imagesc(checkerboardRight)
    hold on
    scatter(imagePoints(:,1),imagePoints(:,2))

    % Show L Warp for inspection
    figure('Name','Warped image')
    imagesc(imWarp{1})
    axis equal

    % Calculate the avarage of Rwarp and Lwarp
    figure('Name','Average warp')   
    ylim([0 size(imAvg, 2)])
    xlim([0 size(imAvg, 1)])
    imagesc(imAvg)

end

imageMaxValue = 2^bitDepth;

% Inspect the intesity histogram, to set new image max
bin = 1:1:imageMaxValue;
if showFigures == 1
    figure(3)
    histogram(imAvg,bin)
    xlim([1 imageMaxValue])
end

if maxPercentile < 0
    [maxPercentileValue,~] = ginputred(1);
elseif (0 < maxPercentile) && (maxPercentile <= 100)
    maxPercentileValue = prctile(imAvg, maxPercentile, 'all');
else
   error('Invalid maxPercentile value. Should be in the range 0-100. Negative value prompts the user to select from a histogram.') 
end
%msgbox(num2str(maxPercentileValue));

imAvg(imAvg>maxPercentileValue) = maxPercentileValue;
imAvg(imAvg<30) = 30;


% Show normalized image
myMax = max(max(imAvg));
imNorm = double(imAvg)/double(myMax);

if showFigures == 1
    figure('Name','Normalized image')
    imagesc(imNorm)
    set(gcf,'color','w');
    colorbar
end


corMax = (1-lossProcentage); % Specify the alloed intesity loss
imCor = ones(size(imNorm))-imNorm+corMax;

imCor = imgaussfilt(imCor,1);
imCor(imCor>1) = 1;



if showFigures == 1
    figure('Name','Corrected image')
    imagesc(imCor)
    colorbar
    set(gcf,'color','w');
end

resultIm = imCor;
end
