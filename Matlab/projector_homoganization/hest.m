function H=hest(po1, po2)
%This function will find the homography between 4 points using svd

N = size(po1,2);
if N < 4 
   disp('To few points given')
   H = zeros(3,3);
   return
end
if size(po2,2) ~= size(po1,2)
   disp('Pointsets not the same size')
   H = zeros(3,3);
   return 
end

B = [];
for K = 1:N
    q1 = [po1(:,K);1];
    q2= [po2(:,K);1];
    B = [B;kron(q2',CrossOp(q1))];
end

[u,s,v]=svd(B);
H=v(:,end);
H=reshape(H,3,3);