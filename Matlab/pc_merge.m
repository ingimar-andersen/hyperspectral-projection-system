function pc_merge(paths, colors, output_path, denoise)
% %paths = {'C:\Users\grena\OneDrive\DTU\Masters Project\Test\Grayscale Cameras\2019-01-31 Balls on stand\1600ms copy\L_505-pc.mat';'C:\Users\grena\OneDrive\DTU\Masters Project\Test\Grayscale Cameras\2019-01-31 Balls on stand\1600ms copy\L_655-pc.mat'};
% paths = {'C:\Users\Troels\OneDrive\Masters Project\Test\Grayscale Cameras\2019-01-31 Balls on stand\1600ms copy\L_505-pc.mat';'C:\Users\Troels\OneDrive\Masters Project\Test\Grayscale Cameras\2019-01-31 Balls on stand\1600ms copy\L_655-pc.mat'};
% 
% denoise = 1;
% colors = [0.8 0 0.1;0 0.8 0];
% %output_path = 'C:\Users\grena\OneDrive\DTU\Masters Project\Test\Grayscale Cameras\2019-01-31 Balls on stand\1600ms copy\merged.ply';
% output_path = 'C:\Users\Troels\OneDrive\Masters Project\Test\Grayscale Cameras\2019-01-31 Balls on stand\1600ms copy\merged.ply';

%%
% save('C:\Users\grena\OneDrive\DTU\Masters Project\Test\colormap.mat','colors');

nfiles =  length(paths);

%inteisty_list = nan(1);
locations = nan(1,3);
color_list = nan(1,3);

for ii=1:nfiles
    pc = load(paths{ii});
    x = pc.ImageIXYZCrop(:,:,2);
    x = x(:);
    y = pc.ImageIXYZCrop(:,:,3);
    y = y(:);
    z = pc.ImageIXYZCrop(:,:,4);
    z = z(:);
    locationsTemp = [x,y,z];
    locations = [locations;  locationsTemp];
     
    colorTemp = pc.ImageIXYZCrop(:,:,1);
  %  inteisty_list = [inteisty_list ; colorTemp(:)];
    
    
    colorTemp = colors(ii,:).*(colorTemp(:));
    
    color_list = [color_list ; colorTemp];
    
    
    
    
    
end

romove_index = any(isnan(locations), 2);

color_list(romove_index,:) = [];
color_list = round(color_list);

locations(romove_index, :) = [];
%inteisty_list(romove_index) = [];

merged_pc = pointCloud(locations,'Color',color_list/255);

if denoise
    merged_pc = pcdenoise(merged_pc,'Threshold',0.1);
end

pcwrite(merged_pc, output_path);
