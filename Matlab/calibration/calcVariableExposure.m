
function result = calcVariableExposure(filedir)

imagefiles = dir(strcat(filedir,'\*.png'));      
nfiles = length(imagefiles);    % Number of files found

xAxis = zeros(nfiles,1);

progress_bar = parfor_progressbar(nfiles,strcat('Loading ', num2str(nfiles), ' images...')); %create the progress bar 

parfor ii=1:nfiles
   currentfilename = imagefiles(ii).name;
   currentimage = imread(strcat(filedir,'\',currentfilename));
   images{ii} = currentimage;
   
   k = strfind(currentfilename,'nm');
   xAxis(ii) = str2num(currentfilename(k-3:k-1));
   if xAxis(ii) > 900 || xAxis(ii) < 500
      error('Read the name worng..!')
   end
   progress_bar.iterate(1);
end
delete(progress_bar)

temp = images{round(nfiles/2)}; %Select image 

figure(1)
imshow(temp)
hold on
text(size(temp,2)/2,size(temp,1)/2,'Click the four corners in the order:','Color','white','FontSize',15,'HorizontalAlignment','center')
text(size(temp,2)/2,size(temp,1)/2+150,'TopLeft   TopRight   BotRight   BotLeft','Color','white','FontSize',15,'HorizontalAlignment','center')

ylim([1 size(temp,1)])
xlim([1 size(temp,2)])
axis equal
 
[x,y] = ginputred(4);
x = round(x);
y = round(y);

mask = double(poly2mask(x,y,size(temp,1),size(temp,2)));
if size(temp, 3) == 3
    imagesc(double(rgb2gray(temp)).*mask)
else 
    imagesc(double(temp).*mask)
end

minX =min(x);
maxX =max(x);
minY =min(y);
maxY =max(y);
croppedMask = mask(minY:maxY,minX:maxX);
croppedMask(croppedMask==0) = NaN;
tic
intesity = zeros(nfiles,3);
for ii=1:nfiles
    intesity(ii,:) = mean(mean(double(images{ii}(minY:maxY,minX:maxX,:)).*croppedMask,'omitnan'),'omitnan');
end

intesityMax = max(intesity,[],2);

intesityNorm = max(intesityMax)./intesityMax;

figure(2)
plot(xAxis,intesityNorm)
ylim([0.5 5])
title('Variable Exposure')
xlabel('Wavelenght [nm]')
ylabel('Exposure ratio')

result = [xAxis,intesityNorm];

end
