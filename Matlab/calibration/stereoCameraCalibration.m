function [A1, distCoeffs1, A2, distCoeffs2, R2, T2] = stereoCameraCalibration(imageFileNamesL, imageFileNamesR, squareSizeMm, stereoParamsOutputPath)

% Detect checkerboards in images
[imagePoints, boardSize, ~] = detectCheckerboardPoints(imageFileNamesL, imageFileNamesR);

% Generate world coordinates of the checkerboard keypoints
worldPoints = generateCheckerboardPoints(boardSize, squareSizeMm);

% Read one of the images from the first stereo pair
%I1 = imread(imageFileNames1{1});


% Check if camera intrinsics are in the workspace
% if exist('cameraParamsLeft', 'var') && exist('cameraParamsRight', 'var')
    [~,cameraParamsLeft] = singleCameraCalibration(imageFileNamesL, squareSizeMm);
    [~,cameraParamsRight] = singleCameraCalibration(imageFileNamesR, squareSizeMm);
% end

% Stereo calibration
[stereoParams, ~, estimationErrors] = estimateStereoBaseline(imagePoints, worldPoints, ...
cameraParamsLeft, cameraParamsRight, ...
'WorldUnits', 'millimeters');
% else
%     [stereoParams, ~, estimationErrors] = estimateCameraParameters(imagePoints, worldPoints, ...
%     'EstimateSkew', true, 'EstimateTangentialDistortion', true, ...
%     'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'millimeters', ...
%     'InitialIntrinsicMatrix', [], 'InitialRadialDistortion', [], ...
%     'ImageSize', [mrows, ncols]);
% end

% View reprojection errors
%h1=figure; showReprojectionErrors(stereoParams);

% Visualize pattern locations
%h2=figure; showExtrinsics(stereoParams, 'CameraCentric');

% Display parameter estimation errors
displayErrors(estimationErrors, stereoParams);


% OLD METHOD THAT WORKS
fc1 = stereoParams.CameraParameters1.FocalLength;
fc2 = stereoParams.CameraParameters2.FocalLength;
cc = fliplr((stereoParams.CameraParameters1.ImageSize/2));
%A1 = [fc1(1) 0 cc(1);0 fc1(2) cc(2); 0 0 1];
%A2 = [fc2(1) 0 cc(1);0 fc2(2) cc(2); 0 0 1];

% TRY AT NEW METHOD, added by Troels
A1 = stereoParams.CameraParameters1.IntrinsicMatrix';
A2 = stereoParams.CameraParameters2.IntrinsicMatrix';

R2= stereoParams.RotationOfCamera2';  % Antages at den skal transponeres
T2= stereoParams.TranslationOfCamera2';

distCoeffs1 = [stereoParams.CameraParameters1.RadialDistortion, stereoParams.CameraParameters1.TangentialDistortion]';
distCoeffs2 = [stereoParams.CameraParameters2.RadialDistortion, stereoParams.CameraParameters2.TangentialDistortion]';

save(stereoParamsOutputPath, 'stereoParams');
% You can use the calibration data to rectify stereo images.
%I2 = imread(imageFileNames2{1});
%[J1, J2] = rectifyStereoImages(I1, I2, stereoParams);

% See additional examples of how to use the calibration data.  At the prompt type:
% showdemo('StereoCalibrationAndSceneReconstructionExample')
% showdemo('DepthEstimationFromStereoVideoExample')
