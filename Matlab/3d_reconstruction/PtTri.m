function [Q,fit] = PtTri(p1,Row1,Col1,p2,Row2,Col2)
% Performs 3D point triangulation based on a correspondence set between the
% left and right camera, using the two projection matrices (p1,p2)

    B = zeros(4,4);
    B(1,:) = p1(1,:)-Col1*p1(3,:);
    B(2,:) = p1(2,:)-Row1*p1(3,:);
    B(3,:) = p2(1,:)-Col2*p2(3,:);
    B(4,:) = p2(2,:)-Row2*p2(3,:);
    [~,s,v] = svd(B);
    Q = v(:,4)/v(4,4);
    fit = s(4,4);