function [H1,H2,OutXDim,OutYDim,ScanDir]=GetRectHomo(A1,R1,T1,A2,R2,T2,ImSize)

UpScale=1;
%UpScale=1.2;
%UpScale=1.9;
ImCent=ImSize/2;
[H1,H2,ScanDir]=STR_GetRectHomo(A1,R1,T1,A2,R2,T2,ImCent,UpScale);
[OutXDim,OutYDim,H1,H2]=CalcXYData(H1,H2,ImSize);


function [H1,H2,ScanDir]=STR_GetRectHomo(A1,R1,T1,A2,R2,T2,ImCent,UpScale)


C1=-R1'*T1;
C2=-R2'*T2;

Cam1=A1*[R1 T1];
Cam2=A2*[R2 T2];

%find 3D point closest to intersection of image centers
B=zeros(4,4);
B(1,:)=Cam1(1,:)-ImCent(1)*Cam1(3,:);
B(2,:)=Cam1(2,:)-ImCent(2)*Cam1(3,:);
B(3,:)=Cam2(1,:)-ImCent(1)*Cam2(3,:);
B(4,:)=Cam2(2,:)-ImCent(2)*Cam2(3,:);
[u,s,v]=svd(B);
Vo=v(1:3,4)/v(4,4);


%vector between the two camera center
Va=C2-C1;
%mean optical axis
Vc=(R1(3,:)+R2(3,:))'/2;
Vb=cross(Vc,Va);
%Vo=(C1+C2)/2+Vc*norm(Va);

Hbas=[Va/norm(Va) Vb/norm(Vb) Vo ; 0 0 1];
H1=inv(Cam1*Hbas); H1=H1/H1(3,3);
H2=inv(Cam2*Hbas); H2=H2/H2(3,3);
%This scaling shem is bassed on the assumption that the cameras are
%relatively alligned and as such that the H's are close to affine. The
% 1.2 factor is an 'arbitrary' upscaling to account for this.
scale=max(sqrt(det(H1(1:2,1:2))),sqrt(det(H2(1:2,1:2))))/UpScale; 
Hbas=[scale*Va/norm(Va) scale*Vb/norm(Vb) Vo];
Hbas=[Hbas*[1 0 -ImCent(1)*UpScale; 0 1 -ImCent(2)*UpScale; 0 0 1]; 0 0 1];
H1=inv(A1*[R1 T1]*Hbas); H1=H1/H1(3,3);
H2=inv(A2*[R2 T2]*Hbas); H2=H2/H2(3,3);

% %Test, these epipoles should be at infinity allon the x-axis (plus/minus)
% e1=H1*Cam1*[C2;1]
% e2=H2*Cam2*[C1;1]

ScanDir=Vc/norm(Vc);

function [OutXDim,OutYDim,H1,H2]=CalcXYData(H1,H2,ImSize)

Q=[1 ImSize(2) 1 ImSize(2);1 1 ImSize(1) ImSize(1);1 1 1 1];
q=[H1*Q H2*Q];
q(1,:)=q(1,:)./q(3,:);
q(2,:)=q(2,:)./q(3,:);
q(3,:)=q(3,:)./q(3,:);
OutXDim=round([min(q(1,:)) max(q(1,:))]);
OutYDim=round([min(q(2,:)) max(q(2,:))]);
dX=OutXDim(1)-1;
dY=OutYDim(1)-1;
OutXDim=OutXDim-dX;
OutYDim=OutYDim-dY;
H1=[1 0 -dX;0 1 -dY;0 0 1]*H1;
H2=[1 0 -dX;0 1 -dY;0 0 1]*H2;




