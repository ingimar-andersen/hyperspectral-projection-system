function [x1, x2, y1 , y2] = GetCorrespondencesFromGrayEncodedImagesTest(im1, im2, n)
%{
  Usage: points = GetCorrespondencesFromGrayEncodedImages(im1, im2, n)
  Before: im1 and im2 are gray encoded images
          n is such that the range of values in im1 and im2 is between
          0 and 2^n
  After: points is a k by 3 array of correspondences where each row
         [y x1 x2] represents the match of points (y, x1) from im1
         and (y, x2) in image 2
%}
points = [];
step = floor(size(im1, 2)/2^(n+1));
disp('using stepsize:')
%step
yofset = 8;

for lineNr=yofset+1:step:size(im1,1)-yofset-1
    
    lineMatches = zeros(2^n, 2);
    line1 = im1(lineNr, :);
    line2 = im2(lineNr+yofset , :);
    
    for i=1:size(im1,2)
        pixelValue1 = line1(i);
        if pixelValue1 ~= 0
            lineMatches(pixelValue1, 1) = i;
        end
        pixelValue2 = line2(i);
        if pixelValue2 ~= 0
            lineMatches(pixelValue2, 2) = i;
        end
    end
    realMatches = zeros(2^n, 3);
    j = 1;
    for i=1:size(lineMatches)
        if lineMatches(i, 1) * lineMatches(i, 2) ~= 0
            realMatches(j, 2:3) = lineMatches(i, :);
            realMatches(j, 1) = lineNr;
            j = j + 1;
        end
    end
    realMatches = realMatches(1:j-1, :);
    points = [points; realMatches];
end

y1 = points(:, 1);
y2 = points(:, 1)+yofset;
x1 = points(:, 2);
x2 = points(:, 3);

end