function [EncIm,B] = GetEncIm(Pos,Neg,OccMask,nfiles,threshold)
    % Pos: Positive projected patterns
    % Neg: Negative projected patterns
    % OccMask: Occlusion Mask
    
    % image size
    imsz = size(OccMask);

    % Compute encoding
    B = zeros([imsz nfiles],'logical');
    EncIm = zeros(imsz,'int16');

    for iii=2:nfiles%+1
       
        n=iii-1;

        PosX = Pos(:,:,iii);
        NegX = Neg(:,:,iii);

        DI = int16(PosX) - int16(NegX);
        DI = int16(imfilter(DI,fspecial('gaussian')));
        
        B(:,:,n) = (DI > threshold) & OccMask;
        
        newadd = int16(B(:,:,n).*(2^(n-1)));
        
        
        %plot(newadd(round(size(newadd,1)/2),:),'LineWidth',1.5)

        EncIm = EncIm + newadd;
    end