function ptCloud = grayCode2pc(workingDir,wavelength,fileFormat,stereoParams,NrOfPat,scale,plots)

% For debugging 
%workingDir = '../../Test/2018-10-30 Flat bar 10img avg';
%wavelength = '540';
%fileFormat = 'png';
%scale = 1;
%NrOfPat = 5;
%camParamsFolder = '../../Test/Spectralon 3D scan 24 okt/stereoParams_2018-10-24';
%load(camParamsFolder);
%plots = 0;
leftFolder = strcat(workingDir, '/L_', wavelength);
rightFolder = strcat(workingDir, '/R_', wavelength);
%camParamsFolder = '../../Test/Spectralon 3D scan 24 okt/stereoParams_2018-10-24';
disp('Loading images')
[Pos1,Neg1,nfiles]=GetImagesSingleFolder(leftFolder, fileFormat);
[Pos2,Neg2,~]=GetImagesSingleFolder(rightFolder, fileFormat);



%% Remove radial distorsion 

disp('Undistorting')
for iii=1:nfiles
    [Pos1(:,:,iii),~] = undistortImage(Pos1(:,:,iii),stereoParams.CameraParameters1);
    [Neg1(:,:,iii),~] = undistortImage(Neg1(:,:,iii),stereoParams.CameraParameters1);
    [Pos2(:,:,iii),~] = undistortImage(Pos2(:,:,iii),stereoParams.CameraParameters2);
    [Neg2(:,:,iii),~] = undistortImage(Neg2(:,:,iii),stereoParams.CameraParameters2);
end

%%  TILF�JER EN BRUGER DEFINERET MASKE
applayMask = 0;

if applayMask == 1
    temp = Pos1(:,:,1); %Select image 
    figure(42);
    imshow(temp)
    [x,y] = ginputred(4);
    mask = double(poly2mask(x,y,size(temp,1),size(temp,2)));

    for ii=1:nfiles
        Pos1(:,:,ii) =  Pos1(:,:,ii).*uint8(mask);
        Neg1(:,:,ii) =  Neg1(:,:,ii).*uint8(mask);
    end
    
    temp = Pos2(:,:,1); %Select image 
    imshow(temp)
    [x,y] = ginputred(4);
    mask = double(poly2mask(x,y,size(temp,1),size(temp,2)));

    for ii=1:nfiles
        Pos2(:,:,ii) =  Pos2(:,:,ii).*uint8(mask);
        Neg2(:,:,ii) =  Neg2(:,:,ii).*uint8(mask);

    end
    
    close 42
end


%% Set Calbraion parameters

fc1 = stereoParams.CameraParameters1.FocalLength;
fc2 = stereoParams.CameraParameters2.FocalLength;
cc = fliplr((stereoParams.CameraParameters1.ImageSize/2));

A1 = [fc1(1) 0 cc(1);0 fc1(2) cc(2); 0 0 1];
R1 = eye(3,3);
T1 = [0 0 0]';

A2 = [fc2(1) 0 cc(1);0 fc2(2) cc(2); 0 0 1];
R2= stereoParams.RotationOfCamera2'; 
T2= stereoParams.TranslationOfCamera2';

%% Rectifying
[H1,H2,OutXDim,OutYDim,ScanDir] = GetRectHomo(A1,R1,T1,A2,R2,T2,size(Pos1(:,:,1)));

disp('Rectifying')

mytform1 = projective2d(H1');
mytform2 = projective2d(H2');

Pos1warp = zeros(OutYDim(2),OutXDim(2),size(Pos1,3));
Neg1warp = zeros(OutYDim(2),OutXDim(2),size(Pos1,3));
Pos2warp = zeros(OutYDim(2),OutXDim(2),size(Pos1,3));
Neg2warp = zeros(OutYDim(2),OutXDim(2),size(Pos1,3));

for qq = 1:nfiles
    
    Pos1warp(:,:,qq) = imwarp(Pos1(:,:,qq),mytform1,'OutputView',imref2d([OutYDim(2),OutXDim(2)]),'Interp','linear');
    Neg1warp(:,:,qq) = imwarp(Neg1(:,:,qq),mytform1,'OutputView',imref2d([OutYDim(2),OutXDim(2)]),'Interp','linear');
    
    Pos2warp(:,:,qq) = imwarp(Pos2(:,:,qq),mytform2,'OutputView',imref2d([OutYDim(2),OutXDim(2)]),'Interp','linear');
    Neg2warp(:,:,qq) = imwarp(Neg2(:,:,qq),mytform2,'OutputView',imref2d([OutYDim(2),OutXDim(2)]),'Interp','linear');
    
end

%%  Mulighed for billede skalering for �get opl�sning

if scale == 1
    Pos1warpBig = Pos1warp;
    Neg1warpBig = Neg1warp;
    Pos2warpBig = Pos2warp;
    Neg2warpBig = Neg2warp;
else
    disp('Scaleing')
    Pos1warpBig = zeros(size(Pos1warp,1)*scale,size(Pos1warp,2)*scale,size(Pos1warp,3));
    Neg1warpBig = zeros(size(Pos1warp,1)*scale,size(Pos1warp,2)*scale,size(Pos1warp,3));
    Pos2warpBig = zeros(size(Pos1warp,1)*scale,size(Pos1warp,2)*scale,size(Pos1warp,3));
    Neg2warpBig = zeros(size(Pos1warp,1)*scale,size(Pos1warp,2)*scale,size(Pos1warp,3));
    
    for qq = 1:nfiles
        Pos1warpBig(:,:,qq) = imresize(Pos1warp(:,:,qq), scale);
        Neg1warpBig(:,:,qq) = imresize(Neg1warp(:,:,qq), scale);
        Pos2warpBig(:,:,qq) = imresize(Pos2warp(:,:,qq), scale);
        Neg2warpBig(:,:,qq) = imresize(Neg2warp(:,:,qq), scale);
    end
end

%%
disp('Computing occ masks')
T = 30;
imw1 = Pos1warpBig(:,:,1);
imb1 = Neg1warpBig(:,:,1);
OccMask1 = GetOccMask(imb1,imw1,T);

imw2 = Pos2warpBig(:,:,1);
imb2 = Neg2warpBig(:,:,1);
OccMask2 = GetOccMask(imb2,imw2,T);

if plots == 1
    figure(1)
    subplot(1,2,1)
    imshow(OccMask1)
    subplot(1,2,2)
    imshow(OccMask2)
end

%%
disp('Computing encodings')
n = NrOfPat; % limit to 2^8 = 256 lines
t = 0;

EncIm1=GetEncIm(Pos1warpBig,Neg1warpBig,OccMask1,n,t);

EncIm2=GetEncIm(Pos2warpBig,Neg2warpBig,OccMask2,n,t);

if plots == 1
figure(2)
subplot(1,2,1)
PlotEnc(EncIm1,n)
title('EncIm1')
subplot(1,2,2)
PlotEnc(EncIm2,n)
title('EncIm2')
end

%%
disp('Finding point correspondances')
[x1, x2, y] = GetCorrespondencesFromGrayEncodedImages(uint16(double(EncIm1)+double(OccMask1)), uint16(double(EncIm2)+double(OccMask2)), 8);

%
x1 = x1./scale;
x2 = x2./scale;
y = y./scale;


disp('Point triangulation')
Q = zeros(4,length(y));
fit = zeros(size(y));

Cam1 = H1*A1*[R1 T1];
Cam2 = H2*A2*[R2 T2];

for i = 1:length(y)
    Col1=x1(i);
    Row1=y(i);
    Col2=x2(i);
    Row2=y(i);
    [Q(:,i) , fit(i)] = PtTri(Cam1,Row1,Col1,Cam2,Row2,Col2);
end

ptCloud = pointCloud(Q(1:3,:)');


disp('-DONE-')
end