function [unwrapped_im,P] = unwrap_phase(primary,cue,wave_count)
% Unwraps the reletaive phase images: primary and cue, to produce an abseloute phase image.
% Based on the heterodyne principle.    

    phase_cue = mod(cue-primary,2*pi);

    P = round(((phase_cue * wave_count) - primary) / (2 * pi));

    unwrapped_im = (primary + (2 * pi * P)) / wave_count;
end