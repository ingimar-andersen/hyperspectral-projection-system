function phaseim = decode_phase(images)
% Decodes a series of shifted sinusoidal images down to one relateve phase
% image using FFT.

imfft = fft(images,size(images,3),3);
phaseim = mod(-angle(imfft(:,:,2)),2*pi);

end

