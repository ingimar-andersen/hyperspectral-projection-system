function phaseShifting_v3(left_path, right_path, calibrationParametersPath, output_filepath, periods, primary_steps, cue_steps, plot_enabled)
%% PHASE SHIFTING V3
% 2018 - 12 - 03 - TKK
% Used for Hyper Spectral Projection System
% This uses the rectified images of a set of primary and cue sinosoidal
% patterns acquirede from both right and left camera, in conjunction with 
% the P matices generated from openCV. From this the phase maps is generate
% and coresponded between the left and rigt wiev. This is then triangulated
% whereafter the calculated 3D points are saved in HSPS format.

% left_path                     string with path to directory containing images acquired from left camera  
% right_path                    string with path to directory containing images acquired from right camera
% calibrationParametersPath     string with path to directory containing P matices generated from openCV
% output_filepath               string with path of desired output path
% periods                       int number of number of periods in the projected sinusoidal pattern
% primary_steps                 int number of number of steps of the primary pattern per period 
% cue_steps                     int number of number of steps of the cue pattern per period
% plot_enabled                  0 = plot disabled, 1 = plot enabled

%% LOAD ALL .jpg IMAGES IN FOLDER

left_folderDir = left_path;
right_folderDir = right_path;


% LOAD images acquired from left camera 
imagefiles = dir(fullfile(left_folderDir,'\*.png'));      
nfiles_left = length(imagefiles);    % Number of left images found

mywbarL = waitbar(0,strcat('Loading left images-',num2str(1),'-of-',num2str(nfiles_left)));
first_image = imread(strcat(left_folderDir,imagefiles(1).name));
[height, width] = size(first_image);
im_warp_left = zeros(height,width,nfiles_left);

for ii=1:nfiles_left
   waitbar(ii/nfiles_left,mywbarL,strcat('Loading left images ',num2str(ii),'-of-',num2str(nfiles_left)))
   currentfilename = imagefiles(ii).name;
   currentimage = imread(strcat(left_folderDir,currentfilename));
   im_warp_left(:,:,ii) = (currentimage);
end
delete(mywbarL) 


% LOAD images acquired from right camera 
imagefiles = dir(fullfile(right_folderDir,'\*.png'));      
nfiles_right = length(imagefiles);    % Number of right images found

mywbarR = waitbar(0,strcat('Loading right images-',num2str(1),'-of-',num2str(nfiles_right)));
im_warp_right = zeros(height,width,nfiles_right);

for ii=1:nfiles_right
   waitbar(ii/nfiles_right,mywbarR,strcat('Loading right images ',num2str(ii),'-of-',num2str(nfiles_right)))
   currentfilename = imagefiles(ii).name;
   currentimage = imread(strcat(right_folderDir,currentfilename));
   im_warp_right(:,:,ii) = (currentimage);
end
delete(mywbarR)

if nfiles_left ~= nfiles_right
   error('not the same smount of images') 
else
    nfiles = nfiles_left;
end


clear currentfilename currentimage ii mywbarL mywbarR imagefiles nfiles_left nfiles_right

%% LOAD previosly calibrated parameters from openCV, p1, p2 are rectified projection matricies.
[~, ~, p1, p2, ~, ~]  = loadCalibrationParametersFromFolder(calibrationParametersPath);

%% Create occlusion mask for background removal, based on at set of fully lit and dark images. Done for each of the two wievs
disp('Creating occ mask')

t = 15;
mask_left = GetOccMask(im_warp_left(:,:,2),im_warp_left(:,:,1),t);
mask_right = GetOccMask(im_warp_right(:,:,2),im_warp_right(:,:,1),t);
mask_left = bwmorph(mask_left,'thin',20);
mask_right = bwmorph(mask_right,'thin',20);

mask_left = bwmorph(mask_left,'majority');
mask_left = bwareaopen(mask_left, 250);
mask_right = bwmorph(mask_right,'majority');
mask_right = bwareaopen(mask_right, 250);

if plot_enabled == 1
    figure(1)
    subplot(1,2,1)
    imshow(mask_left)
    title('Left mask')
    subplot(1,2,2)
    imshow(mask_right)
    title('Right mask')
end

clear t


%% Find bounding box of the data in the occlusion mask. Used later to only process the area inside the bounding box to save on computation time.

boundingBox = regionprops(mask_left, 'BoundingBox' );
boundingBoxArr = cell2mat(struct2cell(boundingBox));
boundingBoxMat = reshape(boundingBoxArr,4,size(boundingBoxArr,2)/4)';

startX = floor(min(boundingBoxMat(:,1)));
endX = floor(max(boundingBoxMat(:,1)+boundingBoxMat(:,3)));
startY = ceil(min(boundingBoxMat(:,2)));
endY = ceil(max(boundingBoxMat(:,2)+boundingBoxMat(:,4)));

boundingBoxCOMBO = [startX,startY,endX-startX,endY-startY];


%% Docode phase in left and right images
disp('Decoding phase')

if size(im_warp_left,3) ~= primary_steps+cue_steps+2
    error('Number of primary and cue images does not match expected values')
end

primary_end_index = primary_steps + 2;

% Decode phase in left images 
primary_left = decode_phase(im_warp_left(:,:,3:primary_end_index));
cue_left = decode_phase(im_warp_left(:,:,primary_end_index + 1:end));

% Decode phase in right images 
primary_right = decode_phase(im_warp_right(:,:,3:primary_end_index));
cue_right = decode_phase(im_warp_right(:,:,primary_end_index + 1:end));


%% Unwrap phase

[phase_left,~] = unwrap_phase(primary_left,cue_left,periods);
[phase_right,~] = unwrap_phase(primary_right,cue_right,periods);

clear cue_left cue_right primary_left primary_right

%% Corrospond phase from left to right image
disp('Correspondance')

% Temp placeholder
correspondancesImage = nan(size(im_warp_left,1),size(im_warp_left,2),3);

% Add black border to images to avoid edge erros
mask_right(:,1) = 0;
mask_left(:,1) = 0;
mask_right(:,end) = 0;
mask_left(:,end) = 0;
%-----

nanmask = double(mask_right);
nanmask(nanmask==0) = nan;

% Determine how close the phase should be to be a match
phase_match_threshold = 0.05;

for y=startY:endY
    
    row_right = phase_right(y,:).*nanmask(y,:); 
    
    for x=startX:endX 
        
        % Check if the pixel is active in the left mask
        if mask_left(y,x)==1
            value = phase_left(y,x);
            norm_row_right = row_right-value;
            
            [minVal,minIndex] = min(abs(norm_row_right));
           
            if minVal < phase_match_threshold
                
                % Check if the found pixel is active in the right mask
                if mask_right(y,minIndex) == 1
                    
                    % Calculate subpixel precision
                    if norm_row_right(minIndex) > 0
                        subIndex = minIndex - abs(norm_row_right(minIndex))/ (abs(norm_row_right(minIndex)) + abs(norm_row_right(minIndex - 1)));
                    else  
                        subIndex = minIndex + abs(norm_row_right(minIndex))/ (abs(norm_row_right(minIndex)) + abs(norm_row_right(minIndex + 1)));
                    end

                    % Save the point correspondances
                    correspondancesImage(y,x,:) = [x,y,subIndex];
                end
            end
        end
    end
end

clear norm_row_right minVal minIndex stepSize x y value subIndex row_right phase_match_threshold nanmaks


%% Point triangulation based on found correspondance

disp('Point triangulation Image format retained')

% Temp placeholder
ImageXYZ = nan(size(im_warp_left,1),size(im_warp_left,2),3);

for y=startY:endY
    for x = startX:endX
        if  ~isnan(correspondancesImage(y,x,3))
            Col1=correspondancesImage(y,x,1);
            Row1=correspondancesImage(y,x,2);
            Col2=correspondancesImage(y,x,3);
            Row2=correspondancesImage(y,x,2);
            
            % Triangulate 
            [Q , ~] = PtTri(p1,Row1,Col1,p2,Row2,Col2);
            
            % Save 3D point, on 2D location in left imaged 
            ImageXYZ(y,x,:) = Q(1:3);
        end
    end
end

% Generate PointCloud structure
% pc = pointCloud(ImageXYZ,'Color',repmat(im_warp_left(:,:,1)/255,1,1,3));

%% Save data in HSPS data format
disp(strcat('Saving data at:',output_filepath))

ImageIXYZ = cat(3,im_warp_left(:,:,1),ImageXYZ);

ImageIXYZCrop = ImageIXYZ(startY:endY,startX:endX,:);

xDim = size(im_warp_left(:,:,1),2);
yDim = size(im_warp_left(:,:,1),1);

DaysDate = datetime;
save(output_filepath,'ImageIXYZCrop','boundingBoxCOMBO','left_folderDir','right_folderDir','DaysDate','xDim','yDim');

end