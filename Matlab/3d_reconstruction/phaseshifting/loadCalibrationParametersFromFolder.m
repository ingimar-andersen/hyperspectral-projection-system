function [r1, r2, p1, p2, distCoeffs1,distCoeffs2] = loadCalibrationParametersFromFolder(folderPath)
% Loads the newest versions of prviosly generated calibration parameters at
% folderPath.

files = dir(fullfile(folderPath, 'p1*.txt'));
[~,idx] = sort([files.datenum], 'descend');
p1 = importdata(fullfile(folderPath, files(idx(1)).name));

files = dir(fullfile(folderPath, 'p2*.txt'));
[~,idx] = sort([files.datenum], 'descend');
p2 = importdata(fullfile(folderPath, files(idx(1)).name));

files = dir(fullfile(folderPath, 'r1*.txt'));
[~,idx] = sort([files.datenum], 'descend');
r1 = importdata(fullfile(folderPath, files(idx(1)).name));

files = dir(fullfile(folderPath, 'r2*.txt'));
[~,idx] = sort([files.datenum], 'descend');
r2 = importdata(fullfile(folderPath, files(idx(1)).name));

files = dir(fullfile(folderPath, 'distCoeffs1*.txt'));
[~,idx] = sort([files.datenum], 'descend');
distCoeffs1 = importdata(fullfile(folderPath, files(idx(1)).name));

files = dir(fullfile(folderPath, 'distCoeffs2*.txt'));
[~,idx] = sort([files.datenum], 'descend');
distCoeffs2 = importdata(fullfile(folderPath, files(idx(1)).name));