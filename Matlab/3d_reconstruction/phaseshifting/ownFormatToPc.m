function pc = ownFormatToPc(ownformatData)

x = ownformatData.ImageIXYZCrop(:,:,2);
y = ownformatData.ImageIXYZCrop(:,:,3);
z = ownformatData.ImageIXYZCrop(:,:,4);

I = ownformatData.ImageIXYZCrop(:,:,1)/255;
colordata = repmat(I(:),1,3);

pc = pointCloud([x(:),y(:),z(:)],'Color',colordata);

end