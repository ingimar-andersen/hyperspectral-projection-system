function output_path = save_hsps_as_ply(hspsPath,plot_enabled)
hsps = load(hspsPath);
[filepath,name,~] = fileparts(hspsPath);

pc = ownFormatToPc(hsps);

if plot_enabled == 1
    pcshow(pc)
end

output_path = fullfile(filepath, strcat(name, '.ply'));
pcwrite(pc, output_path)