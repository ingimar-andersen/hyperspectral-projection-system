function OccMask = GetOccMask(imb,imw,T)
% Generates binary occlusion mask based on a completly lit image(imw) and a compltly
% dark image(imb) and a predefined threshold (T). 

OccMask = abs(imw-imb);
OccMask = imopen(OccMask,strel('disk',4,4));
OccMask = imbinarize(OccMask,T);