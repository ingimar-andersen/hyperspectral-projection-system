function test=myButtonPushedGray(images,Spectralon,specFig,specNromFig,squareFig,xAxis)

yDim = size(images{1},1);
xDim = size(images{1},2);

nfiles = size(images,2);

[x,y] = ginputred(4);
x = round(x);
y = round(y);


squareFig.Shape.Vertices = [x,y];

mask = double(poly2mask(x,y,yDim,xDim));
minX =min(x);
maxX =max(x);
minY =min(y);
maxY =max(y);
croppedMask = mask(minY:maxY,minX:maxX);
croppedMask(croppedMask==0) = NaN;
intesity = zeros(nfiles,1);

for ii=1:nfiles
    intesity(ii) = mean(mean(double(images{ii}(minY:maxY,minX:maxX)).*croppedMask,'omitnan'),'omitnan')/256;
end

response = intesity./Spectralon;

responseNorm = response;


specFig.Children(2).YData = intesity;

specNromFig.Children(3).XData = xAxis;
specNromFig.Children(3).YData = responseNorm;

end