function mySaveButtonPushedGray(specFig,specNromFig)

[file,path] = uiputfile;
saveData = [specNromFig.Children(3).XData,;specNromFig.Children(3).YData];
file = strrep(file,'mldatx','txt');
dlmwrite(strcat(path,file),saveData,'delimiter','\t','precision',5)


end