function myTestFunc(imfig,im,curvefig1,curvefig2,nmtext,val)

val
imfig.CData=im;

curvefig1.XData = [val+499 val+499];
curvefig2.XData = [val+499 val+499];

nmtext.String=strcat('Wavelenght: ',num2str(round(val+499)),' [nm]');
nmtext.Position = [50 0.0500 0];
end