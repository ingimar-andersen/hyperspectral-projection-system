%% LOAD ALL .jpg IMAGES IN FOLDER

STEPSIZE = 1;

folderDir = '..\..\Test\Grayscale Cameras\2018-12-11 Colorchecker variable exposure\R half\L\';
imagefiles = dir(strcat(folderDir,'*.png'));      
nfiles = length(imagefiles);    % Number of files found

xAxis = zeros(1,nfiles);
mywbar = waitbar(0,strcat('Loading images-',num2str(1),'-of-',num2str(nfiles)));

for ii=1:nfiles
   waitbar(ii/nfiles,mywbar,strcat('Loading images ',num2str(ii),'-of-',num2str(nfiles)))
   currentfilename = imagefiles(ii).name;
   currentimage = imread(strcat(folderDir,currentfilename));
   images{ii} = currentimage;
   
   k = strfind(currentfilename,'nm');
   xAxis(ii) = str2num(currentfilename(k-3:k-1));
   if xAxis(ii) > 900 || xAxis(ii) < 500
      error('Read the name worng..!')
   end
end
delete(mywbar)

%%

close all

%load('SpectralonV2.mat')
%Spectralon = SpetralonV2;

temp = load('..\..\Test\Grayscale Cameras\2018-12-11 Variable exposure data\sweep 504-800 1nm\spectralon_left.txt');

Spectralon = temp(1:nfiles,2);

Spectralon = Spectralon./256;


SpectralonNorm = Spectralon./max(Spectralon(:,1));

intesity = ones(nfiles,1)*(2^8);

% MAIN FIGURE
mainfig = figure('Name','Main window');
mainfig.Position=[0 0 1920 1080];
mainfig.Color = [1 1 1];
mainfig.Resize = 'off';


% IMAGE SUB FIGURE
imSubPlot = subplot(1,3,1);
imSubPlot.Position = [0.0400    0.1300    0.3134    0.8150];
hold on
axis equal
imFig = imagesc(images{round(nfiles/2)});
ylim([1 size(images{1},1)])
xlim([1 size(images{1},2)])
set(gca,'YDir','reverse')
colormap gray

pgon2 = polyshape([0 0 1 1],[1 0 0 1]);
squareFig = plot(pgon2);
squareFig.FaceAlpha = 0.0;
squareFig.LineStyle = '--';
squareFig.EdgeColor = 'w';

% IMAGE SPEC FIGURE
specFig = subplot(1,3,2);
title('Measured spectrum')
hold on
plot(xAxis,Spectralon(:,:),'Color',[0.5 0.5 0.5])
plot(xAxis,intesity(:,1),'k')
curveFig1 = line([501 501],[0 2^16],'Color','black','LineStyle','--');
ylim([0 255])
xlim([500 700])
xlabel('Wavelength [nm]')
ylabel('Average intensity [0-255]')


% IMAGE SPEC NORM FIGURE
specNromFig = subplot(1,3,3);
title('Normalized measured spectrum')
hold on
plot(ones(20,1),ones(20,1),'k')
ylim([0 1.1])
xlim([500 700])
grid on
xlabel('Wavelength [nm]')
ylabel('Ratio [0-1]')
curveFig2 = line([501 501],[0 2],'Color','black','LineStyle','--');



nmtext = text(0,0.05,'--');

% SLIDER ----
slmin = 1;
slmax = nfiles;
hsl = uicontrol('Style','slider','Min',slmin,'Max',slmax,'SliderStep',[1 1]./(slmax-slmin),'Value',round(nfiles/2),'Position',[50 130 500 40]);
set(hsl,'Callback',@(hObject,eventdata) myTestFunc(imFig,images{round(get(hObject,'Value'))},curveFig1,curveFig2,nmtext,get(hObject,'Value')*STEPSIZE))

myTestFunc(imFig,images{round(nfiles/2)},curveFig1,curveFig2,nmtext,round(nfiles/2)*STEPSIZE);
% SLIDER ----


mySaveBut = uicontrol;
mySaveBut.String = 'Save Data';
set(mySaveBut,'Callback', @(hObject,eventdata) mySaveButtonPushedGray(specFig,specNromFig))
mySaveBut.Position = [100 820 100 40];



myBut = uicontrol;
myBut.String = 'Select data region';
set(myBut,'Callback', @(hObject,eventdata) myButtonPushedGray(images,Spectralon,specFig,specNromFig,squareFig,xAxis))
myBut.Position = [50 30 500 40];


myButtonPushedGray(images,Spectralon,specFig,specNromFig,squareFig,xAxis)