# README #

Code used for the master thesis "Development of a Hyperspectral Projection System" by Ingimar Andersen and Troels K. Kristensen at the Technical University of Denmark. The developed system can make both 3D and spectral measurements of objects, resulting in a pointcloud per wavelength.

The code is divided into two parts:
LaserDriver - Contains all UI and over-all control of the system in a WPF application written in C#/.Net.
Matlab - Contains Matlab data processing scripts that are called through a Matlab COM object in the WPF client application.