Started: 19-02-01_14-01-47
Input parameters:
leftCameraCalibrationFolder: C:\Users\grena\OneDrive\DTU\Masters Project\Test\Grayscale Cameras\2019-02-01 Camera calibration\L
rightCameraCalibrationFolder: C:\Users\grena\OneDrive\DTU\Masters Project\Test\Grayscale Cameras\2019-02-01 Camera calibration\R
squareSizeMm: 4

FindChessBoardCornersAsync (L + R) execution time: 40233
CalibrateSingleCameraAsync (L + R) execution time: 5152
StereoCalibrate execution time: 5257
OpenCV calibration parameters:
calibrationTimeMs: 50982
singleCameraFlags: FixAspectRatio, FixPrincipalPoint, ZeroTangentDist, FixK3
stereoCameraFlags: FixIntrinsic
singleCameraTerminationCriteria: MaxIter = 100, Epsilon = 0
stereoTerminationCriteria: MaxIter = 200, Epsilon = 0
chessboardCornerGridSize: 17x16 (WxH)
reprojectionErrorL: 1.094701629195
reprojectionErrorR: 1.01497321913473
StereoRectify execution time: 2
Save .json files execution time: 39042
Total execution time: 90148
