﻿using ArduinoDriver;
using ArduinoDriver.SerialProtocol;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HW
{
    public class Arduino : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool IsRunning = false;
        public static bool IsConnected = false;
        public ArduinoDriver.ArduinoDriver Driver;
        public static readonly Dictionary<byte, Tuple<byte, WriteMode>> OutputStateByPinNumber = new Dictionary<byte, Tuple<byte, WriteMode>>();

        public enum PinMode
        {
            Input = 0,
            InputPullup = 1,
            Output = 2
        }

        public enum WriteMode
        {
            NotSet = 0,
            Digital = 0,
            Analog = 1,
        }

        private static readonly Arduino instance = new Arduino();

        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static Arduino()
        {
        }

        private Arduino()
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("        =============  Started Logging  =============        ");
            try
            {
                Driver = new ArduinoDriver.ArduinoDriver(ArduinoUploader.Hardware.ArduinoModel.UnoR3, Properties.Settings.Default.ComPort, autoBootstrap: true);
                IsConnected = true;
                log.Info("Arduino connected.");
                
            }
            catch
            {
                IsConnected = false;
                log.Info("Arduino disconnected.");
            }
        }

        public static Arduino Instance
        {
            get
            {
                return instance;
            }
        }

        public async Task<bool> TryTriggerCameras(byte triggerPin, int signalWidthMs = 10)
        {
            if (IsRunning || !IsConnected)
            {
                return false;
            }
            IsRunning = true;
            try
            {
                return await Task<bool>.Run(() =>
                {
                    var responseHigh = Driver.Send(new AnalogWriteRequest(triggerPin, byte.MaxValue));
                    Thread.Sleep(signalWidthMs);
                    var responseLow = Driver.Send(new AnalogWriteRequest(triggerPin, byte.MinValue));
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("Error triggering cameras.", ex);
                return false;
            }
            finally
            {
                IsRunning = false;
            }
        }

        public void Dispose()
        {
            Driver?.Dispose();
        }

        public bool SetPinMode(byte pin, PinMode mode)
        {
            if (!IsConnected) return false;
            try
            {
                ArduinoDriver.SerialProtocol.PinMode pinMode;
                switch (mode)
                {
                    case PinMode.Input:
                        pinMode = ArduinoDriver.SerialProtocol.PinMode.Input;
                        break;
                    case PinMode.InputPullup:
                        pinMode = ArduinoDriver.SerialProtocol.PinMode.InputPullup;
                        break;
                    case PinMode.Output:
                        pinMode = ArduinoDriver.SerialProtocol.PinMode.Output;
                        if (!OutputStateByPinNumber.ContainsKey(pin))
                        {
                            OutputStateByPinNumber.Add(pin, new Tuple<byte, WriteMode>(0, WriteMode.NotSet));
                        }
                        break;
                    default:
                        throw new NotImplementedException("Unknown pinmode");
                }
                var response = Driver.Send(new PinModeRequest(pin, pinMode));
                return response.Pin == pin && response.Mode == pinMode;
            }
            catch(Exception ex)
            {
                log.Error("Error setting pin mode.", ex);
                return false;
            }
        }

        public bool AnalogWrite(byte triggerPin, byte value)
        {
            if (!IsConnected) return false;
            try
            {
                var response = Driver.Send(new AnalogWriteRequest(triggerPin, value));
                OutputStateByPinNumber[triggerPin] = new Tuple<byte, WriteMode>((byte)response.ValueWritten, WriteMode.Analog);
                return response.PinWritten == triggerPin && response.ValueWritten == value;
            }
            catch(Exception ex)
            {
                log.Error("Error during analog write.", ex);
                return false;
            }
        }

        public bool DigitalWrite(byte triggerPin, bool value)
        {
            if (!IsConnected) return false;
            try
            {
                var writeValue = value ? DigitalValue.High : DigitalValue.Low;
                var response = Driver.Send(new DigitalWriteRequest(triggerPin, writeValue));
                OutputStateByPinNumber[triggerPin] = new Tuple<byte, WriteMode>((byte)response.PinValue, WriteMode.Digital);
                return response.PinWritten == triggerPin && response.PinValue == writeValue;
            }
            catch (Exception ex)
            {
                log.Error("Error during digital write.", ex);
                return false;
            }
        }
    }
}
