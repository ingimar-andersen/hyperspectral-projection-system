﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LaserDriver
{
    public class Spectrometer
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static SpectrometerScanResult ReadSpectrum(int integrationTimeUs, int measurementCount)
        {
            int numberOfSpectrometers;                  // actually attached and talking to us

            using (OmniDriver.CCoWrapper wrapper = new OmniDriver.CCoWrapper())
            {
                numberOfSpectrometers = wrapper.openAllSpectrometers(); // Gets an array of spectrometer objects
                log.Info($"Number of spectrometers found: {numberOfSpectrometers}");
                if (numberOfSpectrometers == 0)
                {
                    var msg = "No Spectrometers were found";
                    log.Error(msg);
                    MessageBox.Show(msg);
                    return null;
                }
                wrapper.setIntegrationTime(0, integrationTimeUs);  // Sets the integration time of the first spectrometer
                log.Info($"Integration time of the first spectrometer has been set to {integrationTimeUs} microseconds");

                var measurements = new List<double[]>();
                for(int i = 0; i < measurementCount; i++)
                {
                    measurements.Add(wrapper.getSpectrum(0));
                }

                return new SpectrometerScanResult { WavelengtArray = wrapper.getWavelengths(0), IntensityMeasurements = measurements };
            }
        }

        public static int NumberOfConnectedDevices()
        {
            using (OmniDriver.CCoWrapper wrapper = new OmniDriver.CCoWrapper())
            {
                return wrapper.openAllSpectrometers(); // Gets the number of connected spectrometers
            }
        }
    }

    public class SpectrometerScanResult
    {
        public double[] WavelengtArray; //[nm]
        public List<double[]> IntensityMeasurements; 
    }
}
