﻿using log4net;
using SpinnakerNET;
using SpinnakerNET.GenApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver
{
    public class ManagedCamera : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IManagedCamera Camera;
        public INodeMap NodeMap;

        public IManagedImage LatestImage;

        public bool UseForAcquisition;

        private string deviceId;
        public string DeviceId
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(deviceId))
                {
                    return deviceId;
                }
                else
                {
                    var serial = GetSerialNumber();
                    if (serial == LaserDriver.Camera.LEFT_CAMERA_SERIAL_NO)
                    {
                        deviceId = "L";
                    }
                    else if (serial == LaserDriver.Camera.RIGHT_CAMERA_SERIAL_NO)
                    {
                        deviceId = "R";
                    }
                    return deviceId;
                }

            }
        }

        private string serialNumber;
        public string SerialNumber
        {
            get
            {
                if (string.IsNullOrWhiteSpace(serialNumber))
                {
                    serialNumber = GetSerialNumber();
                }
                return serialNumber;
            }
        }

        public ManagedCamera(IManagedCamera camera, bool useForAcquisition = true)
        {
            this.UseForAcquisition = useForAcquisition;
            this.Camera = camera;
            Camera.Init();
            this.NodeMap = Camera.GetNodeMap();
        }

        public void Dispose()
        {
            log.Info("Camera container disposal started.");
            Camera.Dispose();
        }

        private string GetSerialNumber()
        {
            try
            {
                String deviceSerialNumber = "";
                INodeMap nodeMapTLDevice = Camera.GetTLDeviceNodeMap();
                IString iDeviceSerialNumber = nodeMapTLDevice.GetNode<IString>("DeviceSerialNumber");
                if (iDeviceSerialNumber != null && iDeviceSerialNumber.IsReadable)
                {
                    deviceSerialNumber = iDeviceSerialNumber.Value;

                    log.Info($"Device serial number retrieved as {deviceSerialNumber}...");
                }
                return iDeviceSerialNumber.ToString();
            }
            catch
            {
                log.Error("Error getting camera serial. Returning empty string.");
                return "";
            }
        }
        internal void DisableLed()
        {
            var ledEnum = NodeMap.GetNode<IEnum>("DeviceIndicatorMode");
            if (ledEnum == null || !ledEnum.IsReadable)
            {
                log.Warn("Cannot read DeviceIndicatorMode");
                return;
            }

            var ledEnumEntry = ledEnum.GetEntryByName("Inactive");
            if (!ledEnumEntry.IsAvailable || !ledEnumEntry.IsReadable)
            {
                log.Warn("Unable to set DeviceIndicatorMode to Inactive");
                return;
            }
            ledEnum.Value = ledEnumEntry.Value;
        }

        internal void ActivateLed()
        {
            var ledEnum = NodeMap.GetNode<IEnum>("DeviceIndicatorMode");
            if (ledEnum == null || !ledEnum.IsReadable)
            {
                log.Warn("Cannot read DeviceIndicatorMode");
                return;
            }

            var ledEnumEntry = ledEnum.GetEntryByName("Active");
            if (!ledEnumEntry.IsAvailable || !ledEnumEntry.IsReadable)
            {
                log.Warn("Unable to set DeviceIndicatorMode to Active");
                return;
            }
            ledEnum.Value = ledEnumEntry.Value;
        }
    }
}
