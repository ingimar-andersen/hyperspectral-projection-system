﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using LaserDriver.VM;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using static LaserDriver.PatternGenerator;

namespace LaserDriver
{
    /// <summary>
    /// Interaction logic for PatternWindow.xaml
    /// </summary>
    public partial class PatternWindow : Window
    {
        public Image<Gray, float> HomogenizationImage;

        public string UserDefinedPatternFolder { get; set; }
        public int PatternHeight { get; set; }
        public int PatternWidth { get; set; }

        public ImageBox imageBox;
        private List<Image<Gray, float>> pattern = new List<Image<Gray, float>>();
        public List<Image<Gray, float>> Pattern
        {
            get { return pattern; }
            set
            {
                pattern = value;
                PatternIndex = 0;
            }
        }

        private int patternIndex = 0;
        public int PatternIndex
        {
            get { return patternIndex; }
            set
            {
                if(value >= 0 && value < pattern.Count)
                {
                    patternIndex = value;
                    imageBox.Image = pattern[patternIndex];
                }
                else
                {
                    throw new IndexOutOfRangeException("Pattern index out of range");
                }
            }
        }

        public PatternWindowVM ViewModel
        {
            get { return this.DataContext as PatternWindowVM; }
        }

        public PhaseShiftingParameters PhaseShiftingParams { get; internal set; }

        public bool IsHotkeysActive = true;

        public PatternWindow(PatternWindowVM viewModel, int patternWidth, int patternHeight, PhaseShiftingParameters phaseShiftingParams, string userDefinedPatternFolder = "", Image<Gray, float> homogenizationImage = null)
        {
            InitializeComponent();
            this.DataContext = viewModel;
            this.HomogenizationImage = homogenizationImage;
            this.UserDefinedPatternFolder = userDefinedPatternFolder;
            this.PatternHeight = patternHeight;
            this.PatternWidth = patternWidth;
            this.PhaseShiftingParams = phaseShiftingParams;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.ToggleWindow();
        }

        private void KeyHandler(object sender, KeyEventArgs e)
        {
            if (!IsHotkeysActive) return;

            if (Keyboard.Modifiers == ModifierKeys.Alt && Keyboard.IsKeyDown(Key.Enter))
            {
                this.ToggleWindow();
            }
            if (Keyboard.Modifiers == ModifierKeys.Alt && Keyboard.IsKeyDown(Key.Left))
            {
                PreviousPatternFrame();
            }
            if (Keyboard.Modifiers == ModifierKeys.Alt && Keyboard.IsKeyDown(Key.Right))
            {
                NextPatternFrame();
            }
        }

        public void NextPatternFrame()
        {
            patternIndex++;
            if (patternIndex >= pattern.Count)
            {
                patternIndex = 0;
            }
            imageBox.Image = pattern[patternIndex];
        }

        public void PreviousPatternFrame()
        {
            patternIndex--;
            if (patternIndex < 0)
            {
                patternIndex = pattern.Count - 1;
            }
            imageBox.Image = pattern[patternIndex];
        }

        public void ToggleWindow()
        {
            switch (this.WindowState)
            {
                case (WindowState.Maximized):
                    {
                        SetWindowToNormal();
                    }
                    break;

                default:
                    {
                        SetWindowToFullscreen();
                    }
                    break;
            }
        }

        private void SetWindowToFullscreen()
        {
            this.WindowState = WindowState.Maximized;
            this.WindowStyle = WindowStyle.None;
            ViewModel.IsToolbarVisible = false;
            this.Topmost = true;
        }

        private void SetWindowToNormal()
        {
            this.WindowState = WindowState.Normal;
            this.WindowStyle = WindowStyle.SingleBorderWindow;
            ViewModel.IsToolbarVisible = true;
            this.Topmost = false;
        }

        private void Titlebar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Close_Clicked(object sender, RoutedEventArgs e)
        {
            CloseApplication();
        }

        private void CloseApplication()
        {
            Application.Current.Shutdown();
        }

        private static System.Timers.Timer aTimer;
        private void SetTimer()
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(1000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            imageBox.Image = pattern[patternIndex];
            patternIndex++;
            if (patternIndex >= pattern.Count)
            {
                patternIndex = 0;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Create the interop host control.
            System.Windows.Forms.Integration.WindowsFormsHost host =
                new System.Windows.Forms.Integration.WindowsFormsHost
                {
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Top,
                    Width = HardwareSettings.PROJECTOR_WIDTH,
                    Height = HardwareSettings.PROJECTOR_HEIGHT,
                };
            host.Loaded += Host_Loaded;

            // Add the interop host control to the Grid
            // control's collection of child controls.
            this.formsHostGrid.Children.Add(host);
        }

        private void Host_Loaded(object sender, EventArgs e)
        {
            AddImageBoxToFormsHost();
            RefreshPattern();
        }

        private void AddImageBoxToFormsHost()
        {
            imageBox = new ImageBox
            {
                FunctionalMode = ImageBox.FunctionalModeOption.Minimum,
                Width = PatternWidth,
                Height = PatternHeight,
            };
            imageBox.MouseEnter += Pattern_MouseEnter;
            imageBox.MouseLeave += Pattern_MouseLeave;
            imageBox.Width = HardwareSettings.PROJECTOR_WIDTH;
            imageBox.Height = HardwareSettings.PROJECTOR_HEIGHT;
            imageBox.Location = GetPatternOutputOffset();
            var host = (this.formsHostGrid.Children[0] as System.Windows.Forms.Integration.WindowsFormsHost);
            host.Child = imageBox;
        }

        public void RefreshPattern()
        {
            var imageBox = (this.formsHostGrid.Children[0] as System.Windows.Forms.Integration.WindowsFormsHost).Child as ImageBox;
            imageBox.Location = GetPatternOutputOffset();
            
            if (pattern.Any())
            {
                pattern.ForEach(x => x.Dispose());
            }
            pattern = GetPatternFromPatternType(ViewModel.PatternType, PatternWidth, PatternHeight, UserDefinedPatternFolder, ViewModel.IsRotated45, HomogenizationImage, PhaseShiftingParams);

            if (HomogenizationImage != null)
            {
                var projectorSize = new System.Drawing.Size(int.Parse(ViewModel.WindowWidth), int.Parse(ViewModel.WindowHeight));
                if (HomogenizationImage.Size != projectorSize)
                {
                    MessageBox.Show("Size of intensity correcion mask does not fit with projector size.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    var upperLeftLocation = new System.Drawing.Point(
                        (int)Math.Round((projectorSize.Width - PatternWidth) / 2.0),
                        (int)Math.Round((projectorSize.Height - PatternHeight) / 2.0)
                        );
                    var correctedPattern = new List<Image<Gray, float>>();
                    foreach (var img in pattern)
                    {
                        if (img.Size.Width > HomogenizationImage.Width && img.Size.Height > HomogenizationImage.Height)
                        {
                            MessageBox.Show("Intensity correction mask smaller than pattern. Aborting.");
                            return;
                        }

                        Image<Gray, float> correctedImg = null;
                        correctedImg = img.Mul(HomogenizationImage.GetSubRect(new Rectangle(upperLeftLocation, img.Size)));

                        correctedPattern.Add(correctedImg);
                        img.Dispose();
                    }

                    pattern.ForEach(x => x.Dispose());
                    pattern = correctedPattern;
                }
            }
            //Display the first pattern image
            PatternIndex = 0;
        }

        private System.Drawing.Point GetPatternOutputOffset()
        {
            var xTopLeft = (int)Math.Round((HardwareSettings.PROJECTOR_WIDTH - PatternWidth) / 2.0);
            var yTopLeft = (int)Math.Round((HardwareSettings.PROJECTOR_HEIGHT - PatternHeight) / 2.0);
            return new System.Drawing.Point(xTopLeft, yTopLeft);
        }

        private void Pattern_MouseLeave(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Show(); //The pattern is in a Windows Forms container
        }

        private void Pattern_MouseEnter(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Hide(); //The pattern is in a Windows Forms container
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            var isFullscreen = ViewModel.IsToolbarVisible == false;
            if (isFullscreen)
            {
                Window window = (Window)sender;
                window.Topmost = true;
                window.Activate();
            }
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            var isFullscreen = ViewModel.IsToolbarVisible == false;
            if (isFullscreen)
            {
                this.Topmost = true;
                this.Top = 0;
                this.Left = 0;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            HomogenizationImage?.Dispose();
            if (Pattern.Any())
            {
                Pattern.ForEach(x => x.Dispose());
            }
        }
    }
}
