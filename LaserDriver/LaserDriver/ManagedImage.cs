﻿using log4net;
using SpinnakerNET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LaserDriver
{
    public class ManagedImage : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IManagedImage Image;
        public string CameraId;
        public string CameraSerial;

        public ManagedImage(IManagedImage image, string cameraId, string cameraSerial)
        {
            this.Image = image;
            this.CameraId = cameraId;
            this.CameraSerial = cameraSerial;
        }

        public void Dispose()
        {
            try
            {
                Image.Dispose();
            }
            catch (Exception ex)
            {
                log.Error($"Error disposing image: {ex.Message}");
            }
        }

        public Task<string> Save(string outputFolderPath, string id, bool exportChunkData = true)
        {
            //return Task.Factory.StartNew(() => {

            //}, null, TaskCreationOptions.None, PriorityScheduler.BelowNormal);
            return Task<string>.Run<string>(() =>
            {
                string outputFilePath = "";
                try
                {
                    if (!Directory.Exists(outputFolderPath))
                    {
                        Directory.CreateDirectory(outputFolderPath);
                    }
                    // Create a unique filename
                    var filename = CameraId != "" ? CameraId : CameraSerial;
                    filename = $"{filename}_{id}.png";
                    outputFilePath = Path.Combine(outputFolderPath, filename);

                    var formatOption = new PngOption { interlaced = true, compressionLevel = 3 }; //CompressionLevel 0-9
                                                                                                  //var formatOption = new TiffOption { compression = TiffOption.CompressionMethod.None };
                    Image.Save(outputFilePath, formatOption);
                    log.Info($"Image saved at {outputFilePath}");
                    if (exportChunkData)
                    {
                        ExportChunkData(outputFolderPath, filename);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("Error saving image.", ex);
                    MessageBox.Show($"Error saving file: {ex.Message}");
                }
                finally
                {
                    Dispose();
                }
                return outputFilePath;
            });
        }

        public int ExportChunkData(string outputPath, string imageFilename)
        {
            return ExportChunkData(Image, outputPath, imageFilename);
        }

        public static int ExportChunkData(IManagedImage managedImage, string outputPath, string imageFilename)
        {
            int result = 0;

            log.Info("Exporting chunk data from image...");

            try
            {
                if (!Directory.Exists(outputPath))
                {
                    Directory.CreateDirectory(outputPath);
                }
                var chunkdataFilename = Path.GetFileNameWithoutExtension(imageFilename) + "_chunkdata.txt";
                var chunkdataFilePath = Path.Combine(outputPath, chunkdataFilename);
                using (var sw = new StreamWriter(chunkdataFilePath, append: true))
                {
                    ManagedChunkData managedChunkData = managedImage.ChunkData;
                    double exposureTime = managedChunkData.ExposureTime;
                    sw.WriteLine("Exposure time: \t{0} [us]", exposureTime);

                    long frameID = managedChunkData.FrameID;
                    sw.WriteLine("Frame ID: \t{0}", frameID);

                    // Retrieve gain; gain recorded in decibels
                    double gain = managedChunkData.Gain;
                    sw.WriteLine("Gain: \t{0} [dB]", gain);

                    // Retrieve width; width recorded in pixels
                    long width = managedChunkData.Width;
                    sw.WriteLine("Width: \t{0}", width);

                    // Retrieve height; height recorded in pixels
                    long height = managedChunkData.Height;
                    sw.WriteLine("Height: \t{0}", height);

                    // Retrieve offset X; offset X recorded in pixels
                    long offsetX = managedChunkData.OffsetX;
                    sw.WriteLine("Offset X: \t{0}", offsetX);

                    // Retrieve offset Y; offset Y recorded in pixels
                    long offsetY = managedChunkData.OffsetY;
                    sw.WriteLine("Offset Y: \t{0}", offsetY);

                    // Retrieve sequencer set active
                    long sequencerSetActive = managedChunkData.SequencerSetActive;
                    sw.WriteLine("Sequencer set active: \t{0}", sequencerSetActive);

                    // Retrieve timestamp
                    long timestamp = managedChunkData.Timestamp;
                    sw.WriteLine("Timestamp: \t{0}", timestamp);

                    sw.WriteLine();
                }
            }
            catch (Exception ex)
            {
                log.Error("Error exporting chunk data.", ex);
                result = -1;
            }

            return result;
        }
    }
}
