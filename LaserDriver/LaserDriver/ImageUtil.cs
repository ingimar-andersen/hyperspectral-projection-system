﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using SpinnakerNET;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace LaserDriver
{
    public class ImageUtil
    {
        public static Bitmap BitmapFromManagedImage(IManagedImage image)
        {
            if (image.PixelFormat == PixelFormatEnums.RGB8) {
                return new Bitmap((int)image.Width, (int)image.Height, (int)image.Stride, PixelFormat.Format24bppRgb, image.DataPtr);
            }
            else if(image.PixelFormat == PixelFormatEnums.Mono8 || image.PixelFormat == PixelFormatEnums.Mono16)
            {
                var converted = image.Convert(PixelFormatEnums.RGB8); //If it's stupid but it works, it's not stupid
                return new Bitmap((int)converted.Width, (int)converted.Height, (int)converted.Stride, PixelFormat.Format24bppRgb, converted.DataPtr);
            }
            throw new NotSupportedException($"BitmapFromManagedImage: PixelFormat '{image.PixelFormat.ToString()}' not supported");
        }

        public static Image<TColor, TDepth> ImageFromManagedImage<TColor, TDepth>(IManagedImage managedImage)
            where TColor : struct, IColor
            where TDepth : new()
        {
            using (var bitmap = BitmapFromManagedImage(managedImage))
            {
                var openCvImg = new Image<TColor, TDepth>(bitmap);

                if (managedImage.PixelFormat != PixelFormatEnums.Mono16 && managedImage.PixelFormat != PixelFormatEnums.Mono8)
                {
                    //Convert RGB -> BGR. It is not enough to define it in "new Image<Bgr, byte>"
                    var rgb = openCvImg.Split();
                    CvInvoke.Merge(new VectorOfMat(rgb[2].Mat, rgb[1].Mat, rgb[0].Mat), openCvImg);
                }
              
                return openCvImg;
            }
        }
    }
}
