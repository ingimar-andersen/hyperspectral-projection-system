﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LaserDriver
{
    /// <summary>
    /// Interaction logic for SplashScreenWindow.xaml
    /// </summary>
    public partial class SplashScreenWindow : Window
    {
        
        public SplashScreenWindow()
        {
            InitializeComponent();
            CenterWindowOnScreen();
            this.DataContext = new SplashScreenWindowVM();
            //(this.DataContext as SplashScreenWindowVM).GifPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Media", "Spiral-Loop.gif");
            (this.DataContext as SplashScreenWindowVM).GifPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Media", "laser_loading.png");
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Closing -= Window_Closing;
            e.Cancel = true;
            var anim = new DoubleAnimation(0, (Duration)TimeSpan.FromMilliseconds(650));
            anim.Completed += (s, _) => this.Close();
            this.BeginAnimation(UIElement.OpacityProperty, anim);
        }

    }

    class SplashScreenWindowVM : INotifyPropertyChanged
    {
        private string gifPath;
        public string GifPath
        {
            get { return this.gifPath; }
            set
            {
                if (this.gifPath != value)
                {
                    this.gifPath = value;
                    this.NotifyPropertyChanged("GifPath");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
