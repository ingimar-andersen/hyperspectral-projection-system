﻿using HW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver
{
    public static class ArduinoHelper
    {
        public static void TurnOnEnclosureLight(byte value)
        {
            Arduino.Instance.AnalogWrite(HardwareSettings.ENCLOSURE_LIGHT_POWER, value);
        }

        public static void TurnOffEnclosureLight()
        {
            Arduino.Instance.DigitalWrite(HardwareSettings.ENCLOSURE_LIGHT_POWER, false);
        }

        public static void TurnOnScanningLight()
        {
            Arduino.Instance.DigitalWrite(HardwareSettings.SCANNING_LIGHT_POWER, true);
        }

        public static void TurnOffScanningLight()
        {
            Arduino.Instance.DigitalWrite(HardwareSettings.SCANNING_LIGHT_POWER, false);
        }
    }
}
