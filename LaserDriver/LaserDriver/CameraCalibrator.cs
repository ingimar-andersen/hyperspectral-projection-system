﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using LaserDriver.Extensions;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace LaserDriver
{
    public class CameraCalibrator
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public class FindChessboardCornersAsyncResult
        {
            public int Id;
            public PointF[] CornerPoints;

            public FindChessboardCornersAsyncResult(int id, PointF[] cornerPoints)
            {
                this.Id = id;
                this.CornerPoints = cornerPoints;
            }
        }

        public class CalibrateCameraAsyncResult
        {
            public Matrix<double> CameraMatrix;
            public Matrix<double> DistCoeffs;
            public double ReprojectionError;
            public CalibrateCameraAsyncResult(Matrix<double> cameraMatrix, Matrix<double> distCoeffs, double reprojectionError)
            {
                this.CameraMatrix = cameraMatrix;
                this.DistCoeffs = distCoeffs;
                this.ReprojectionError = reprojectionError;
            }
        }

        private static readonly string[] allowedImageExtensions = { ".tiff", ".png", ".bmp", ".tif", ".pbm", ".pgm", ".ppm" };
        internal async static Task CalibrateCamerasWithIndividualIntrinsics(string leftCameraCalibrationFolder, string rightCameraCalibrationFolder, double squareSizeMm, System.Drawing.Size chessboardGridsize, string outputFolderPath, bool useOpenCv)
        {
            await Task.Run(async () =>
            {
                try
                {
                    Thread.CurrentThread.Priority = ThreadPriority.Highest;
                    var sw = new Stopwatch();
                    sw.Start();
                    var timestamp = DateTime.Now.ToString("yy-MM-dd_HH-mm-ss");
                    var metadataFilePath = Path.Combine(outputFolderPath, $"metadata_{timestamp}.txt");
                    using (var metadataSW = new StreamWriter(metadataFilePath, append: true))
                    {
                        metadataSW.WriteLine($"Started: {timestamp}");
                        metadataSW.WriteLine($"Input parameters:");
                        metadataSW.WriteLine($"leftCameraCalibrationFolder: {leftCameraCalibrationFolder}");
                        metadataSW.WriteLine($"rightCameraCalibrationFolder: {rightCameraCalibrationFolder}");
                        metadataSW.WriteLine($"squareSizeMm: {squareSizeMm}");
                        metadataSW.WriteLine();

                        var leftDirInfo = new DirectoryInfo(leftCameraCalibrationFolder);
                        var leftImages = leftDirInfo.GetFilesByExtensions(allowedImageExtensions);

                        var rightDirInfo = new DirectoryInfo(rightCameraCalibrationFolder);
                        var rightImages = rightDirInfo.GetFilesByExtensions(allowedImageExtensions);

                        if (!VerifyForCalibrationWithIntrinsics(leftImages, rightImages)) return;

                        Matrix<double> cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, r, t;

                        var stereoParamsOutputPath = Path.Combine(outputFolderPath, $"stereoParams_{timestamp}.mat");

                        var leftImagesFilepaths = leftImages.Select(x => x.FullName).ToArray();
                        var rightImagesFilepaths = rightImages.Select(x => x.FullName).ToArray();

                        var tempImage = new Image<Gray, float>(leftImagesFilepaths.First());
                        var imageSize = new System.Drawing.Size(tempImage.Width, tempImage.Height);
                        tempImage.Dispose();

                        long calibrationTimeMs = 0;
                        //Matlab
                        if (!useOpenCv)
                        {
                            var matlab = new Matlab();
                            var stereoCalibrationResult = matlab.StereoCalibrate(leftImagesFilepaths, rightImagesFilepaths, squareSizeMm, stereoParamsOutputPath);
                            cameraMatrix1 = stereoCalibrationResult.CameraMatrixLeft;
                            cameraMatrix2 = stereoCalibrationResult.CameraMatrixRight;
                            distCoeffs1 = stereoCalibrationResult.DistCoeffsLeft;
                            distCoeffs2 = stereoCalibrationResult.DistCoeffsRight;
                            r = stereoCalibrationResult.R;
                            t = stereoCalibrationResult.T;

                            calibrationTimeMs = sw.ElapsedMilliseconds;
                            metadataSW.WriteLine("Matlab calibration parameters:");
                            metadataSW.WriteLine($"calibrationTimeMs: {calibrationTimeMs}");
                        }
                        //OpenCV
                        else
                        {
                            var imageCount = leftImagesFilepaths.Length;
                            
                            MCvPoint3D32f[] chessboardWorldCoordinates = CreateChessboardWorldCoordinates(squareSizeMm, chessboardGridsize);
                            var cornersObjectList = CreateObjectList(chessboardWorldCoordinates, imageCount);

                            //var singleCameraFlags = CalibType.FixAspectRatio | CalibType.FixK2 | CalibType.FixK3 | CalibType.ZeroTangentDist | CalibType.FixPrincipalPoint; //Parameters from Seema-scanner
                            //var singleCameraFlags = CalibType.FixAspectRatio | CalibType.FixK2 | CalibType.FixK3 | CalibType.ZeroTangentDist;
                            var singleCameraFlags = CalibType.FixAspectRatio | CalibType.FixK3 | CalibType.ZeroTangentDist | CalibType.FixPrincipalPoint;
                            
                            var singleCameraTerminationCriteria = new MCvTermCriteria(100);

                            //Start chessboard corner search for both cameras
                            var startFindChessBoardCornersAsync = sw.ElapsedMilliseconds;
                            log.Info($"Chessboards started: {DateTime.Now.ToString("HH-mm-ss")}");
                            var chessboardTasksL = FindChessBoardCornersAsync(leftImagesFilepaths, chessboardGridsize);
                            var chessboardTasksR = FindChessBoardCornersAsync(rightImagesFilepaths, chessboardGridsize);
                            PointF[][] cornersPointListL = await chessboardTasksL;
                            PointF[][] cornersPointListR = await chessboardTasksR;
                            metadataSW.WriteLine($"FindChessBoardCornersAsync (L + R) execution time: {sw.ElapsedMilliseconds - startFindChessBoardCornersAsync}");

                            var startCalibrateSingleCameraAsync = sw.ElapsedMilliseconds;

                            //Left camera calibration
                            bool[] foundCornersL = cornersPointListL.Select(x => x != null).ToArray();
                            var filteredCornersL = FilterFoundCornerLists(cornersPointListL, foundCornersL);
                            var cornersObjectListL = CreateObjectList(chessboardWorldCoordinates, filteredCornersL.Length);
                            var calibrationTaskLeft = CalibrateSingleCameraAsync(imageSize, cornersObjectListL, singleCameraFlags, singleCameraTerminationCriteria, filteredCornersL);

                            //Right camera calibration
                            bool[] foundCornersR = cornersPointListR.Select(x => x != null).ToArray();
                            var filteredCornersR = FilterFoundCornerLists(cornersPointListR, foundCornersR);
                            var cornersObjectListR = CreateObjectList(chessboardWorldCoordinates, filteredCornersR.Length);
                            var calibrationTaskRight = CalibrateSingleCameraAsync(imageSize, cornersObjectListR, singleCameraFlags, singleCameraTerminationCriteria, filteredCornersR);

                            //Output data Left
                            var calibrationL = await calibrationTaskLeft;
                            cameraMatrix1 = calibrationL.CameraMatrix;
                            distCoeffs1 = calibrationL.DistCoeffs;
                            cameraMatrix1.ExportToFile(Path.Combine(outputFolderPath, $"cameraMatrix1_{ timestamp}.txt"));
                            distCoeffs1.ExportToFile(Path.Combine(outputFolderPath, $"distCoeffs1_{ timestamp}.txt"));

                            //Output data Right
                            var calibrationR = await calibrationTaskRight;
                            cameraMatrix2 = calibrationR.CameraMatrix;
                            distCoeffs2 = calibrationR.DistCoeffs;
                            cameraMatrix2.ExportToFile(Path.Combine(outputFolderPath, $"cameraMatrix2_{ timestamp}.txt"));
                            distCoeffs2.ExportToFile(Path.Combine(outputFolderPath, $"distCoeffs2_{ timestamp}.txt"));

                            metadataSW.WriteLine($"CalibrateSingleCameraAsync (L + R) execution time: {sw.ElapsedMilliseconds - startCalibrateSingleCameraAsync}");

                            var startStereoCalibrate = sw.ElapsedMilliseconds;
                            //Removes point and object lists not found in both cameras
                            FilterFoundCornerLists(ref cornersObjectList, ref cornersObjectList, ref cornersPointListL, foundCornersL, ref cornersPointListR, foundCornersR);

                            //TODO
                            var rMat = new Mat();
                            var tMat = new Mat();
                            var e = new Mat();
                            var f = new Mat();
                            var stereoCameraFlags = CalibType.FixIntrinsic;
                            var stereoTerminationCriteria = new MCvTermCriteria(200);
                            CvInvoke.StereoCalibrate(cornersObjectList, cornersPointListL, cornersPointListR, calibrationL.CameraMatrix.Mat, calibrationL.DistCoeffs.Mat, calibrationR.CameraMatrix.Mat, calibrationR.DistCoeffs.Mat, imageSize, rMat, tMat, e, f, stereoCameraFlags, stereoTerminationCriteria);

                            r = new Matrix<double>(rMat.ToDoubleArray());
                            t = new Matrix<double>(tMat.ToDoubleArray());
                            r.ExportToFile(Path.Combine(outputFolderPath, $"r_{timestamp}.txt"));
                            t.ExportToFile(Path.Combine(outputFolderPath, $"t_{timestamp}.txt"));
                            metadataSW.WriteLine($"StereoCalibrate execution time: {sw.ElapsedMilliseconds - startStereoCalibrate}");
                            calibrationTimeMs = sw.ElapsedMilliseconds;
                            metadataSW.WriteLine("OpenCV calibration parameters:");
                            metadataSW.WriteLine($"calibrationTimeMs: {calibrationTimeMs}");
                            metadataSW.WriteLine($"singleCameraFlags: {singleCameraFlags}");
                            metadataSW.WriteLine($"stereoCameraFlags: {stereoCameraFlags}");
                            metadataSW.WriteLine($"singleCameraTerminationCriteria: MaxIter = {singleCameraTerminationCriteria.MaxIter}, Epsilon = {singleCameraTerminationCriteria.Epsilon}");
                            metadataSW.WriteLine($"stereoTerminationCriteria: MaxIter = {stereoTerminationCriteria.MaxIter}, Epsilon = {stereoTerminationCriteria.Epsilon}");
                            metadataSW.WriteLine($"chessboardCornerGridSize: {chessboardGridsize.Width}x{chessboardGridsize.Height} (WxH)");
                            metadataSW.WriteLine($"reprojectionErrorL: {calibrationL.ReprojectionError}");
                            metadataSW.WriteLine($"reprojectionErrorR: {calibrationR.ReprojectionError}");
                        }

                        var startStereoRectify = sw.ElapsedMilliseconds;

                        var scaling = -1;

                        //output 
                        var r1 = new Mat();
                        var r2 = new Mat();
                        var p1 = new Mat();
                        var p2 = new Mat();
                        var q = new Mat();
                        var validPixRoi1 = new Rectangle();
                        var validPixRoi2 = new Rectangle();

                        CvInvoke.StereoRectify(cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, imageSize, r, t, r1, r2, p1, p2, q, StereoRectifyType.Default, scaling, imageSize, ref validPixRoi1, ref validPixRoi2);
                        metadataSW.WriteLine($"StereoRectify execution time: {sw.ElapsedMilliseconds - startStereoRectify}");

                        validPixRoi1.ExportToFile(Path.Combine(outputFolderPath, $"roi1_{timestamp}.txt"));
                        validPixRoi2.ExportToFile(Path.Combine(outputFolderPath, $"roi2_{timestamp}.txt"));
                        var map1x = new Mat();
                        var map1y = new Mat();
                        CvInvoke.InitUndistortRectifyMap(cameraMatrix1, distCoeffs1, r1, p1, imageSize, DepthType.Cv32F, map1x, map1y);

                        var map2x = new Mat();
                        var map2y = new Mat();
                        CvInvoke.InitUndistortRectifyMap(cameraMatrix2, distCoeffs2, r2, p2, imageSize, DepthType.Cv32F, map2x, map2y);

                        //Export results
                        r1.ExportToFile(Path.Combine(outputFolderPath, $"r1_{timestamp}.txt"));
                        r2.ExportToFile(Path.Combine(outputFolderPath, $"r2_{timestamp}.txt"));
                        p1.ExportToFile(Path.Combine(outputFolderPath, $"p1_{timestamp}.txt"));
                        p2.ExportToFile(Path.Combine(outputFolderPath, $"p2_{timestamp}.txt"));

                        //Json conversion preferred in basic datatypes that can also be parsed in Matlab
                        var jsonStart = sw.ElapsedMilliseconds;
                        map1x.ToDoubleArray().ExportToJsonFile(Path.Combine(outputFolderPath, $"map1x_{timestamp}.json"));
                        map1y.ToDoubleArray().ExportToJsonFile(Path.Combine(outputFolderPath, $"map1y_{timestamp}.json"));
                        map2x.ToDoubleArray().ExportToJsonFile(Path.Combine(outputFolderPath, $"map2x_{timestamp}.json"));
                        map2y.ToDoubleArray().ExportToJsonFile(Path.Combine(outputFolderPath, $"map2y_{timestamp}.json"));
                        metadataSW.WriteLine($"Save .json files execution time: {sw.ElapsedMilliseconds - jsonStart}");

                        Utils.TryOpenFolder(outputFolderPath);
                        Utils.PlaySuccessSound();
                        metadataSW.WriteLine($"Total execution time: {sw.ElapsedMilliseconds}");
                    }
                }
                catch (Exception ex)
                {
                    var msg = $"Error during camera calibration.";
                    log.Error(msg, ex);
                    MessageBox.Show(msg);
                }
                finally
                {
                    Thread.CurrentThread.Priority = ThreadPriority.Normal;
                }
            });
        }

        private static async Task<CalibrateCameraAsyncResult> CalibrateSingleCameraAsync(System.Drawing.Size imageSize, MCvPoint3D32f[][] cornersObjectList, CalibType singleCameraFlags, MCvTermCriteria singleCameraTerminationCriteria, PointF[][] cornersPointListL)
        {
            return await Task.Run(() =>
            {
                try
                {
                    Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;
                    return CalibrateSingleCamera(imageSize, cornersObjectList, singleCameraFlags, singleCameraTerminationCriteria, cornersPointListL);
                }
                finally
                {
                    Thread.CurrentThread.Priority = ThreadPriority.Normal;
                }
            });
        }

        private static CalibrateCameraAsyncResult CalibrateSingleCamera(System.Drawing.Size imageSize, MCvPoint3D32f[][] cornersObjectList, CalibType singleCameraFlags, MCvTermCriteria singleCameraTerminationCriteria, PointF[][] cornersPointList)
        {
            var cameraMatrix1Mat = new Mat();
            var distCoeffs1Mat = new Mat();
            var reprojectionError = CvInvoke.CalibrateCamera(cornersObjectList, cornersPointList, imageSize, cameraMatrix1Mat, distCoeffs1Mat, singleCameraFlags, singleCameraTerminationCriteria, out Mat[] rot1, out Mat[] trans1);
            var cameraMatrix = new Matrix<double>(cameraMatrix1Mat.ToDoubleArray());
            var distCoeffs = new Matrix<double>(distCoeffs1Mat.ToDoubleArray());
            return new CalibrateCameraAsyncResult(cameraMatrix, distCoeffs, reprojectionError);
        }

        private static MCvPoint3D32f[][] CreateObjectList(MCvPoint3D32f[] chessboardWorldCoordinates, int size)
        {
            var result = new MCvPoint3D32f[size][];
            for (int i = 0; i< size; i++)
            {
                result[i] = chessboardWorldCoordinates;
            }
            return result;
        }

        private static MCvPoint3D32f[] CreateChessboardWorldCoordinates(double squareSizeMm, System.Drawing.Size chessboardPatternSize)
        {
            List<MCvPoint3D32f> object_list = new List<MCvPoint3D32f>();
            for (int i = 0; i < chessboardPatternSize.Height; i++)
            {
                for (int j = 0; j < chessboardPatternSize.Width; j++)
                {
                    object_list.Add(new MCvPoint3D32f(j * (float)squareSizeMm, i * (float)squareSizeMm, 0.0F));
                }
            }

            return object_list.ToArray();
        }

        private static async Task<PointF[][]> FindChessBoardCornersAsync(string[] imagesFilepaths, System.Drawing.Size chessboardPatternSize)
        {
            // LEFT CAMERA
            var imageIndex = 0;
            var tasks = new List<Task<FindChessboardCornersAsyncResult>>();
            foreach (var imgPath in imagesFilepaths)
            {
                var id = imageIndex;
                var imagePath = imgPath;
                tasks.Add(new Task<FindChessboardCornersAsyncResult>(() => {
                    try
                    {
                        log.Info($"Task {id} started {DateTime.Now.ToString("HH-mm-ss")}");
                        Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;
                        using (var image = new Image<Gray, byte>(imagePath))
                        {
                            var cornerPoints = new VectorOfPointF();
                            var found = CvInvoke.FindChessboardCorners(image, chessboardPatternSize, cornerPoints, CalibCbType.AdaptiveThresh | CalibCbType.FastCheck);
                            if (found)
                            {
                                CvInvoke.CornerSubPix(image, cornerPoints, new System.Drawing.Size(6, 6), new System.Drawing.Size(1, 1), new MCvTermCriteria(20, 0.0001));
                                return new FindChessboardCornersAsyncResult(id, cornerPoints.ToArray());
                            }
                            else
                            {
                                return new FindChessboardCornersAsyncResult(id, null);
                            }
                        }
                    }
                    finally
                    {
                        Thread.CurrentThread.Priority = ThreadPriority.Normal;
                        log.Info($"Task {id} finished {DateTime.Now.ToString("HH-mm-ss")}");
                    }
                    
                }));
                imageIndex++;
            }
            tasks.ForEach(x => x.Start());
            var results = await Task.WhenAll(tasks.ToArray());
            var orderedResult = new PointF[imagesFilepaths.Length][];
            foreach (var r in results)
            {
                orderedResult[r.Id] = r.CornerPoints;
            }
            return orderedResult;
        }

        private static PointF[][] FilterFoundCornerLists(PointF[][] cornersPointList, bool[] foundCorners)
        {
            //Some corners not found
            if (foundCorners.Any(x => !x))
            {
                var filteredSize = foundCorners.Count(x => x == true);

                if (filteredSize == foundCorners.Length) return cornersPointList; //No filtering neccessary

                var filteredCorners = new PointF[filteredSize][];
                var filteredIndex = 0;
                for (int i = 0; i < foundCorners.Length; i++)
                {
                    if (foundCorners[i])
                    {
                        filteredCorners[filteredIndex] = cornersPointList[i];
                        filteredIndex++;
                    }
                }

                //Update lists
                return filteredCorners;
            }
            return cornersPointList;
        }

        private static void FilterFoundCornerLists(ref MCvPoint3D32f[][] corners_object_list_L, ref MCvPoint3D32f[][] corners_object_list_R, ref PointF[][] corners_point_list_L, bool[] foundCorners1, ref PointF[][] corners_point_list_R, bool[] foundCorners2)
        {
            //Some corners not found
            if (foundCorners1.Any(x => !x) || foundCorners2.Any(x => !x))
            {
                var bothHaveCorners = foundCorners1.And(foundCorners2);
                var filteredSize = bothHaveCorners.Count(x => x == true);

                if (filteredSize == foundCorners1.Length) return; //No filtering neccessary

                var filteredCornersL = new PointF[filteredSize][];
                var filteredCornersR = new PointF[filteredSize][];
                var filteredObjectList = new MCvPoint3D32f[filteredSize][];
                var filteredIndex = 0;
                for (int i = 0; i < bothHaveCorners.Length; i++)
                {
                    if (bothHaveCorners[i])
                    {
                        filteredCornersL[filteredIndex] = corners_point_list_L[i];
                        filteredCornersR[filteredIndex] = corners_point_list_R[i];
                        filteredObjectList[filteredIndex] = corners_object_list_R[i];
                        filteredIndex++;
                    }
                }

                //Update lists
                corners_point_list_L = filteredCornersL;
                corners_point_list_R = filteredCornersR;
                corners_object_list_L = filteredObjectList;
                corners_object_list_R = filteredObjectList;
            }
        }

        public async static Task RectifyImagesFromFolder(string sourcePath, string destinationPath, IInputArray xMap, IInputArray yMap) {
            var sourceDirInfo = new DirectoryInfo(sourcePath);
            var images = sourceDirInfo.GetFilesByExtensions(allowedImageExtensions);

            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }
            var tasks = new List<Task>();
            foreach (var img in images)
            {
                var name = img.Name;
                var fullName = img.FullName;
                var task = Task.Run(() => {
                    try
                    {
                        Thread.CurrentThread.Priority = ThreadPriority.AboveNormal;
                        using (var image = new Image<Gray, byte>(img.FullName))
                        using (var output = new Image<Gray, byte>(image.Size.Width, image.Size.Height, new Gray(byte.MinValue)))
                        {
                            CvInvoke.Remap(image, output, xMap, yMap, Inter.Linear);
                            output.Save(Path.Combine(destinationPath, img.Name));
                        }
                    }
                    finally
                    {
                        Thread.CurrentThread.Priority = ThreadPriority.Normal;
                    }
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks.ToArray());
        }

        private static bool VerifyForCalibrationWithIntrinsics(IEnumerable<FileInfo> leftImages, IEnumerable<FileInfo> rightImages)
        {
            if (rightImages.Count() != leftImages.Count())
            {
                var msg = "Left and right images must be equally many";
                log.Error(msg);
                MessageBox.Show(msg, "", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (rightImages.Count() == 0 || leftImages.Count() == 0)
            {
                var msg = "Folders contain no images";
                log.Error(msg);
                MessageBox.Show(msg, "", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            var rightExtensions = rightImages.Select(x => x.Extension).Distinct();
            var leftExtensions = leftImages.Select(x => x.Extension).Distinct();
            if (rightExtensions.Count() > 1 || leftExtensions.Count() > 1 || rightExtensions.First() != leftExtensions.First())
            {
                var msg = "Images must be of same type";
                log.Error(msg);
                MessageBox.Show(msg, "", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }
    }
}
