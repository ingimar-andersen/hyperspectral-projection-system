﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using HW;
using LaserDriver.Dialogs;
using LaserDriver.Plots;
using LaserDriver.VM;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Threading;

namespace LaserDriver
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Laser laser;
        private Camera camera;

        private List<DeviceStatus> devicesStatus = new List<DeviceStatus>();

        private PatternWindow patternWindow = null;
        private VariableExposurePlot variableExposureGraph = null;

        private LaserControlVM ViewModel
        {
            get { return this.DataContext as LaserControlVM; }
        }

        public string OutputFolderPath = Properties.Settings.Default.OutputFolder;
        //public static readonly string wavelengthToExposuretimeFactorFilename = "wavelength_exposuretime_compensation_feedback.txt";
        public static readonly string wavelengthToExposuretimeFactorFilename = "wavelength_exposuretime_compensation_grayscale.txt";
        public static Dictionary<int, double> exposuretimeFactorByWavelength = new Dictionary<int, double>();


        public MainWindow()
        {
            log.Info("MainWindow opened.");
            InitializeComponent();
        }

        private const int WM_DEVICECHANGE = 0x219;
        private const int DBT_DEVICEARRIVAL = 0x8000;
        private const int DBT_DEVICEREMOVECOMPLETE = 0x8004;
        private const int DBT_DEVNODES_CHANGED = 0x0007;
        private const int DBT_DEVTYP_VOLUME = 0x00000002;
        private object usbEventLock = new object();
        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            lock (usbEventLock)
            {
                switch (msg)
                {
                    case WM_DEVICECHANGE:

                        //List of cases: https://docs.microsoft.com/en-us/windows/desktop/devio/wm-devicechange
                        switch ((int) wParam)
                        {
                            case DBT_DEVICEARRIVAL:
                                //listBox1.Items.Add("New Device Arrived");
                                log.Info("Added USB device");
                                int devType = Marshal.ReadInt32(lParam, 4);
                                if (devType != DBT_DEVTYP_VOLUME)
                                {
                                    handled = true;
                                    this.Dispatcher.Invoke(new Action(async () =>
                                    {
                                        if (camera.CamList.Count() < 2) //Only update camera if both cameras are not connected
                                        {
                                            camera = new Camera();
                                        }
                                        await Utils.WaitAsync(1000); //Necessary when disconnecting and reconnecting laser usb
                                        RefreshUi();
                                    }));
                                }

                                break;

                            case DBT_DEVICEREMOVECOMPLETE:
                                //listBox1.Items.Add("Device Removed");
                                log.Info("Removed USB device");
                                handled = true;
                                this.Dispatcher.Invoke(new Action(async () => {
                                    await Utils.WaitAsync(1000); //Necessary when disconnecting and reconnecting laser usb
                                    RefreshUi();
                                }));
                                break;

                            case DBT_DEVNODES_CHANGED:
                                handled = true;
                                this.Dispatcher.Invoke(new Action(async () =>
                                {
                                    if (camera.CamList.Count() < 2) //Only update camera if both cameras are not connected
                                    {
                                        camera = new Camera();
                                    }
                                    await Utils.WaitAsync(1000); //Necessary when disconnecting and reconnecting laser usb
                                    RefreshUi();
                                }));
                                break;
                            default:
                                log.Info($"Unhandled USB event. wParam: {(int) wParam}");
                                break;
                        }
                        break;
                }
            }

            return IntPtr.Zero;
        }

        private DispatcherTimer uiTimer;
        private void InitUiTimer()
        {
            uiTimer = new DispatcherTimer();
            uiTimer.Interval = new TimeSpan(0, 0, 0, 0, 1000);
            uiTimer.Tick += uiTimer_Tick;
            uiTimer.Start();
        }

        private async void uiTimer_Tick(object sender, EventArgs e)
        {
            if (!ViewModel.IsUiEnabled) return;
          
            ViewModel.LaserStatus = await GetLaserStatus();
            ViewModel.IsEmissionOn = await IsLaserEmissionOn();
            if (!ViewModel.LaserStatus.IsConnected)
            {
                var result = laser.Reconnect();

                if(result == NKTPDLL.PortResultTypes.OPSuccess)
                {
                    //Refresh UI values once reconnected
                    UpdateLaserUiValues();
                }
            }
        }

        private void ReadWavelengthToExposuretimeFactorMapping()
        {
            try
            {
                StreamReader reader = File.OpenText(Properties.Settings.Default.ExposureScalingFilepath);
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] items = line.Split('\t');
                    var wavelength = int.Parse(items[0].Trim(), NumberStyles.Float);
                    var exposuretimeFactor = double.Parse(items[1].Trim(), NumberStyles.Float, CultureInfo.InvariantCulture);
                    if (exposuretimeFactor < 1)
                    {
                        throw new Exception("Exposure time factor must be >= 1");
                    }
                    exposuretimeFactorByWavelength.Add(wavelength, exposuretimeFactor);
                }
            }
            catch (Exception ex)
            {
                exposuretimeFactorByWavelength.Clear();
                log.Error($"Unable to load wavelength to exposuretime mapping.", ex);
                MessageBox.Show("Unable to load wavelength to exposuretime mapping");
            }
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        private async Task FetchDevicesStatus()
        {
            devicesStatus = await GetDevicesStatus();
        }

        private void SetAmplitude_Click(object sender, RoutedEventArgs e)
        {
            SetAmplitudeFromUI();
        }

        private async void SetAmplitudeFromUI()
        {
            if (ViewModel.Amplitude != null)
            {
                if (laser.Select.Amplitude0.TrySetValue((double)ViewModel.Amplitude, laser.ComPort))
                {
                    ViewModel.LaserStatus = await GetLaserStatus();
                }
                else
                {
                    if (laser.Select.Amplitude0.TryGetStringValue(laser.ComPort, value: out string amplitudeTxt))
                    {
                        ViewModel.AmplitudeTxt = amplitudeTxt;
                    }
                }
            }
        }

        private void SetWavelength_Click(object sender, RoutedEventArgs e)
        {
            SetWavelengthFromUI();
        }

        private async void SetWavelengthFromUI()
        {
            if (ViewModel.Wavelength != null)
            {
                if (laser.Select.Wavelength0.TrySetValue((double)ViewModel.Wavelength, laser.ComPort))
                {
                    ViewModel.LaserStatus = await GetLaserStatus();
                }
                else
                {
                    if (laser.Select.Wavelength0.TryGetStringValue(laser.ComPort, out string wavelengthTxt))
                    {
                        ViewModel.WavelengthTxt = wavelengthTxt;
                    }
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            OnApplicationExit();
        }

        private void OnApplicationExit()
        {
            //Avoid UI conflict for timer
            ViewModel.IsUiEnabled = false;
            uiTimer.Stop();
            
            // Always turn off RF power and Emission on exit
            if (camera != null)
            {
                camera.Dispose();
            }
            if (patternWindow != null)
            {
                patternWindow.Close();
            }
            if (variableExposureGraph != null)
            {
                variableExposureGraph.Close();
            }
            TurnOffRfPower();
            TurnOffEmission();
            ArduinoHelper.TurnOffScanningLight();
            ArduinoHelper.TurnOffEnclosureLight();
        }

        private void Emission_Checked(object sender, RoutedEventArgs e)
        {
            TurnOnEmission();
        }

        private void Emission_Unchecked(object sender, RoutedEventArgs e)
        {
            TurnOffEmission();
        }

        private async void TurnOffEmission()
        {
            try
            {
                ViewModel.IsUiEnabled = false;
                await Task.Run(() =>
                {
                    var retryCount = 0;
                    while (retryCount < 5)
                    {
                        if (laser.Extreme.Emission.TrySetValueScaled(0, laser.ComPort))
                        {
                            break;
                        }
                        Thread.Sleep(100);
                        retryCount++;
                    }
                    Thread.Sleep(300);
                });
                ViewModel.LaserStatus = await GetLaserStatus();
            }
            catch (Exception ex)
            {
                log.Error($"Error turning off emission.", ex);
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
            }

            // TODO Handle bindings properly...
            //double emission;
            //if (laser.Extreme.Emission.TryGetValue(laser.ComPort, out emission))
            //{
            //    ViewModel.IsEmissionOn = emission == 3;
            //}
        }

        private async void TurnOnEmission()
        {
            try
            {
                ViewModel.IsUiEnabled = false;
                var retryCount = 0;
                while (retryCount < 5) { 
                    if (laser.Extreme.Emission.TrySetValueScaled(3, laser.ComPort))
                    {
                        break;
                    }
                    await Task.Run(() => Thread.Sleep(100));
                    retryCount++;
                }
                await Task.Run(() => Thread.Sleep(1500));
                ViewModel.IsEmissionOn = await IsLaserEmissionOn();
                ViewModel.LaserStatus = await GetLaserStatus();
            }
            catch (Exception ex)
            {
                log.Error($"Error turning on emission.", ex);
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
            }

            // TODO Handle bindings properly...
            //double emission;
            //if (laser.Extreme.Emission.TryGetValue(laser.ComPort, out emission))
            //{
            //    ViewModel.IsEmissionOn = emission == 3;
            //}
        }

        private void RfPower_Checked(object sender, RoutedEventArgs e)
        {
            TurnOnRfPower();
        }

        private void RfPower_Unchecked(object sender, RoutedEventArgs e)
        {
            TurnOffRfPower();
        }

        private async void TurnOnRfPower()
        {
            laser.Select.RfPower.TrySetValueScaled(1, laser.ComPort);
            ViewModel.LaserStatus = await GetLaserStatus();
        }

        private async void TurnOffRfPower()
        {
            laser.Select.RfPower.TrySetValueScaled(0, laser.ComPort);
            ViewModel.LaserStatus = await GetLaserStatus();
        }

        private void SetLaserPower_Click(object sender, RoutedEventArgs e)
        {
            SetLaserPowerFromUI();
        }

        private async void SetLaserPowerFromUI()
        {
            if (ViewModel.LaserPower != null)
            {
                if (laser.Extreme.PowerLevel.TrySetValue((double)ViewModel.LaserPower, laser.ComPort))
                {
                    ViewModel.LaserStatus = await GetLaserStatus();
                }
                else
                {
                    if (laser.Extreme.PowerLevel.TryGetStringValue(laser.ComPort, out string powerLevelTxt))
                    {
                        ViewModel.LaserPowerTxt = powerLevelTxt;
                    }
                }
            }
        }

        private async void StartMeasurement_Click(object sender, RoutedEventArgs e)
        {
            var success = false;
            if (ViewModel.IsScanAndReconstruct && ViewModel.IsPatternActive)
            {
                ScanAndReconstruct();
            }
            else
            {
                if (ViewModel.IsStepMode)
                {
                    success = await TryRunCameraSweep();
                }
                else
                {
                    success = await RunSingleCameraMeasurement();
                }

                if (success)
                {
                    Utils.PlaySuccessSound();
                    Utils.TryOpenFolder(OutputFolderPath);
                }
            }
        }

        private async Task<bool> RunSingleCameraMeasurement()
        {
            var isVerified = await IsVerifiedDeviceAndParameters();
            if (!isVerified) return false;

            var useSpectrometer = ViewModel.UseSpectrometer;
            var integrationTimeMs = (double)ViewModel.IntegrationTimeMs;
            var exposureTimeMs = (double)ViewModel.ExposureTimeMs;
            var wavelength = (double)ViewModel.Wavelength;
            var measurementCount = (int)ViewModel.MeasurementsNo;

            if (!TryGetOutputFolder())
            {
                return false; //Cancelled by user
            }
            try
            {
                HandleLightsBeforeScan();

                int id = 1;
                int idLength = 5;
                ExportMetadata(new List<double>() { wavelength }, id, idLength, useSpectrometer);
                var now = DateTime.Now;
                var idString = id.ToString().PadLeft(idLength, '0') + $"_{wavelength.ToString("N0")}nm" + now.ToString("HH-mm-ss");

                if (!laser.Select.Wavelength0.TrySetValue(wavelength, laser.ComPort))
                {
                    log.Warn("Could not set filter wavelength.");
                }
                if (useSpectrometer)
                {
                    RunSingleSpectrometerMeasurement(idString, integrationTimeMs, wavelength, measurementCount);
                }
                else
                {
                    camera.BeginAquisitionAll();
                    camera.SetExposureTimeForCameras(exposureTimeMs);

                    var imageSaveTasks = new List<Task>();
                    var averageCount = ViewModel.NumberOfImagesToAverage ?? 1;
                    if (averageCount == 1)
                    {
                        var result = await camera.AcquireSingleImageAllCameras(HardwareSettings.TRIGGER_TYPE);
                        var tasks = result.Select(x => x.Save(OutputFolderPath, idString));
                        imageSaveTasks.AddRange(tasks);
                    }
                    else
                    {
                        var imgByOutputPath = await GetAverageImagesByOutputPath<Bgr, Int32>(HardwareSettings.TRIGGER_TYPE, wavelength, idString, averageCount, createCameraFolders: true);
                        var tasks = GetSaveImagesTasks(imgByOutputPath);
                        tasks.ForEach(x => x.Start());
                        imageSaveTasks.AddRange(tasks);
                    }
                    await Task.WhenAll(imageSaveTasks.ToArray());
                }
                //Success
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                HandleLightsAfterScan();
                camera.EndAquisitionAll();
            }
        }

        private void HandleLightsAfterScan()
        {
            ArduinoHelper.TurnOnEnclosureLight(ViewModel.EnclosureLightIntensity);
            ArduinoHelper.TurnOffScanningLight();
        }

        private async Task<bool> IsVerifiedDeviceAndParameters()
        {
            var verificationResult = VerifyInputParameters(isSingleMeasurement: true);
            if (!verificationResult.Verified)
            {
                MessageBox.Show(verificationResult.Message);
                return false;
            }

            var devicesStatus = await GetDevicesStatus();

            //Laser must be connected
            //TODO Possible to bypass laser
            if (!devicesStatus.Any(x => x.Device == Device.Laser && x.IsReady && x.IsConnected))
            {
                //Spectrometer
                var spectrometer = devicesStatus.Single(x => x.Device == Device.Spectrometer);
                if (ViewModel.UseSpectrometer && !spectrometer.IsConnected)
                {
                    MessageBox.Show(spectrometer.Message);
                    return false;
                }
                //Cameras
                var cameras = devicesStatus.Single(x => x.Device == Device.Cameras);
                if (!ViewModel.UseSpectrometer && !cameras.IsConnected)
                {
                    MessageBox.Show(cameras.Message);
                    return false;
                }
            }
            return true;
        }

        private async Task<List<DeviceStatus>> GetDevicesStatus()
        {
            return await Task.Run(async () =>
            {
                var devicesStatus = new List<DeviceStatus>();
                //Laser
                devicesStatus.Add(await GetLaserStatus());

                //Spectrometer
                devicesStatus.Add(GetSpectrometerStatus());
                //Camera
                devicesStatus.Add(GetCameraStatus());
                return devicesStatus;
            });
        }

        private DeviceStatus GetCameraStatus()
        {
            var isReady = true;
            var isConnected = true;
            var msg = "";
            int numberOfCameras = 0;
            int numberOfActiveCameras = 0;
            if (camera.TryResetCamlist())
            {
                numberOfCameras = camera?.CamList.Count() ?? 0;
                numberOfActiveCameras = camera?.CamList.Where(x => x.UseForAcquisition).Count() ?? 0;
            }

            if (camera == null || numberOfCameras <= 0)
            {
                isConnected = isReady = false;
                msg += "No connected cameras. ";
            }
            else
            {
                var stringIDs = new List<string>();
                foreach (var cam in camera.CamList)
                {
                    stringIDs.Add(cam.DeviceId);
                }
                var cameras = numberOfCameras == 1 ? "camera" : "cameras";
                msg += string.Join(", ", stringIDs) + $" {cameras} connected, {numberOfActiveCameras}/{numberOfCameras} active.";
            }

            if (numberOfActiveCameras < numberOfCameras)
            {
                isReady = false;
            }

            return new DeviceStatus(Device.Cameras, isConnected, isReady, msg, numberOfCameras);
        }

        private DeviceStatus GetSpectrometerStatus()
        {
            var isReady = true;
            var isConnected = true;
            var msg = "";
            var numberOfSpectrometers = Spectrometer.NumberOfConnectedDevices();
            if (numberOfSpectrometers <= 0)
            {
                isConnected = isReady = false;
                msg += "No connected spectrometers. ";
            }
            else
            {
                var spectrometers = numberOfSpectrometers == 1 ? "spectrometer" : "spectrometers";
                msg += $"{numberOfSpectrometers} {spectrometers} connected.";
            }

            return new DeviceStatus(Device.Spectrometer, isConnected, isReady, msg, numberOfSpectrometers);
        }

        private async Task<DeviceStatus> GetLaserStatus()
        {
            return await Task.Run(() =>
            {
                var isReady = true; // is ready unless shown otherwise
                var msg = "";
                var portStatus = laser.GetPortStatus();
                var isConnected = portStatus == PortStatus.Connected;
                if (isConnected)
                {
                    if (laser.Extreme.Interlock.TryGetValue(laser.ComPort, out double interlock))
                    {
                        if (interlock != 2)
                        {
                            isReady = false;
                            switch (interlock)
                            {
                                case 256: //Keyswitch
                                    msg += "Check interlock (keyswitch). ";
                                    break;
                                case 768: //External
                                    msg += "Check interlock (external). ";
                                    break;
                                default:
                                    msg += "Check interlock. ";
                                    break;
                            }
                        }
                    }
                    else
                    {
                        isReady = false;
                    }

                    if (laser.Extreme.Emission.TryGetValue(laser.ComPort, out double emission))
                    {
                        if (emission <= 0)
                        {
                            isReady = false;
                            msg += "Emission off. ";
                        }
                    }
                    else
                    {
                        isReady = false;
                    }

                    if (laser.Select.Amplitude0.TryGetValue(laser.ComPort, out double amplitude))
                    {
                        if (amplitude <= 0)
                        {
                            isReady = false;
                            msg += "Filter amplitude is zero. ";
                        }
                    }
                    else
                    {
                        isReady = false;
                    }

                    if (laser.Extreme.PowerLevel.TryGetValue(laser.ComPort, out double laserPower))
                    {
                        if (laserPower <= 0)
                        {
                            isReady = false;
                            msg += "Laser power is zero. ";
                        }
                    }
                    else
                    {
                        isReady = false;
                    }

                    if (laser.Select.RfPower.TryGetValue(laser.ComPort, out double rfPower))
                    {
                        if (rfPower <= 0)
                        {
                            isReady = false;
                            msg += "RF Power off. ";
                        }
                    }
                    else
                    {
                        isReady = false;
                    }

                    if (laser.Select.Wavelength0.TryGetValue(laser.ComPort, out double wavelength0))
                    {
                        if (wavelength0 < HardwareSettings.MINIMUM_WAVELENGTH || wavelength0 > HardwareSettings.MAXIMUM_WAVELENGTH)
                        {
                            isReady = false;
                            msg += $"Wavelength out of bounds ({HardwareSettings.MINIMUM_WAVELENGTH}-{HardwareSettings.MAXIMUM_WAVELENGTH}nm)";
                        }
                    }
                    else
                    {
                        isReady = false;
                    }

                    //If no message is set and the laser is not ready, the laser is turned off. TODO: Make a proper check 
                    if (string.IsNullOrWhiteSpace(msg) && !isReady)
                    {
                        msg = "USB connected. SuperK EXTREME turned off.";
                    }
                }
                else
                {
                    isReady = false;
                    msg += portStatus == PortStatus.Disconnected ? "Laser disconnected. " : "Error occured when trying to connect to laser. ";
                }

                return new DeviceStatus(Device.Laser, isConnected, isReady, msg, numberOfDevices: 1);
            });
        }

        /// <summary>
        /// Sweep over exposure time. Only used for debugging shadow like phenomenon observed over approx. 735 nm 
        /// </summary>
        private async void StartExposureSweep_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckPatternWindowOk()) { return; }
            camera.CloseCameraWindows(); //So they won't interfere with measurements

            var progressDialog = new ProgressDialog();
            ViewModel.IsUiEnabled = false;
            try
            {
                var isVerified = await IsVerifiedDeviceAndParameters();
                if (!isVerified) return;

                var exposureTimeMs = (double)ViewModel.ExposureTimeMs;
                if (!TryGetOutputFolder())
                {
                    return; //Cancelled by user
                }

                HandleLightsBeforeScan();

                progressDialog.Show();
                progressDialog.ViewModel.Progress = 0;
                List<double> wavelengths = GetSweepWavelengths();

                int id = 1;
                int idLength = 5;

                //Todo: optimally this timestamp should also be used for metadata filename.
                var timeStamp = DateTime.Now.ToString("HH-mm-ss");
                progressDialog.ViewModel.Message = "Beginning aquisition.";
                camera.SetTriggerAll(HardwareSettings.TRIGGER_TYPE); //Trigger must be set before acquisition is started, otherwise images might be added to the buffer
                camera.BeginAquisitionAll();
                camera.ClearBufferAll(exposureTimeMs);
                var constantWavelength = (double)ViewModel.Wavelength;
                if (!laser.Select.Wavelength0.TrySetValue(constantWavelength, laser.ComPort))
                {
                    log.Warn($"WARNING: Could not set wavelength to {constantWavelength} nm");
                }
                var numberOfWavelengths = wavelengths.Count();
                var imageSaveTasks = new List<Task>();

                //For progess calculation
                double elapsedExposureTime = 0;
                double totalExposureTime = GetTotalExposureTimeFromWavelengths(wavelengths);

                foreach (var wavelength in wavelengths)
                {

                    SetScaledExposureFromWavelength((int)Math.Round(wavelength));
                    var idString = id.ToString().PadLeft(idLength, '0') + $"_{wavelength.ToString("N0")}nm_" + timeStamp;

                    progressDialog.ViewModel.Message = $"Aquiring image {id} of {numberOfWavelengths}";
                    var result = await camera.AcquireSingleImageAllCameras(HardwareSettings.TRIGGER_TYPE);
                    var tasks = result.Select(x => x.Save(OutputFolderPath, idString));
                    imageSaveTasks.AddRange(tasks);

                    //Single image
                    StartTasks(imageSaveTasks);

                    //Update progressbar
                    elapsedExposureTime += GetExposureTimeForWavelenght((int)Math.Round(wavelength));
                    progressDialog.ViewModel.Progress = elapsedExposureTime / totalExposureTime;
                    id++;
                }

                progressDialog.ViewModel.Message = "Ending aquisition.";
                camera.EndAquisitionAll();
                camera.SetTriggerAll(Camera.CameraTriggerType.Off);

                if (imageSaveTasks.Any())
                {
                    // Start remaining tasks
                    imageSaveTasks.Where(x => x.Status == TaskStatus.Created).ToList().ForEach(x => x.Start());
                    progressDialog.ViewModel.Message = "Saving images ...";
                    await Task.WhenAll(imageSaveTasks.ToArray());
                    if (imageSaveTasks.Any(x => x.IsCanceled || x.IsFaulted))
                    {
                        var message = "Not all images could be saved successfully!";
                        log.Error(message);
                        MessageBox.Show($"Error! {message}", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        log.Info("All images saved successfully");
                    }
                }
                Utils.PlaySuccessSound();
                log.Info("Sweep finished.");

                // Store backup in zip file. No need to zip image files
                progressDialog.ViewModel.Message = "Finished";
                Utils.TryOpenFolder(OutputFolderPath);
                if (progressDialog.ViewModel.DoNotifyViaEmail)
                {
                    await MailHandler.TrySend("HSPS: Scan completed", $"Notes:<br/>{ViewModel.Notes}", progressDialog.ViewModel.NotifyEmail);
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error during sweep.", ex);
                if (progressDialog.ViewModel.DoNotifyViaEmail)
                {
                    await MailHandler.TrySend("HSPS: Scan failed", ex.Message, progressDialog.ViewModel.NotifyEmail);
                }
                MessageBox.Show($"Error during sweep. See log for details.");
            }
            finally
            {
                HandleLightsAfterScan();

                ViewModel.IsUiEnabled = true;
                progressDialog.Close();

                //Reset laser to wavelength set in UI
                laser.Select.Wavelength0.TrySetValue((double)ViewModel.Wavelength, laser.ComPort);
            }
        }

        private async Task<bool> TryRunCameraSweep()
        {
            var createCameraFolders = true;

            if (!CheckPatternWindowOk()) { return false; }
            camera.CloseCameraWindows(); //So they won't interfere with measurements

            var progressDialog = new ProgressDialog();
            ViewModel.IsUiEnabled = false;
            try
            {
                var isVerified = await IsVerifiedDeviceAndParameters();
                if (!isVerified) return false;

                var useSpectrometer = ViewModel.UseSpectrometer;
                var integrationTimeMs = (double)ViewModel.IntegrationTimeMs;
                var exposureTimeMs = (double)ViewModel.ExposureTimeMs;
                var measurementCount = (int)ViewModel.MeasurementsNo;
                if (!TryGetOutputFolder())
                {
                    return false; //Cancelled by user
                }

                HandleLightsBeforeScan();

                progressDialog.Show();
                progressDialog.ViewModel.Progress = 0;
                List<double> wavelengths = GetSweepWavelengths();

                int id = 1;
                int idLength = 5;

                var metadataPath = ExportMetadata(wavelengths, id, idLength, useSpectrometer);

                if (!useSpectrometer && !ViewModel.IsVariableExposure)
                {
                    camera.SetExposureTimeForCameras(exposureTimeMs);
                }

                //Todo: optimally this timestamp should also be used for metadata filename.
                var timeStamp = DateTime.Now.ToString("HH-mm-ss");
                var numberOfWavelengths = wavelengths.Count();
                if (!useSpectrometer)
                {
                    progressDialog.ViewModel.Message = "Beginning aquisition.";
                    camera.SetTriggerAll(HardwareSettings.TRIGGER_TYPE); //Trigger must be set before acquisition is started, otherwise images might be added to the buffer
                    camera.BeginAquisitionAll();
                    camera.ClearBufferAll(exposureTimeMs);
                }
                int imageCount = 1;
                if (ViewModel.IsPatternActive)
                {
                    PatternGenerator.TryExportPattern(patternWindow.Pattern, Path.Combine(OutputFolderPath, "pattern_" + timeStamp));
                }
                var imageSaveTasks = new List<Task>();

                //For progess calculation
                var averageCount = ViewModel.NumberOfImagesToAverage ?? 1;
                double elapsedExposureTime = 0;
                double totalExposureTime = ViewModel.IsVariableExposure ? GetTotalExposureTimeFromWavelengths(wavelengths) : wavelengths.Count * (double)ViewModel.ExposureTimeMs;
                totalExposureTime *= averageCount;

                var patternCount = patternWindow?.Pattern.Count;
                if (patternCount != null)
                {
                    totalExposureTime *= (int)patternCount;
                }

                foreach (var wavelength in wavelengths)
                {
                    if (progressDialog.IsClosed) return false;
                    await CheckHardware();
                    if (ViewModel.IsVariableExposure)
                    {
                        SetScaledExposureFromWavelength((int)Math.Round(wavelength));
                    }
                    var idString = id.ToString().PadLeft(idLength, '0') + $"_{wavelength.ToString("N0")}nm_" + timeStamp;
                    if (!laser.Select.Wavelength0.TrySetValue(wavelength, laser.ComPort))
                    {
                        throw new Exception($"Could not set wavelength to {wavelength} nm");
                    }
                    if (useSpectrometer)
                    {
                        progressDialog.ViewModel.Message = $"Aquiring measurement {id} of {numberOfWavelengths}";
                        RunSingleSpectrometerMeasurement(idString, integrationTimeMs, wavelength, measurementCount);

                        //Update progress
                        progressDialog.ViewModel.Progress = id / numberOfWavelengths;
                    }
                    else
                    {
                        // One image per pattern
                        if (ViewModel.IsPatternActive)
                        {
                            patternWindow.IsHotkeysActive = false;

                            var totalImageCount = patternCount * numberOfWavelengths;
                            for (int i = 0; i < patternCount; i++)
                            {
                                if (progressDialog.IsClosed) return false;
                                await CheckHardware();
                                patternWindow.PatternIndex = i;

                                //It takes time for the HDMI output to respond
                                var patternUpdateDelayMs = 50;
                                await Task.Run(() => Thread.Sleep(patternUpdateDelayMs));

                                progressDialog.ViewModel.Message = $"Aquiring image {imageCount} of {totalImageCount} ({wavelength.ToString("0.##")} nm)";
                                var idStringPattern = idString + "_P" + i.ToString().PadLeft(3, '0');

                                // Average images
                                if (averageCount > 1)
                                {
                                    var imgByOutputPath = await GetAverageImagesByOutputPath<Bgr, Int32>(HardwareSettings.TRIGGER_TYPE, wavelength, idStringPattern, averageCount, createCameraFolders);
                                    var tasks = GetSaveImagesTasks(imgByOutputPath);
                                    imageSaveTasks.AddRange(tasks);
                                }
                                else
                                {
                                    var result = await camera.AcquireSingleImageAllCameras(HardwareSettings.TRIGGER_TYPE);
                                    IEnumerable<Task> tasks;
                                    if (createCameraFolders)
                                    {
                                        tasks = result.Select(x => x.Save(Path.Combine(OutputFolderPath, x.CameraId + $"_{wavelength}"), idStringPattern));
                                    }
                                    else
                                    {
                                        tasks = result.Select(x => x.Save(OutputFolderPath, idStringPattern));
                                    }
                                    imageSaveTasks.AddRange(tasks);
                                }
                                LogCameraTemperatures(timeStamp, idStringPattern);

                                StartTasks(imageSaveTasks);
                                imageCount++;
                                progressDialog.ViewModel.Progress = GetUpdatedProgress(wavelength, averageCount, ViewModel.IsVariableExposure, (double)ViewModel.ExposureTimeMs, totalExposureTime, ref elapsedExposureTime);
                            }
                            patternWindow.IsHotkeysActive = true;
                        }
                        //Single image
                        else
                        {
                            progressDialog.ViewModel.Message = $"Aquiring image {id} of {numberOfWavelengths}";
                            if (averageCount == 1)
                            {

                                var result = await camera.AcquireSingleImageAllCameras(HardwareSettings.TRIGGER_TYPE);
                                var tasks = result.Select(x => x.Save(OutputFolderPath, idString));
                                imageSaveTasks.AddRange(tasks);
                            }
                            else
                            {
                                var imgByOutputPath = await GetAverageImagesByOutputPath<Bgr, Int32>(HardwareSettings.TRIGGER_TYPE, wavelength, idString, averageCount, createCameraFolders);
                                var tasks = GetSaveImagesTasks(imgByOutputPath);
                                imageSaveTasks.AddRange(tasks);
                                StartTasks(imageSaveTasks);
                            }

                            LogCameraTemperatures(timeStamp, idString);
                            //Update progress
                            progressDialog.ViewModel.Progress = GetUpdatedProgress(wavelength, averageCount, ViewModel.IsVariableExposure, (double)ViewModel.ExposureTimeMs, totalExposureTime, ref elapsedExposureTime);
                        }
                    }
                    id++;
                }
                progressDialog.ViewModel.ProgressState = System.Windows.Shell.TaskbarItemProgressState.Indeterminate;
                if (!useSpectrometer)
                {
                    progressDialog.ViewModel.Message = "Ending aquisition.";
                    camera.EndAquisitionAll();
                    camera.SetTriggerAll(Camera.CameraTriggerType.Off);
                }
                if (imageSaveTasks.Any())
                {
                    // Start remaining tasks
                    imageSaveTasks.Where(x => x.Status == TaskStatus.Created).ToList().ForEach(x => x.Start());
                    progressDialog.ViewModel.Message = "Saving images ...";
                    await Task.WhenAll(imageSaveTasks.ToArray());
                    if (imageSaveTasks.Any(x => x.IsCanceled || x.IsFaulted))
                    {
                        var message = "Not all images could be saved successfully!";
                        log.Error(message);
                        MessageBox.Show($"Error! {message}", "", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        log.Info("All images saved successfully");
                    }
                }
                log.Info("Sweep finished.");
                // Store backup in zip file. No need to zip image files
                if (useSpectrometer)
                {
                    progressDialog.ViewModel.Message = "Generating backup .zip file";
                    await CreateBackupZip();
                }
                progressDialog.ViewModel.Message = "Finished";

                //Success
                if (progressDialog.ViewModel.DoNotifyViaEmail)
                {
                    var attachments = new string[] { metadataPath };
                    await MailHandler.TrySend("HSPS: Scan completed", $"Notes:<br/>{ViewModel.Notes}", progressDialog.ViewModel.NotifyEmail, attachments: attachments);
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Error($"Error during sweep.", ex);
                if (progressDialog.ViewModel.DoNotifyViaEmail)
                {
                    await MailHandler.TrySend("HSPS: Scan failed", $"Notes:<br/>{ViewModel.Notes}<br/><br/>Exception msg:<br/>{ex.Message}<br/><br/>Stacktrace:<br/>{ex.StackTrace}", progressDialog.ViewModel.NotifyEmail);
                }
                MessageBox.Show($"Error during sweep. See log for details.");
                //Failure
                camera.EndAquisitionAll();
                return false;
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
                progressDialog.Close();
                //Reset laser to wavelength set in UI
                laser.Select.Wavelength0.TrySetValue((double)ViewModel.Wavelength, laser.ComPort);
                HandleLightsAfterScan();
            }
        }

        private void LogCameraTemperatures(string logId, string logEntryId)
        {
            var logfilePath = Path.Combine(OutputFolderPath, $"cam_temp_log_{logId}.txt");
            var timeStamp = DateTime.Now.ToLongTimeString();
            var existingLog = File.Exists(logfilePath);
            using (var sw = new StreamWriter(logfilePath, append: true))
            {
                //Header
                if (!existingLog)
                {
                    var header = "Timestamp\tId";
                    foreach (var cam in camera.CamList)
                    {
                        header += $"\tT_{cam.DeviceId} [°C]";
                    }
                    sw.WriteLine(header);
                }
                //Log entry
                var logEntry = $"{timeStamp}\t{logEntryId}";
                foreach (var cam in camera.CamList)
                {
                    double deviceTemp = 0, tempMainboard = 0;
                    if(camera.TryGetCameraTemperatures(cam.NodeMap, ref deviceTemp))
                    {
                        logEntry += $"\t{deviceTemp}";
                    }
                    else
                    {
                        logEntry += $"\t-";
                    }
                }
                sw.WriteLine(logEntry);
            }
        }

        private async Task CheckHardware()
        {
            var isLaserOn = await IsLaserEmissionOn();
            if (!isLaserOn)
            {
                throw new Exception("Laser emission off during scan");
            }
        }

        private async Task<bool> IsLaserEmissionOn()
        {
            var laserPortStatus = laser.GetPortStatus();
            if (laserPortStatus == PortStatus.Connected)
            {
                double emission;
                var retryCount = 0;
                while (retryCount < 5)
                {
                    if (laser.Extreme.Emission.TryGetValue(laser.ComPort, out emission))
                    {
                        return emission == 3;
                    }
                    await Utils.WaitAsync(100);
                }
            }
            return false;
        }

        private void HandleLightsBeforeScan()
        {
            ArduinoHelper.TurnOnScanningLight();
            if (ViewModel.UseEnclosureLight)
            {
                ArduinoHelper.TurnOnEnclosureLight(ViewModel.EnclosureLightIntensity);
            }
            else
            {
                ArduinoHelper.TurnOffEnclosureLight();
            }
        }

        private double GetUpdatedProgress(double wavelength, double averageCount, bool isVariableExposure, double exsposureTimeMs, double totalExposureTime, ref double elapsedExposureTime)
        {
            //Update progress
            if (isVariableExposure)
            {
                elapsedExposureTime += GetExposureTimeForWavelenght((int)Math.Round(wavelength)) * averageCount;
            }
            else
            {
                elapsedExposureTime += exsposureTimeMs;
            }
            return elapsedExposureTime / totalExposureTime;
        }

        private static void StartTasks(List<Task> imageSaveTasks, int maxRunningTasks = 7)
        {
            var runningTasks = imageSaveTasks.Count(x => x.Status == TaskStatus.Running);
            var tasksCompleted = imageSaveTasks.Count(x => x.Status == TaskStatus.RanToCompletion);
            if (runningTasks < maxRunningTasks)
            {
                var tasksToStart = maxRunningTasks - runningTasks;
                log.Info($"{tasksToStart} tasks started");
                log.Info($"{tasksCompleted} tasks completed successfully");
                imageSaveTasks.Where(x => x.Status == TaskStatus.Created).Take(tasksToStart).ToList().ForEach(x => x.Start());
            }
        }

        private async Task<Dictionary<string, Image<TColor, TDepth>>> GetAverageImagesByOutputPath<TColor, TDepth>(Camera.CameraTriggerType trigger, double wavelength, string idStringPattern, int averageCount, bool createCameraFolders)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var imagesByOutputPath = new Dictionary<string, Image<TColor, TDepth>>();
            for (int j = 0; j < averageCount; j++)
            {
                var result = await camera.AcquireSingleImageAllCameras(trigger);
                foreach (var r in result)
                {
                    using (var image = ImageUtil.ImageFromManagedImage<TColor, TDepth>(r.Image)) //.ConvertScale<Int32>(1.0 / 255, 0);
                    {
                        string imageFilename = "";
                        string outputFilePath = "";
                        string outputFolderPath = "";
                        if (createCameraFolders)
                        {
                            imageFilename = idStringPattern + ".png";
                            outputFolderPath = Path.Combine(OutputFolderPath, $"{r.CameraId}_{wavelength}");
                            outputFilePath = Path.Combine(outputFolderPath, imageFilename);
                        }
                        else
                        {
                            imageFilename = $"{r.CameraId}_{idStringPattern}.png";
                            outputFolderPath = OutputFolderPath;
                            outputFilePath = Path.Combine(outputFolderPath, imageFilename);
                        }

                        //Add image to existing
                        if (imagesByOutputPath.TryGetValue(outputFilePath, out Image<TColor, TDepth> sumImage))
                        {
                            try
                            {
                                imagesByOutputPath[outputFilePath] = sumImage.Add(image);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show($"Error in add: {ex.Message}");
                            }
                        }
                        //First image
                        else
                        {
                            imagesByOutputPath.Add(outputFilePath, image.Clone()); //Must clone, else it will be disposed!
                        }
                        ManagedImage.ExportChunkData(r.Image, outputFolderPath, imageFilename);
                    }
                }
                result.ForEach(x => x.Dispose());
            }
            //Divide sum with average count
            foreach (var sumImageKey in imagesByOutputPath.Keys.ToList()) //Extract keys as you cannot update dictionary values while iterating over it
            {
                imagesByOutputPath[sumImageKey] = imagesByOutputPath[sumImageKey].Mul(1.0 / averageCount);
            }
            return imagesByOutputPath;
        }

        private List<Task<string>> GetSaveImagesTasks<TColor, TDepth>(Dictionary<string, Image<TColor, TDepth>> imgByOutputPath)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var tasks = new List<Task<string>>();
            foreach (var imgKVPair in imgByOutputPath.ToList())
            {
                var filePath = imgKVPair.Key;
                var outputDir = Directory.GetParent(filePath).FullName;
                if (!Directory.Exists(outputDir))
                {
                    Directory.CreateDirectory(outputDir);
                }
                try
                {
                    tasks.Add(new Task<string>(() =>
                    {
                        try
                        {
                            var pngCompression = new KeyValuePair<ImwriteFlags, int>(ImwriteFlags.PngCompression, 1);
                            CvInvoke.Imwrite(filePath, imgByOutputPath[filePath], pngCompression);
                            //image8bit.Save(filePath);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show($"Error saving image: {ex.Message}");
                        }
                        finally
                        {
                            imgByOutputPath[filePath].Dispose();
                        }
                        return filePath;
                    }));
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error in mul");
                }
            }

            return tasks;
        }

        private void SetScaledExposureFromWavelength(int wavelength)
        {
            var exposureTime = GetExposureTimeForWavelenght(wavelength);
            camera.SetExposureTimeForCameras(exposureTime);
        }

        private double GetExposureTimeForWavelenght(int wavelength)
        {
            double exposureFactor;
            if (exposuretimeFactorByWavelength.TryGetValue(wavelength, out exposureFactor))
            {
                var exposureTime = (double)ViewModel.MinExposureTimeMs * exposureFactor;
                return Math.Min(exposureTime, (double)ViewModel.MaxExposureTimeMs);

            }
            else //value outside correction profile range
            {
                //set to nearest extreme
                var max = exposuretimeFactorByWavelength.Keys.Max();
                var min = exposuretimeFactorByWavelength.Keys.Min();
                if (wavelength > max)
                {
                    return GetExposureTimeForWavelenght(max);
                }
                else
                {
                    return GetExposureTimeForWavelenght(min);
                }
            }
        }

        private bool CheckPatternWindowOk()
        {
            if (patternWindow == null)
            {
                var result = MessageBox.Show("No pattern window open. Continue?", "No pattern window open.", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                return result == MessageBoxResult.Yes;
            }
            return true;
        }

        private async Task CreateBackupZip()
        {
            await Task.Run(() =>
            {
                try
                {
                    // GetFileName returns the last path component, in this case the directory. Timestampt just to be absolutely sure not to overwrite
                    var zipFileName = Path.GetFileName(OutputFolderPath) + $"_backup_{DateTime.Now.ToString("HH-mm-ss")}.zip";
                    var tempOutput = Path.Combine(Path.GetTempPath(), zipFileName);
                    ZipFile.CreateFromDirectory(OutputFolderPath, tempOutput);
                    //Move to the zipped folder
                    File.Move(tempOutput, Path.Combine(OutputFolderPath, zipFileName));
                }
                catch (Exception ex)
                {
                    log.Error($"Error creating zip file.", ex);
                    MessageBox.Show("Error saving zip file. See log for details.");
                }
            });
        }

        private void RunSingleSpectrometerMeasurement(string idString, double integrationTimeMs, double wavelength, int measurementCount)
        {
            var result = Spectrometer.ReadSpectrum((int)Math.Round(integrationTimeMs * 1000), measurementCount);
            ExportResult(result, OutputFolderPath, idString, wavelength);
            var dataPoints = new List<DataPoint>();
            for (int i = 0; i < result.WavelengtArray.Length; i++)
            {
                double sum = 0;
                foreach (var measurementResult in result.IntensityMeasurements)
                {
                    sum += measurementResult[i];
                }
                dataPoints.Add(new DataPoint(result.WavelengtArray[i], sum / result.IntensityMeasurements.Count()));
            }
            var resultWindow = new SpectrometerResultPlot(new SpectrometerResultPlotVM(dataPoints));
            resultWindow.Show();
        }


        private VerificationResult VerifyInputParameters(bool isSingleMeasurement)
        {
            var msg = "";
            var verified = true;
            if (ViewModel.ExposureTimeMs == null && !ViewModel.UseSpectrometer)
            {
                msg += "ExposureTime is not set. ";
                verified = false;
            }
            if (ViewModel.IntegrationTimeMs == null && ViewModel.UseSpectrometer)
            {
                msg += "IntegrationTime is not set. ";
                verified = false;
            }

            if (ViewModel.MeasurementsNo == null && ViewModel.UseSpectrometer)
            {
                msg += "MeasurementNo is not set. ";
                verified = false;
            }

            //Single measurement only
            if (isSingleMeasurement)
            {
                if (ViewModel.Wavelength == null)
                {
                    msg += "Wavelenght is not set. ";
                    verified = false;
                }
            }
            //Sweep only
            else
            {
                if (ViewModel.StartWavelength == null)
                {
                    msg += "StartWavelength is not set. ";
                    verified = false;
                }
                if (ViewModel.EndWavelength == null)
                {
                    msg += "EndWavelength is not set. ";
                    verified = false;
                }
            }
            return new VerificationResult(verified, msg);
        }

        private void ExportResult(SpectrometerScanResult result, string outputFolderPath, string id, double wavelength)
        {
            var now = DateTime.Now;
            var filePath = Path.Combine(OutputFolderPath, $"spectrometer_{id}.txt");
            using (var sw = new StreamWriter(filePath, append: true))
            {
                sw.WriteLine($"Generated: {now.ToLongDateString()} {now.ToLongTimeString()}");
                sw.WriteLine();
                sw.WriteLine($"Laser wavelength: {wavelength} nm");
                sw.WriteLine();
                sw.WriteLine("Spectrum:");
                var header = "Wavelength [nm]";
                for (int i = 0; i < result.IntensityMeasurements.Count; i++)
                {
                    header += $"\tIntensity{i}";
                }
                sw.WriteLine(header);
                for (int i = 0; i < result.WavelengtArray.Length; i++)
                {
                    var line = $"{result.WavelengtArray[i]}";
                    foreach (var measurement in result.IntensityMeasurements)
                    {
                        line += $"\t{measurement[i]}";
                    }

                    sw.WriteLine(line);
                }
            }
        }

        private string ExportMetadata(List<double> wavelengths, int idStart, int idLength, bool useSpectrometer)
        {
            bool useVariableExposureTime = !useSpectrometer && ViewModel.IsVariableExposure;
            var now = DateTime.Now;
            var device = useSpectrometer ? "spectrometer" : "camera";
            var metadataFilePath = Path.Combine(OutputFolderPath, $"metadata_{device}_{now.ToString("HH-mm-ss")}.txt");
            using (var sw = new StreamWriter(metadataFilePath, append: true))
            {
                sw.WriteLine($"Generated: {now.ToLongDateString()} {now.ToLongTimeString()}");
                sw.WriteLine();
                sw.WriteLine("Notes:");
                sw.WriteLine(ViewModel.Notes);
                sw.WriteLine();
                //Laser
                sw.WriteLine("Name\tValue\tUnit");
                if (!laser.Select.Amplitude0.TryGetStringValue(laser.ComPort, out string amplitudeTxt))
                {
                    amplitudeTxt = "Could not read amplitude.";
                    log.Error(amplitudeTxt);
                }
                sw.WriteLine($"Filter amplitude\t{amplitudeTxt}\t{laser.Select.Amplitude0.Unit}");
                if (!laser.Extreme.PowerLevel.TryGetStringValue(laser.ComPort, out string powerLevelTxt))
                {
                    powerLevelTxt = "Could not read laser power.";
                    log.Error(amplitudeTxt);
                }
                sw.WriteLine($"Laser power\t{powerLevelTxt}\t{laser.Extreme.PowerLevel.Unit}");
                sw.WriteLine($"Start wavelength\t{ViewModel.StartWavelength}\tnm");
                sw.WriteLine($"End wavelength\t{ViewModel.EndWavelength}\tnm");
                sw.WriteLine($"Step size\t{ViewModel.StepSize}\tnm");
                sw.WriteLine($"Average count\t{ViewModel.NumberOfImagesToAverage}");
                if (ViewModel.IsPatternActive)
                {
                    sw.WriteLine($"Pattern width\t{ViewModel.PatternWidth}");
                    sw.WriteLine($"Pattern height\t{ViewModel.PatternHeight}");
                }
                sw.WriteLine($"Variable exposure\t{ViewModel.IsVariableExposure}");
                //Camera
                if (useSpectrometer)
                {
                    sw.WriteLine($"Integration time\t{ViewModel.IntegrationTimeMs}\tms");
                }
                else
                {
                    if (useVariableExposureTime)
                    {
                        sw.WriteLine($"Min exposure time\t{ViewModel.MinExposureTimeMs}\tms");
                        sw.WriteLine($"Max exposure time\t{ViewModel.MaxExposureTimeMs}\tms");
                    }
                    else
                    {
                        sw.WriteLine($"Exposure time\t{ViewModel.ExposureTimeMs}\tms");
                    }
                }

                //Wavelength to image id mapping
                sw.WriteLine();
                var wavelengthMappingHeader = useVariableExposureTime ? "Id\tWavelength [nm]\tExposureTime [ms]" : "Id\tWavelength [nm]";
                sw.WriteLine(wavelengthMappingHeader);
                int id = idStart;
                foreach (var w in wavelengths)
                {
                    var idString = id.ToString().PadLeft(idLength, '0');
                    if (useVariableExposureTime)
                    {
                        var exposureTime = GetExposureTimeForWavelenght((int)Math.Round(w));
                        sw.WriteLine($"{idString}\t{w}\t{exposureTime}");
                    }
                    else
                    {
                        sw.WriteLine($"{idString}\t{w}");
                    }

                    id++;
                }
            }
            return metadataFilePath;
        }

        private List<double> GetSweepWavelengths()
        {
            var wavelengths = new List<double>();
            if (ViewModel.StepSize == null || ViewModel.StepSize <= 0)
            {
                MessageBox.Show("Step size invalid");
                return wavelengths;
            }
            var stepSize = (double)ViewModel.StepSize;

            var start = (double)ViewModel.StartWavelength;
            var end = (double)ViewModel.EndWavelength;
            var steps = (int)Math.Floor((end - start) / stepSize) + 1;
            for (int i = 0; i < steps; i++)
            {
                wavelengths.Add(start + i * stepSize);
            }
            return wavelengths;
        }

        private bool TryGetOutputFolder()
        {
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
           
            if (!string.IsNullOrWhiteSpace(OutputFolderPath) && Directory.Exists(OutputFolderPath))
            {
                dialog.SelectedPath = OutputFolderPath;
            }
            dialog.Description = "Select output folder";
            var result = dialog.ShowDialog();
            if (result == null || result == false)
            {
                return false;
            }
            if (!Utils.HasWritePermission(dialog.SelectedPath))
            {
                MessageBox.Show("Missing write permission in folder. See log for details.");
                return false;
            }
            OutputFolderPath = dialog.SelectedPath;
            return true;
        }

        private bool TryGetUserDefinedPatternFolder()
        {
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (!string.IsNullOrWhiteSpace(ViewModel.UserDefinedPatternFolder) && Directory.Exists(ViewModel.UserDefinedPatternFolder))
            {
                dialog.SelectedPath = ViewModel.UserDefinedPatternFolder;
            }
            var result = dialog.ShowDialog();
            if (result != true)
            {
                return false;
            }
            ViewModel.UserDefinedPatternFolder = dialog.SelectedPath;
            return true;
        }

        private void CameraRadioBtn_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.UseSpectrometer = false;
        }

        private void SpectrometerRadioBtn_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.UseSpectrometer = true;
        }

        private void RefreshDevicesStatus_Click(object sender, RoutedEventArgs e)
        {
            RefreshUi();
        }

        private bool isRefreshingUi = false;

        private async void RefreshUi()
        {
            isRefreshingUi = true;
            try
            {
                ViewModel.IsUiEnabled = false;
                await Task.Run(async () => await FetchDevicesStatus());
                UpdateDevicesStatusUI();
            }
            catch (Exception ex)
            {
                log.Error($"Error refreshing device status.", ex);
            }
            finally
            {
                isRefreshingUi = false;
                ViewModel.IsUiEnabled = true;
            }
        }

        private void LaserPowerTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SetLaserPowerFromUI();
            }
        }

        private void AmplitudeTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SetAmplitudeFromUI();
            }
        }

        private void WavelengthTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SetWavelengthFromUI();
            }
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Hide window while loading
            this.ShowInTaskbar = false;
            this.WindowState = WindowState.Minimized;
            this.Visibility = Visibility.Hidden;

            //Initilize
            this.DataContext = new LaserControlVM();
            ReadWavelengthToExposuretimeFactorMapping();
            InitArduinoPins();
            InitUiTimer();
            await WindowLoadedRutine();

            //Default pattern
            ViewModel.SelectedPatternType = PatternGenerator.PatternType.PhaseShifting;
            PatternComboBox.SelectedIndex = 2; //Phase shifting index. TODO: Change this so that the selected item is directly binded in ComboBox

            //Ensure that the splashscreen fadeout animation is visible, but only if splash screen is active
            if (App.SplashScreenWindow.IsActive)
            {
                App.SplashScreenWindow.Topmost = true;
            }

            //Show window
            this.ShowInTaskbar = true;
            this.Visibility = Visibility.Visible;

            //Hack: waiting for Visibility event to fire so the window can be correctly restored and centered
            await Utils.WaitAsync(10);
            this.WindowState = WindowState.Normal;
            CenterWindowOnScreen();

            //USB events
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc));

            App.SplashScreenWindow.Close();

            //Must set UI values last to ensure correct binding
            ViewModel.TabItemHeight = "880";
            ViewModel.TabItemWidth = "430";
        }

        private async Task WindowLoadedRutine()
        {
            try
            {
                ArduinoHelper.TurnOnEnclosureLight(ViewModel.EnclosureLightIntensity);
                ViewModel.IsUiEnabled = false;
                laser = new Laser(Properties.Settings.Default.LaserComPort);
                camera = new Camera();

                await FetchDevicesStatus();
                UpdateDevicesStatusUI();
                if (laser.GetPortStatus() == PortStatus.Connected)
                {
                    UpdateLaserUiValues();
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error during window loaded ruitine.", ex);
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
            }
        }

        private void UpdateLaserUiValues()
        {
            //Read values
            string valueTxt;
            if (laser.Extreme.PowerLevel.TryGetStringValue(laser.ComPort, out valueTxt))
            {
                ViewModel.LaserPowerTxt = valueTxt;
            }
            double valueDouble;
            if (laser.Select.RfPower.TryGetValue(laser.ComPort, out valueDouble))
            {
                ViewModel.IsRfPowerOn = valueDouble > 0;
            }
            if (laser.Extreme.Emission.TryGetValue(laser.ComPort, out valueDouble))
            {
                ViewModel.IsEmissionOn = valueDouble == 3;
            }

            //Write values
            laser.Select.Amplitude0.TrySetValue((double)ViewModel.Amplitude, laser.ComPort);
            laser.Select.Wavelength0.TrySetValue((double)ViewModel.Wavelength, laser.ComPort);
        }

        private void UpdateDevicesStatusUI()
        {
            ViewModel.CamerasStatus = devicesStatus.Single(x => x.Device == Device.Cameras);
            ViewModel.LaserStatus = devicesStatus.Single(x => x.Device == Device.Laser);
            ViewModel.SpectrometerStatus = devicesStatus.Single(x => x.Device == Device.Spectrometer);
        }

        private void OpenPatternWindow_Click(object sender, RoutedEventArgs e)
        {
            OpenPatternWindow();
        }

        private void OpenPatternWindow()
        {
            if (patternWindow != null)
            {
                patternWindow.Close();
            }
            Image<Gray, float> homogenizationImage = GetHomogenizationImage();
            var patternVM = new PatternWindowVM(width: HardwareSettings.PROJECTOR_WIDTH, height: HardwareSettings.PROJECTOR_HEIGHT, patternType: ViewModel.SelectedPatternType, isRotated45: ViewModel.IsPatternRotated45);
            patternWindow = new PatternWindow(patternVM, patternWidth: (int)ViewModel.PatternWidth, patternHeight: (int)ViewModel.PatternHeight, phaseShiftingParams: GetPhaseShiftingParametersFromViewModel(), userDefinedPatternFolder: ViewModel.UserDefinedPatternFolder, homogenizationImage: homogenizationImage);
            patternWindow.Unloaded += PatternWindow_Unloaded;
            ViewModel.IsPatternWindowOpen = true;
            patternWindow.Show();
        }

        private void PatternWindow_Unloaded(object sender, RoutedEventArgs e)
        {
            ViewModel.IsPatternWindowOpen = false;
        }

        private Image<Gray, float> GetHomogenizationImage()
        {
            return ViewModel.UseUserDefinedHomogenizationImage && File.Exists(ViewModel.UserDefinedHomogenizationImage) ? new Image<Gray, float>(ViewModel.UserDefinedHomogenizationImage).ConvertScale<float>(1.0 / byte.MaxValue, 0) : null;
        }

        private void ShowLeftCamera_Click(object sender, RoutedEventArgs e)
        {
            camera.SetExposureTimeForCameras((double)ViewModel.ExposureTimeMs);
            camera.OpenLeftCameraGui();
        }

        private void ShowRightCamera_Click(object sender, RoutedEventArgs e)
        {
            camera.SetExposureTimeForCameras((double)ViewModel.ExposureTimeMs);
            camera.OpenRightCameraGui();
        }

        private void ShowBothCameras_Click(object sender, RoutedEventArgs e)
        {
            camera.SetExposureTimeForCameras((double)ViewModel.ExposureTimeMs);
            camera.OpenLeftCameraGui();
            camera.OpenRightCameraGui();
        }

        private void WhiteOutput_Selected(object sender, RoutedEventArgs e)
        {
            ViewModel.SelectedPatternType = PatternGenerator.PatternType.WhiteOutput;
        }

        private void GrayCode_Selected(object sender, RoutedEventArgs e)
        {
            ViewModel.SelectedPatternType = PatternGenerator.PatternType.GrayCodeHorizontal;
        }

        private void PhaseShifting_Selected(object sender, RoutedEventArgs e)
        {
            ViewModel.SelectedPatternType = PatternGenerator.PatternType.PhaseShifting;
        }

        private void LoadFromFolder_Selected(object sender, RoutedEventArgs e)
        {
            ViewModel.SelectedPatternType = PatternGenerator.PatternType.LoadFromFolder;
            if (string.IsNullOrWhiteSpace(ViewModel.UserDefinedPatternFolder) || !Directory.Exists(ViewModel.UserDefinedPatternFolder))
            {
                TryGetUserDefinedPatternFolder();
            }
        }

        private void UpdatePattern_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (patternWindow != null)
                {
                    patternWindow.UserDefinedPatternFolder = ViewModel.UserDefinedPatternFolder;
                    patternWindow.ViewModel.PatternType = ViewModel.SelectedPatternType;
                    patternWindow.ViewModel.IsRotated45 = ViewModel.IsPatternRotated45;
                    patternWindow.HomogenizationImage = GetHomogenizationImage();
                    if (HasValidPatternDimensionChanges())
                    {
                        patternWindow.PatternWidth = (int)ViewModel.PatternWidth;
                        patternWindow.PatternHeight = (int)ViewModel.PatternHeight;
                    }
                    patternWindow.PhaseShiftingParams = GetPhaseShiftingParametersFromViewModel();
                    patternWindow.RefreshPattern();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error changing pattern: {ex.Message}");
            }
        }

        private PhaseShiftingParameters GetPhaseShiftingParametersFromViewModel()
        {
            return new PhaseShiftingParameters(ViewModel.PS_NPeriods, ViewModel.PS_PrimarySteps, ViewModel.PS_CueSteps, phaseMultiplesCount: 0);
        }

        private bool HasValidPatternDimensionChanges()
        {
            return (ViewModel.PatternWidth != patternWindow.PatternWidth || ViewModel.PatternHeight != patternWindow.PatternHeight) && ViewModel.PatternHeight != null && ViewModel.PatternWidth != null;
        }

        private void StartCalibration_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (!TryGetOutputFolder())
                {
                    return; //Cancelled by user
                }

                var exposureTimeMs = 120;
                var viewModel = new AcquireImagesDialogVM(exposureTimeMs);
                var acquireImagesDialog = new AcquireImagesDialog(viewModel, camera, OutputFolderPath);
                var dialogResult = acquireImagesDialog.ShowDialog();
                if (dialogResult != true) return; //Cancelled by user

                //Set selected calibration folders to those created
                var outputFolders = acquireImagesDialog.OutputPaths.Select(x => Directory.GetParent(x).FullName).Distinct();
                if (outputFolders.Count() == 2)
                {
                    var leftFolder = outputFolders.SingleOrDefault(x => x.Contains("L"));
                    if (leftFolder != null)
                    {
                        ViewModel.LeftCameraCalibrationFolder = leftFolder;
                    }

                    var rightFolder = outputFolders.SingleOrDefault(x => x.Contains("R"));
                    if (rightFolder != null)
                    {
                        ViewModel.RightCameraCalibrationFolder = rightFolder;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error during camera calibration.", ex);
                MessageBox.Show("Error during camera calibration. See log for details.");
            }
            finally
            {
                camera.EndAquisitionAll();
            }
        }

        private void KeyHandler(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.F5) && !isRefreshingUi)
            {
                RefreshUi();
            }
            if (patternWindow != null)
            {
                if (Keyboard.Modifiers == ModifierKeys.Alt && Keyboard.IsKeyDown(Key.Left))
                {
                    patternWindow.PreviousPatternFrame();
                }
                if (Keyboard.Modifiers == ModifierKeys.Alt && Keyboard.IsKeyDown(Key.Right))
                {
                    patternWindow.NextPatternFrame();
                }
            }
            
        }

        private void MinExposure_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var isPotentialDouble = !InputVerification.PositiveDoubleRegex.IsMatch(e.Text);
            var isValidValue = false;
            if (isPotentialDouble)
            {
                var tb = (sender as TextBox);
                var input = tb.Text.Remove(tb.SelectionStart, tb.SelectionLength);
                input = input.Insert(tb.SelectionStart, e.Text);
                double value;
                if (double.TryParse(input, out value))
                {
                    isValidValue = true;
                }
            }
            e.Handled = !isPotentialDouble || !isValidValue; // Don't use value if it does not match regex
        }

        private void MinExposure_TextChanged(object sender)
        {
            var input = (sender as TextBox).Text;
            if (double.TryParse(input, out double value))
            {
                if (value <= Camera.CAMERA_MIN_EXPOSURE_TIME_MS)
                {
                    value = Camera.CAMERA_MIN_EXPOSURE_TIME_MS;
                    ViewModel.MinExposureTimeMsTxt = value.ToString();
                }
                else if (value >= Camera.CAMERA_MAX_EXPOSURE_TIME_MS)
                {
                    value = Camera.CAMERA_MAX_EXPOSURE_TIME_MS;
                    ViewModel.MinExposureTimeMsTxt = value.ToString();
                }

                if (value >= ViewModel.MaxExposureTimeMs)
                {
                    ViewModel.MaxExposureTimeMsTxt = value.ToString();
                }
            }
            else
            {
                ViewModel.MinExposureTimeMsTxt = Camera.CAMERA_MIN_EXPOSURE_TIME_MS.ToString();
            }
        }

        private void MaxExposure_TextChanged(object sender)
        {
            var input = (sender as TextBox).Text;
            if (double.TryParse(input, out double value))
            {
                if (value <= Camera.CAMERA_MIN_EXPOSURE_TIME_MS)
                {
                    value = Camera.CAMERA_MIN_EXPOSURE_TIME_MS;
                    ViewModel.MaxExposureTimeMsTxt = value.ToString();
                }
                else if (value >= Camera.CAMERA_MAX_EXPOSURE_TIME_MS)
                {
                    value = Camera.CAMERA_MAX_EXPOSURE_TIME_MS;
                    ViewModel.MaxExposureTimeMsTxt = value.ToString();
                }

                if (value <= ViewModel.MinExposureTimeMs)
                {
                    ViewModel.MinExposureTimeMsTxt = value.ToString();
                }
            }
            else
            {
                ViewModel.MaxExposureTimeMsTxt = Camera.CAMERA_MAX_EXPOSURE_TIME_MS.ToString();
            }
        }

        private void MaxExposure_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var isPotentialDouble = !InputVerification.PositiveDoubleRegex.IsMatch(e.Text);
            var isValidValue = false;
            if (isPotentialDouble)
            {
                var input = (sender as TextBox).Text.Insert((sender as TextBox).CaretIndex, e.Text);
                double value;
                if (double.TryParse(input, out value))
                {
                    isValidValue = true;
                }
            }
            e.Handled = !isPotentialDouble || !isValidValue; // Don't use value if it does not match regex
        }

        private async void Homogenize_Click(object sender, RoutedEventArgs e)
        {
            await Homogenize(useCheckerboard: false);
        }

        private async void HomogenizeWithCheckerboard_Click(object sender, RoutedEventArgs e)
        {
            await Homogenize(useCheckerboard: true);
        }

        private async Task Homogenize(bool useCheckerboard)
        {
            if (patternWindow == null)
            {
                MessageBox.Show("Pattern window must be open");
                return;
            }
            try
            {
                if (!TryGetOutputFolder())
                {
                    return;
                }
                ViewModel.IsUiEnabled = false;
                ArduinoHelper.TurnOffEnclosureLight();
                camera.CloseCameraWindows();
                camera.SetTriggerAll(HardwareSettings.TRIGGER_TYPE); //Trigger must be set before acquisition is started, otherwise images might be added to the buffer
                camera.BeginAquisitionAll();
                camera.SetExposureTimeForCameras((double)ViewModel.ExposureTimeMs);
                Image<Gray, float> checkerboardImage = null;
                string checkerboardPatternPath = "";
                string leftCheckerboardPath = "";
                string rightCheckerboardPath = "";
                var tempImagePaths = new List<string>();
                if (useCheckerboard)
                {
                    //First time take checkerboard images
                    var checkerboarImagePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "checkerboard_for_homogenization.png");
                    checkerboardImage = new Image<Gray, float>(checkerboarImagePath);
                    checkerboardPatternPath = Path.Combine(OutputFolderPath, "checkerboard.png");
                    checkerboardImage.Save(checkerboardPatternPath);

                    //Temporary hack to center new pattern
                    patternWindow.PatternWidth = HardwareSettings.PROJECTOR_WIDTH;
                    patternWindow.PatternHeight = HardwareSettings.PROJECTOR_HEIGHT;
                    patternWindow.RefreshPattern();

                    patternWindow.Pattern = new List<Image<Gray, float>> { checkerboardImage };

                    await Utils.WaitAsync(1500); //Takes time to update pattern

                    var checkerboardImages = await camera.AcquireSingleImageAllCameras(HardwareSettings.TRIGGER_TYPE);
                    leftCheckerboardPath = await checkerboardImages.Single(x => x.CameraSerial == Camera.LEFT_CAMERA_SERIAL_NO).Save(OutputFolderPath, "checkerboard");
                    rightCheckerboardPath = await checkerboardImages.Single(x => x.CameraSerial == Camera.RIGHT_CAMERA_SERIAL_NO).Save(OutputFolderPath, "checkerboard");
                }
                var whiteImage = new Image<Gray, float>((int)ViewModel.PatternWidth, (int)ViewModel.PatternHeight, new Gray(byte.MaxValue));
                var prevImage = new Image<Gray, float>(HardwareSettings.PROJECTOR_WIDTH, HardwareSettings.PROJECTOR_HEIGHT, new Gray(0));
                var offsetX = (int)Math.Round((HardwareSettings.PROJECTOR_WIDTH - (int)ViewModel.PatternWidth) / 2.0);
                var offsetY = (int)Math.Round((HardwareSettings.PROJECTOR_HEIGHT - (int)ViewModel.PatternHeight) / 2.0);
                var offset = new System.Drawing.Point(offsetX, offsetY);
                var patternRegion = new Rectangle(offset, whiteImage.Size);
                var pattern = new List<Image<Gray, float>>();

                whiteImage.CopyTo(prevImage.GetSubRect(patternRegion));

                //Temporary hack to center new pattern
                patternWindow.PatternWidth = HardwareSettings.PROJECTOR_WIDTH;
                patternWindow.PatternHeight = HardwareSettings.PROJECTOR_HEIGHT;
                patternWindow.RefreshPattern();

                pattern.Add(prevImage);
                patternWindow.Pattern = pattern;

                await Utils.WaitAsync(1500); //Takes time to update pattern

                // Define the output 
                var count = 0;
                var now = DateTime.Now;
                var timestamp = now.ToString("HH-mm-ss");

                //Recursive homogenization
                double[,] boundingBoxCoordinates = null; // new double[8, 2];

                //Recursive auto homogenization
                double[,] transformationL = null;
                double[,] transformationR = null;

                // Create the MATLAB instance 
                var matlab = new Matlab();

                var calibrationParamsPath = Properties.Settings.Default.StereoCalibParamsFolder;

                var idString = $"{count}_{timestamp}";
                var filePaths = await OutputSingleImageAllCameras<Gray, float>(idString, (double)ViewModel.Wavelength, createCameraFolders: false);

                while (true)
                {
                    count++; //First image is before any correction

                    var leftImagePath = filePaths.Single(x => Path.GetFileName(x).StartsWith("L"));
                    var rightImagePath = filePaths.Single(x => Path.GetFileName(x).StartsWith("R"));
                    var lossPercentage = 0.3 * Math.Pow(0.5, count);

                    double[,] homoImageData = null;
                    if (useCheckerboard)
                    {
                        HomogenizationAutoResult result;
                        bool showFigures = false;
                        double maxPercentile = 80;
                        result = matlab.HomogenizeAuto(leftImagePath, leftCheckerboardPath, rightImagePath, rightCheckerboardPath, checkerboardPatternPath, calibrationParamsPath, lossPercentage, maxPercentile, showFigures, transformationL, transformationR);
                        homoImageData = result.HomogenizationImageData;
                        transformationL = result.TransformationL;
                        transformationR = result.TransformationR;
                    }
                    else
                    {
                        var autoModeEnabled = false; //auto mode finds corners automagically
                        HomogenizationResult result;
                        result = matlab.Homogenize(leftImagePath, rightImagePath, calibrationParamsPath, lossPercentage, autoModeEnabled, (int)ViewModel.PatternWidth, (int)ViewModel.PatternHeight, boundingBoxCoordinates);
                        homoImageData = result.HomogenizationImageData;
                        boundingBoxCoordinates = result.BoundingBoxCoordinates;
                    }

                    var width = homoImageData.GetLength(1);
                    var height = homoImageData.GetLength(0);
                    var image = new Image<Gray, float>(HardwareSettings.PROJECTOR_WIDTH, HardwareSettings.PROJECTOR_HEIGHT);
                    var imageOffsetX = (int)Math.Round((HardwareSettings.PROJECTOR_WIDTH - width) / 2.0);
                    var imageOffsetY = (int)Math.Round((HardwareSettings.PROJECTOR_HEIGHT - height) / 2.0);
                    for (var x = 0; x < width; x++)
                    {
                        for (var y = 0; y < height; y++)
                        {
                            image.Data[imageOffsetY + y, imageOffsetX + x, 0] = (float)homoImageData[y, x];
                        }
                    }
                    prevImage = prevImage.Mul(image);

                    pattern.Reverse();
                    pattern.Add(prevImage);
                    pattern.Reverse();

                    var prevImagePath = Path.Combine(OutputFolderPath, $"homoImage_{count}_{timestamp}.png");
                    prevImage.Save(prevImagePath);
                    patternWindow.Pattern = pattern;

                    //Show result from homogenization (input to next iteration)
                    idString = $"{count}_{timestamp}";
                    filePaths = await OutputSingleImageAllCameras<Gray, float>(idString, (double)ViewModel.Wavelength, createCameraFolders: false);
                    ImageViewer viewer = new ImageViewer();
                    viewer.Text = $"Homogenization result, iteration: {count} (close to continue)";
                    viewer.Image = new Image<Gray, float>(filePaths.First());
                    viewer.ShowDialog();

                    var answer = MessageBox.Show($"Another iteration? (#{count+1})", "", MessageBoxButton.YesNo);

                    if (answer != MessageBoxResult.Yes)
                    {
                        Utils.TryOpenFolder(OutputFolderPath);
                        var dialogResult = MessageBox.Show($"Set this homogenization as default?\n{prevImagePath}", "Set as default?", MessageBoxButton.YesNo);
                        if(dialogResult == MessageBoxResult.Yes)
                        {
                            try
                            {
                                Properties.Settings.Default.HomogenizationImage = prevImagePath;
                                Properties.Settings.Default.Save();
                                ViewModel.UserDefinedHomogenizationImage = prevImagePath;
                            }
                            catch (Exception ex)
                            {
                                log.Error("Error saving new settings for homogenization.", ex);
                            }
                        }
                        break;
                    }
                }
                matlab.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error running matlab script: {ex.Message}");
            }
            finally
            {
                camera.EndAquisitionAll();
                camera.SetTriggerAll(Camera.CameraTriggerType.Off);
                ArduinoHelper.TurnOnEnclosureLight(ViewModel.EnclosureLightIntensity);
                ViewModel.IsUiEnabled = true;
            }
        }

        private async Task<string[]> OutputSingleImageAllCameras<TColor, TDepth>(string idString, double wavelength, bool createCameraFolders)
            where TColor : struct, IColor
            where TDepth : new()
        {
            var imageSaveTasks = new List<Task<string>>();
            var averageCount = ViewModel.NumberOfImagesToAverage ?? 1;
            if (averageCount == 1)
            {
                var result = await camera.AcquireSingleImageAllCameras(HardwareSettings.TRIGGER_TYPE);
                var tasks = result.Select(x => x.Save(OutputFolderPath, idString));
                imageSaveTasks.AddRange(tasks);
            }
            else
            {
                var imgByOutputPath = await GetAverageImagesByOutputPath<TColor, TDepth>(HardwareSettings.TRIGGER_TYPE, wavelength, idString, averageCount, createCameraFolders);
                var image = imgByOutputPath.First().Value;
                var tasks = GetSaveImagesTasks(imgByOutputPath);
                tasks.ForEach(x => x.Start());
                imageSaveTasks.AddRange(tasks);
            }
            var filePaths = await Task.WhenAll(imageSaveTasks.ToArray());
            return filePaths;
        }

        private void SelectPatternFolder_Click(object sender, RoutedEventArgs e)
        {
            TryGetUserDefinedPatternFolder();
        }

        private void SelectCorrectionMask_Click(object sender, RoutedEventArgs e)
        {
            TryGetUserDefinedHomogenizationImage();
        }

        private bool TryGetUserDefinedHomogenizationImage()
        {
            // Set filter for file extension and default file extension 
            var dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "Image files (*.png, *.bmp, *.tiff) | *.png; *.bmp; *.tiff";
            dialog.Multiselect = false;
            if (!string.IsNullOrWhiteSpace(ViewModel.UserDefinedHomogenizationImage) && File.Exists(ViewModel.UserDefinedHomogenizationImage))
            {
                dialog.InitialDirectory = Directory.GetParent(ViewModel.UserDefinedHomogenizationImage).FullName;
            }
            var result = dialog.ShowDialog();
            if (result == null || !(bool)result)
            {
                return false;
            }
            ViewModel.UserDefinedHomogenizationImage = dialog.FileName;
            return true;
        }

        private async void CalculateVariableExposure_Click(object sender, RoutedEventArgs e)
        {
            var waitingDialog = new WaitingDialog(new WaitingDialogVM("Processing...", "Variable exposure"));
            try
            {
                var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
                dialog.Description = "Select image folder L/R";
                if (!string.IsNullOrWhiteSpace(OutputFolderPath) && Directory.Exists(OutputFolderPath))
                {
                    dialog.SelectedPath = OutputFolderPath;
                }
                var dialogResult = dialog.ShowDialog();
                if (dialogResult != true)
                {
                    return;
                }
                ViewModel.IsUiEnabled = false;
                var inputFolder = dialog.SelectedPath;
                waitingDialog.Show();
                await CalculateVariableExposure(inputFolder);
                Utils.TryOpenFolder(inputFolder);
            }
            catch (Exception ex)
            {
                var message = $"Error occured during calculation of variable exposure.";
                log.Error(message, ex);
                MessageBox.Show(message + " See log for details.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
                waitingDialog.Close();
            }
        }

        private Task CalculateVariableExposure(string inputFolder)
        {
            var inputFolderThreadSafe = inputFolder;
            return Task.Run(() =>
            {
                // Create the MATLAB instance 
                var matlab = new Matlab();
                //matlab.Visible = 0;

                var variableExposureData = matlab.CalcVariableExposure(inputFolderThreadSafe);
                matlab.Quit();

                var filePath = Path.Combine(inputFolder, $"variableExposureFactors_{DateTime.Now.ToString("yy-MM-dd_HH-mm-ss")}.txt");

                using (var sw = new StreamWriter(filePath, append: true))
                {
                    for (int i = 0; i < variableExposureData.GetLength(0); i++)
                    {
                        sw.WriteLine($"{variableExposureData[i, 0]}\t{variableExposureData[i, 1]}");
                    }
                }
            });
        }

        private void ShowVariableExposureGraph_Click(object sender, RoutedEventArgs e)
        {
            if (variableExposureGraph != null && VariableExposurePlot.IsOpen)
            {
                variableExposureGraph.Focus();
                return;
            }

            var factorPoints = new List<DataPoint>();
            var exposurePoints = new List<DataPoint>();
            double exposureTimePerImage = 0;
            var wavelengths = GetSweepWavelengths();
            foreach (var wavelength in wavelengths)
            {
                var wavelengthInt = (int)Math.Round(wavelength);
                var exposureTime = GetExposureTimeForWavelenght(wavelengthInt);
                exposureTimePerImage += exposureTime * (int)ViewModel.NumberOfImagesToAverage;
                exposurePoints.Add(new DataPoint(wavelength, exposureTime));
            }

            foreach (var kvPair in exposuretimeFactorByWavelength)
            {
                factorPoints.Add(new DataPoint(kvPair.Key, kvPair.Value));
            }

            var viewModel = new VariableExposurePlotVM(factorPoints, exposurePoints);
            viewModel.TotalExposureTimeTxt = Utils.TimeMsToTimeSpanString(exposureTimePerImage * (int)viewModel.NumberOfImagesPerWavelength);
            viewModel.MinExposureTimeMs = (double)ViewModel.MinExposureTimeMs;
            viewModel.MaxExposureTimeMs = (double)ViewModel.MaxExposureTimeMs;
            variableExposureGraph = new VariableExposurePlot(viewModel, exposureTimePerImage);
            variableExposureGraph.Show();
        }

        private void MinVariableExposure_LostFocus(object sender, RoutedEventArgs e)
        {
            MinExposure_TextChanged(sender);
            UpdateVariableExposureGraph();
            ViewModel.ExposureTimeMsTxt = ViewModel.MinExposureTimeMsTxt;
            TryUpdateExposureTime();
        }

        private void MaxVariableExposure_LostFocus(object sender, RoutedEventArgs e)
        {
            MaxExposure_TextChanged(sender);
            UpdateVariableExposureGraph();
        }

        private double GetTotalExposureTimeFromWavelengths(IEnumerable<double> wavelengths)
        {
            return wavelengths.Select(x => GetExposureTimeForWavelenght((int)Math.Round(x))).Sum();
        }

        private void UpdateVariableExposureGraph()
        {
            if (variableExposureGraph != null)
            {
                try
                {
                    variableExposureGraph.ViewModel.MinExposureTimeMs = (double)ViewModel.MinExposureTimeMs;
                    variableExposureGraph.ViewModel.MaxExposureTimeMs = (double)ViewModel.MaxExposureTimeMs;
                    var exposurePoints = new List<DataPoint>();
                    double totalExposureTime = 0;
                    var wavelengths = GetSweepWavelengths();
                    foreach (var wavelength in wavelengths)
                    {
                        var wavelengthInt = (int)Math.Round(wavelength);
                        var exposureTime = GetExposureTimeForWavelenght(wavelengthInt);
                        totalExposureTime += exposureTime * (int)ViewModel.NumberOfImagesToAverage;
                        exposurePoints.Add(new DataPoint(wavelength, exposureTime));
                    }
                    variableExposureGraph.ViewModel.ExposurePoints = exposurePoints;
                    variableExposureGraph.ExposureTimePerImageMs = totalExposureTime; //Must be set so total time can be updated from within the window
                    var numImages = (int)variableExposureGraph.ViewModel.NumberOfImagesPerWavelength;
                    variableExposureGraph.ViewModel.TotalExposureTimeTxt = Utils.TimeMsToTimeSpanString(totalExposureTime * numImages);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error occurred while updating variable exposure graph.");
                }
            }
        }


        private void MinVariableExposure_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                MinExposure_TextChanged(sender);
                UpdateVariableExposureGraph();
                ViewModel.ExposureTimeMsTxt = ViewModel.MinExposureTimeMsTxt;
                TryUpdateExposureTime();
            }
        }

        private void TryUpdateExposureTime()
        {
            try
            {
                camera.SetExposureTimeForCameras((double)ViewModel.ExposureTimeMs);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MaxVariableExposure_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                MaxExposure_TextChanged(sender);
                UpdateVariableExposureGraph();
            }
        }

        private void SelectLeftCameraCalibrationFolder_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (!string.IsNullOrWhiteSpace(ViewModel.LeftCameraCalibrationFolder) && Directory.Exists(ViewModel.LeftCameraCalibrationFolder))
            {
                dialog.SelectedPath = ViewModel.LeftCameraCalibrationFolder;
            }
            dialog.Description = "Select Left folder";
            var result = dialog.ShowDialog();
            if (result == true)
            {
                ViewModel.LeftCameraCalibrationFolder = dialog.SelectedPath;
                UpdateCanRunCameraCalibration();
            }
        }

        private void SelectRightCameraCalibrationFolder_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (!string.IsNullOrWhiteSpace(ViewModel.RightCameraCalibrationFolder) && Directory.Exists(ViewModel.RightCameraCalibrationFolder))
            {
                dialog.SelectedPath = ViewModel.RightCameraCalibrationFolder;
            }
            dialog.Description = "Select Right folder";
            var result = dialog.ShowDialog();
            if (result == true)
            {
                ViewModel.RightCameraCalibrationFolder = dialog.SelectedPath;
                UpdateCanRunCameraCalibration();
            }
        }

        private void UpdateCanRunCameraCalibration()
        {
            ViewModel.CanRunCameraCalibration = !string.IsNullOrWhiteSpace(ViewModel.LeftCameraCalibrationFolder) &&
                                                !string.IsNullOrWhiteSpace(ViewModel.RightCameraCalibrationFolder) &&
                                                Directory.Exists(ViewModel.LeftCameraCalibrationFolder) &&
                                                Directory.Exists(ViewModel.RightCameraCalibrationFolder);
        }

        private async void RunCameraCalibration_Click(object sender, RoutedEventArgs e)
        {
            if (!TryGetOutputFolder()) return;

            var leftCameraCalibrationFolder = ViewModel.LeftCameraCalibrationFolder;
            var rightCameraCalibrationFolder = ViewModel.RightCameraCalibrationFolder;
            var squareSizeMm = (double)ViewModel.SquareSizeMm;

            var waitingDialog = new WaitingDialog(new WaitingDialogVM("Calibrating...", "Stereo calibration"));
            waitingDialog.Show();
            ViewModel.IsUiEnabled = false;
            try
            {
                await CameraCalibrator.CalibrateCamerasWithIndividualIntrinsics(leftCameraCalibrationFolder, rightCameraCalibrationFolder, squareSizeMm, ViewModel.ChessboardInnerGridsize, OutputFolderPath, useOpenCv: true);
                waitingDialog.Close();
                Utils.TryOpenFolder(OutputFolderPath);
                var dialogResult = MessageBox.Show($"Set this calibration as default? Instead of\n{Properties.Settings.Default.StereoCalibParamsFolder}", "Set as default?", MessageBoxButton.YesNo);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    try
                    {
                        Properties.Settings.Default.StereoCalibParamsFolder = OutputFolderPath;
                        Properties.Settings.Default.Save();
                    }
                    catch (Exception ex)
                    {
                        log.Error("Error saving new settings for calibration parameters.", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = $"Error during calibration.";
                log.Error(msg, ex);
                MessageBox.Show(msg + " See log for details.", "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
            }
        }

        private async void CameraSettings_Click(object sender, RoutedEventArgs e)
        {
            var camSettingsDialog = new CameraSettingsDialog(new CameraSettingsDialogVM(camera?.CamList));
            var result = camSettingsDialog.ShowDialog();
            if (result == true)
            {
                foreach (var camSetting in camSettingsDialog.ActiveCameraIds)
                {
                    var cam = camera.CamList.SingleOrDefault(x => x.DeviceId == camSetting.Item1);
                    if (cam != null)
                    {
                        cam.UseForAcquisition = camSetting.Item2;
                    }
                }
            }
            await FetchDevicesStatus();
            UpdateDevicesStatusUI();
        }

        private void InitArduinoPins()
        {
            Arduino.Instance.SetPinMode(HardwareSettings.SCANNING_LIGHT_POWER, Arduino.PinMode.Output);
            Arduino.Instance.SetPinMode(HardwareSettings.ENCLOSURE_LIGHT_POWER, Arduino.PinMode.Output);
            Arduino.Instance.SetPinMode(HardwareSettings.CAMERA_LEFT_POWER_ARDUINO, Arduino.PinMode.Output);
            Arduino.Instance.SetPinMode(HardwareSettings.CAMERA_RIGHT_POWER_ARDUINO, Arduino.PinMode.Output);
        }

        private void SweepSetting_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                UpdateVariableExposureGraph();
            }
        }

        private void SweepSetting_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateVariableExposureGraph();
        }

        private async void RectifyImages_Click(object sender, RoutedEventArgs e)
        {
            var waitingDialog = new WaitingDialog(new WaitingDialogVM("Processing...", "Rectify Images"));
            ViewModel.IsUiEnabled = false;
            try
            {
                var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
                dialog.Description = "Select image folder";
                var dialogResult = dialog.ShowDialog();
                if (dialogResult != true)
                {
                    return;
                }
                var selectedFolder = dialog.SelectedPath;
                waitingDialog.Show();
                await RectifyImages(selectedFolder);
                Utils.PlaySuccessSound();
                Utils.TryOpenFolder(selectedFolder);
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
                waitingDialog.Close();
            }
        }

        private static async Task RectifyImages(string selectedFolder)
        {
            var leftId = "L_";
            var rightId = "R_";

            var directories = Directory.GetDirectories(selectedFolder).Select(x => new DirectoryInfo(x)).ToList();
            var leftDirectories = directories.Where(x => x.Name.ToUpper().StartsWith(leftId));
            var rightDirectories = directories.Where(x => x.Name.ToUpper().StartsWith(rightId));
            if (!leftDirectories.Any() || !rightDirectories.Any())
            {
                MessageBox.Show($"Folder must contain folder pairs beginning with '{leftId}' and '{rightId}'");
                return;
            }

            //Load maps
            var mapsFolderPath = Properties.Settings.Default.StereoCalibParamsFolder;

            var dirFilesByCreationDate = new DirectoryInfo(mapsFolderPath).EnumerateFiles().OrderByDescending(x => x.CreationTime);
            var xMapLPath = dirFilesByCreationDate.First(x => x.Extension.ToLower() == ".json" && x.Name.StartsWith("map1x_")).FullName;
            var yMapLPath = dirFilesByCreationDate.First(x => x.Extension.ToLower() == ".json" && x.Name.StartsWith("map1y_")).FullName;

            var xMapL = new Matrix<float>(
                Extensions.ArrayExtensions.FromJsonFile(xMapLPath));
            var yMapL = new Matrix<float>(
                Extensions.ArrayExtensions.FromJsonFile(yMapLPath));

            var xMapRPath = dirFilesByCreationDate.First(x => x.Extension.ToLower() == ".json" && x.Name.StartsWith("map2x_")).FullName;
            var yMapRPath = dirFilesByCreationDate.First(x => x.Extension.ToLower() == ".json" && x.Name.StartsWith("map2y_")).FullName;
            var xMapR = new Matrix<float>(
                Extensions.ArrayExtensions.FromJsonFile(xMapRPath));
            var yMapR = new Matrix<float>(
                Extensions.ArrayExtensions.FromJsonFile(yMapRPath));

            var tasks = new List<Task>();
            foreach (var dirL in leftDirectories)
            {
                var dirR = rightDirectories.SingleOrDefault(r => r.Name.Substring(rightId.Length) == dirL.Name.Substring(leftId.Length));
                if (dirR == null) continue;

                var inputFolderL = dirL.FullName;
                var inputFolderR = dirR.FullName;

                var destinationFolderL = Path.Combine(inputFolderL, "rectified");
                var destinationFolderR = Path.Combine(inputFolderR, "rectified");

                tasks.Add(CameraCalibrator.RectifyImagesFromFolder(inputFolderL, destinationFolderL, xMapL, yMapL));
                tasks.Add(CameraCalibrator.RectifyImagesFromFolder(inputFolderR, destinationFolderR, xMapR, yMapR));
            }
            await Task.WhenAll(tasks.ToArray());
        }

        private Matrix<float> GetMatrixFromTxtFile(string sourcePath)
        {
            string[] lines = File.ReadAllLines(sourcePath);
            var rows = lines.Length;
            var cols = lines.First().Split().Length;
            var data = new float[rows, cols];
            for (int y = 0; y < rows; y++)
            {
                var colValues = lines[y].Split();
                for (int x = 0; x < cols; x++)
                {
                    data[y, x] = float.Parse(colValues[x]);
                }
            }
            return new Matrix<float>(data);
        }

        private void AverageImages_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                UpdateVariableExposureGraph();
            }
        }

        private void IsEnclosureLightOn_Checked(object sender, RoutedEventArgs e)
        {
            ArduinoHelper.TurnOnEnclosureLight(ViewModel.EnclosureLightIntensity);
        }

        private void IsEnclosureLightOn_Unchecked(object sender, RoutedEventArgs e)
        {
            ArduinoHelper.TurnOffEnclosureLight();
        }

        private void ToggleEnclosureLight_Click(object sender, RoutedEventArgs e)
        {
            if(Arduino.OutputStateByPinNumber.TryGetValue(HardwareSettings.ENCLOSURE_LIGHT_POWER, out Tuple<byte, Arduino.WriteMode> value)){
                if(value.Item1 >= 1)
                {
                    ArduinoHelper.TurnOffEnclosureLight();
                }
                else
                {
                    ArduinoHelper.TurnOnEnclosureLight(ViewModel.EnclosureLightIntensity);
                }
            }
        }

        private void EnclosureLightSlider_Drop(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                Arduino.Instance.AnalogWrite(HardwareSettings.ENCLOSURE_LIGHT_POWER, ViewModel.EnclosureLightIntensity);
            }
            catch { }
        }

        private async void ScanAndReconstruct()
        {
            if(ViewModel.SelectedPatternType != PatternGenerator.PatternType.PhaseShifting)
            {
                MessageBox.Show("Only phase shifting is supported for reconstruction");
                return;
            }
            var waitingDialog = new WaitingDialog(new WaitingDialogVM("", ""));
            try
            {
                if (ViewModel.IsStepMode)
                {
                    if (!await TryRunCameraSweep()) return;
                }
                else
                {
                    MessageBox.Show("Rectify and 3D reconstruct must be run in 'Sweep mode'.");
                    return;
                    // TODO: change RunSingleCameraMeasurement() so that it supports a pattern 
                    // OR run TryRunCameraSweep so that it "mimicks" a single wavelength
                    //if (!await RunSingleCameraMeasurement()) return;
                }

                //Hack
                ViewModel.IsUiEnabled = false;

                waitingDialog.ViewModel.Header = "Rectifying images";
                waitingDialog.Show();

                await RectifyImages(OutputFolderPath);

                waitingDialog.ViewModel.Header = "Reconstructing";
                var matPcPaths = await ReconstructRectifiedImages(OutputFolderPath, ViewModel.PS_NPeriods, ViewModel.PS_PrimarySteps, ViewModel.PS_CueSteps);

                // Merge pointclouds
                //var timeStamp = DateTime.Now;
                //var mergedPlyFilePath = Path.Combine(OutputFolderPath, $"merged_{timeStamp.ToString("HH-mm-ss")}.ply");
                //MergePointclouds(matPcPaths, mergedPlyFilePath);
                Utils.TryOpenFolder(OutputFolderPath);
                //Utils.OpenPointCloudsInMeshlab(new string[] { mergedPlyFilePath });
                Utils.PlaySuccessSound();
            }
            finally
            {
                waitingDialog.Close();
                ViewModel.IsUiEnabled = true;
            }
        }

        /// <summary>
        /// Merge Matlab mat objects into a single .ply pointcloud. Color values are hardcoded for 2-4 wavelengths, else they are random 
        /// </summary>
        /// <param name="matPcPaths">List of Matlab .mat pointclouds</param>
        /// <param name="mergedPlyFilePath">Absolute output path for the merged point cloud on .ply format</param>
        private void MergePointclouds(string[] matPcPaths, string mergedPlyFilePath)
        {
            using (var matlab = new Matlab())
            {
                //Hardcoded values for presentation
                var numPointClouds = matPcPaths.Count();
                double[,] colors;
                if (numPointClouds == 2)
                {
                    colors = new double[2, 3] { { 0, 0, 1 }, { 1, 0, 0 } };
                }
                else if (numPointClouds == 3)
                {
                    colors = new double[3, 3] { { 0, 0, 1 }, { 0, 1, 0 }, { 1, 0, 0 } };
                }
                else if (numPointClouds == 4)
                {
                    colors = new double[4, 3] { { 0, 0, 1 }, { 0, 1, 0 }, { 0.9, 0.5, 0 }, { 1, 0, 0 } };
                }
                else
                {
                    colors = GenerateRandomColors(matPcPaths.Count());
                }
                matlab.MergePointclouds(matPcPaths, colors, mergedPlyFilePath, denoise: true);
            }
        }

        private async Task<string[]> ReconstructRectifiedImages(string folderPath, int periods, int primarySteps, int cueSteps)
        {
            var leftId = "L_";
            var rightId = "R_";

            var directories = Directory.GetDirectories(folderPath).Select(x => new DirectoryInfo(x)).ToList();
            var leftDirectories = directories.Where(x => x.Name.ToUpper().StartsWith(leftId));
            var rightDirectories = directories.Where(x => x.Name.ToUpper().StartsWith(rightId));
            if (!leftDirectories.Any() || !rightDirectories.Any())
            {
                MessageBox.Show($"Folder must contain folder pairs beginning with '{leftId}' and '{rightId}'");
                return new string[] { };
            }
            using (var matlab = new Matlab())
            {
                var tasks = new List<Task<string>>();
                foreach (var dirL in leftDirectories)
                {
                    var dirR = rightDirectories.SingleOrDefault(r => r.Name.Substring(rightId.Length) == dirL.Name.Substring(leftId.Length));
                    if (dirR == null) continue;
                    tasks.Add(new Task<string>(() =>
                    {
                        var inputFolderL = dirL.FullName;
                        var inputFolderR = dirR.FullName;

                        var rectifiedFolderL = Path.Combine(inputFolderL, "rectified");
                        var rectifiedFolderR = Path.Combine(inputFolderR, "rectified");
                        var fileName = $"{dirL.FullName.Substring(leftId.Length)}-pc.mat";
                        var outputFileMat = Path.Combine(dirL.Parent.FullName, fileName);
                        matlab.PhaseShiftingReconstruction(rectifiedFolderL, rectifiedFolderR, outputFileMat, periods, primarySteps, cueSteps);
                        var plyPath = matlab.SaveHspsAsPly(outputFileMat);
                        return outputFileMat;
                    }));
                }
                tasks.ForEach(x => x.Start());
                return await Task.WhenAll(tasks.ToArray());
            }
        }

        private async void Reconstruct_Click(object sender, RoutedEventArgs e)
        {
            var waitingDialog = new WaitingDialog(new WaitingDialogVM("Processing...", "Reconstruction"));
            ViewModel.IsUiEnabled = false;
            try
            {
                var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
                dialog.Description = "Select image folder (with rectified subsubfolder)";
                var dialogResult = dialog.ShowDialog();
                if (dialogResult != true)
                {
                    return;
                }
                var selectedFolder = dialog.SelectedPath;
                waitingDialog.Show();
                var matPcPaths = await ReconstructRectifiedImages(selectedFolder, ViewModel.PS_NPeriods, ViewModel.PS_PrimarySteps, ViewModel.PS_CueSteps);
                var timeStamp = DateTime.Now;
                var mergedPlyFilePath = Path.Combine(selectedFolder, $"merged_{timeStamp.ToString("HH-mm-ss")}.ply");
                using(var matlab = new Matlab())
                {
                    var numPointClouds = matPcPaths.Count();
                    double[,] colors;
                    if(numPointClouds == 2)
                    {
                        colors = new double[2, 3] { { 0, 0, 1 }, { 1, 0, 0 } };
                    }
                    else if(numPointClouds == 3)
                    {
                        colors = new double[3, 3] { { 0, 0, 1 }, { 0, 1, 0 }, { 1, 0, 0 } };
                    }
                    else if(numPointClouds == 4)
                    {
                            colors = new double[4, 3]{{0,0,1},{0,1,0},{0.9,0.5,0},{1,0,0}};
                    }
                    else
                    {
                        colors = GenerateRandomColors(matPcPaths.Count());
                    }
                    matlab.MergePointclouds(matPcPaths, colors, mergedPlyFilePath, denoise:true);
                    Utils.TryOpenFolder(selectedFolder);
                    Utils.OpenPointCloudsInMeshlab(new string[]{mergedPlyFilePath});
                    Utils.PlaySuccessSound();
                }
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
                waitingDialog.Close();
            }
        }

        private double[,] GenerateRandomColors(int length)
        {
            var colors = new double[length,3];
            var random = new Random();
            for(int i = 0; i < length; i++)
            {
                //Minimum to avoid too dark colors
                colors[i,0] = random.Next(60, 255)/255.0; //R
                colors[i,1] = random.Next(60, 255)/255.0; //G
                colors[i,2] = random.Next(60, 255)/255.0; //B
            }
            return colors;
        }

        private void ExportPattern_Click(object sender, RoutedEventArgs e)
        {
            if (TryGetOutputFolder())
            {
                var homogenizationImage = GetHomogenizationImage();
                var phaseShiftingParams = GetPhaseShiftingParametersFromViewModel();
                var pattern = PatternGenerator.GetPatternFromPatternType(ViewModel.SelectedPatternType, (int)ViewModel.PatternWidth, (int)ViewModel.PatternHeight, ViewModel.UserDefinedPatternFolder, ViewModel.IsPatternRotated45, homogenizationImage, phaseShiftingParams);
                PatternGenerator.TryExportPattern(pattern, OutputFolderPath);
            }
        }

        private void PatternInformationButton_Click(object sender, RoutedEventArgs e)
        {
            new PatternOutputInfoDialog().ShowDialog();
        }

        private void SettingsConfig_Click(object sender, RoutedEventArgs e)
        {
            var settingsDialog = new SettingsDialog();
            var dialogResult = settingsDialog.ShowDialog();

            if(dialogResult == true)
            {
                ViewModel.UserDefinedHomogenizationImage = Properties.Settings.Default.HomogenizationImage;
                ViewModel.SquareSizeMmTxt = Properties.Settings.Default.ChessboardSquareSize.ToString(CultureInfo.InvariantCulture);
                ViewModel.ChessboardInnerGridsizeTxt = Utils.CheckerboardSizeToString(Properties.Settings.Default.ChessboardInnerGridsize);
            }

            //var launchPath = Assembly.GetExecutingAssembly().Location + ".config";
            //Process.Start(launchPath);
            //var dialogResult = MessageBox.Show("Program must be restarted to apply configuration changes.\nRestart program now?", "Restart program now?", MessageBoxButton.YesNo);
            //if(dialogResult == MessageBoxResult.Yes)
            //{
            //    RestartApplication();
            //}
        }

        private void PositiveDouble_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = InputVerification.IsPositiveDouble(e.Text); // Don't use value if it does not match regex
        }

        private void PositiveInt_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = InputVerification.IsPositiveInt(e.Text); // Don't use value if it does not match regex
        }

        private void ChessboardSquareSize_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = InputVerification.IsPositiveDouble(e.Text); // Don't use value if it does not match regex
        }

        private void ChessboardInnerGridsize_LostFocus(object sender, RoutedEventArgs e)
        {
            var input = (sender as TextBox).Text;
            InputVerification.TryParseCheckerboard(input, out System.Drawing.Size newGridsize);
        }

        private void ExposureTime_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                TryUpdateExposureTime();
            }
        }

        private void ExposureTime_LostFocus(object sender, RoutedEventArgs e)
        {
            TryUpdateExposureTime();
        }
    }

    public class DeviceStatus
    {
        public Device Device;
        public bool IsConnected;
        public bool IsReady;
        public string Message;
        public int NumberOfDevices;

        public DeviceStatus(Device device, bool isConnected, bool isReady, string message, int numberOfDevices)
        {
            this.Device = device;
            this.IsConnected = isConnected;
            this.IsReady = isReady;
            this.Message = message;
            this.NumberOfDevices = numberOfDevices;
        }
    }

    public enum Device
    {
        Laser = 1,
        Cameras = 2,
        Spectrometer,
    }

    public class VerificationResult
    {
        public bool Verified;
        public string Message;

        public VerificationResult(bool verified, string message)
        {
            this.Verified = verified;
            this.Message = message;
        }
    }
}
