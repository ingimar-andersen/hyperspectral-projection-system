﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver.Extensions
{
    static class FileInfoExtensions
    {
        public static IEnumerable<FileInfo> GetFilesByExtensions(this DirectoryInfo dir, params string[] extensions)
        {
            if (extensions == null)
                throw new ArgumentNullException("extensions");
            IEnumerable<FileInfo> files = dir.EnumerateFiles();
            return files.Where(f => extensions.Contains(f.Extension));
        }
    }

    static class MatrixExtensions
    {
        public static void ExportToFile<TDepth>(this Matrix<TDepth> matrix, string filePath)
        where TDepth : new()
        {
            var outputDirectory = Directory.GetParent(filePath).FullName;
            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }
            using (var sw = new StreamWriter(filePath, append: false))
            {
                sw.Write(matrix.ToStringArray());
            }
        }

        public static string ToStringArray<TDepth>(this Matrix<TDepth> matrix)
            where TDepth : new()
        {
            var sb = new StringBuilder();
            for (int y = 0; y < matrix.Rows; y++)
            {
                for (int x = 0; x < matrix.Cols; x++)
                {
                    var value = matrix.Data[y, x];
                    string valueString;
                    var formattable = value as IFormattable;
                    if (formattable != null)
                    {
                        valueString = formattable.ToString(null, CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        valueString = value.ToString();
                    }

                    sb.Append(valueString);
                    if (x < matrix.Cols - 1) //Tab delimit all except the last one
                    {
                        sb.Append('\t');
                    }
                }
                sb.Append(Environment.NewLine);
            }
            return sb.ToString();
        }
    }

    static class RectangleExtensions
    {
        public static void ExportToFile(this Rectangle rect, string filePath)
        {
            var outputDirectory = Directory.GetParent(filePath).FullName;
            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }
            using (var sw = new StreamWriter(filePath, append: false))
            {
                sw.Write(rect.ToStringXYWH());
            }
        }

        public static string ToStringXYWH(this Rectangle rect)
        {
            var sb = new StringBuilder();
            string valueString;
            valueString = rect.X.ToString(null, CultureInfo.InvariantCulture);
            sb.Append(valueString);
            sb.Append('\t');

            valueString = rect.Y.ToString(null, CultureInfo.InvariantCulture);
            sb.Append(valueString);
            sb.Append('\t');

            valueString = rect.Width.ToString(null, CultureInfo.InvariantCulture);
            sb.Append(valueString);
            sb.Append('\t');

            valueString = rect.Height.ToString(null, CultureInfo.InvariantCulture);
            sb.Append(valueString);
            sb.Append('\t');
            return sb.ToString();
        }
    }

    static class ArrayExtensions
    {
        public static bool[] And(this bool[] array, bool[] other)
        {
            if (array.Length != other.Length) throw new Exception("Arrays must be of equal length");
            var resultArray = new bool[array.Length];    
            for (int i = 0; i < array.Length; i++)
            {
                resultArray[i] = array[i] && other[i];
            }
            return resultArray;
        }

        public static void ExportToJsonFile(this double[,] mat, string filePath)
        {
            var outputDirectory = Directory.GetParent(filePath).FullName;
            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }
            using (var sw = new StreamWriter(filePath, append: false))
            {
                sw.Write(JsonConvert.SerializeObject(mat));
            }
        }

        public static float[,] FromJsonFile(string jsonFilePath)
        {
            using (StreamReader file = File.OpenText(jsonFilePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                return (float[,])serializer.Deserialize(file, typeof(float[,]));
            }
        }
    }

    static class MatExtensions
    {
        public static void ExportToFile(this Mat mat, string filePath)
        {
            var outputDirectory = Directory.GetParent(filePath).FullName;
            if (!Directory.Exists(outputDirectory))
            {
                Directory.CreateDirectory(outputDirectory);
            }
            using (var sw = new StreamWriter(filePath, append: false))
            {
                sw.Write(mat.ToStringArray());
            }
        }

        public static string ToStringArray(this Mat mat)
        {
            var sb = new StringBuilder();
            for (int y = 0; y < mat.Rows; y++)
            {
                for (int x = 0; x < mat.Cols; x++)
                {
                    var value = mat.GetValue(y, x);
                    string valueString;
                    var formattable = value as IFormattable;
                    if (formattable != null)
                    {
                        valueString = formattable.ToString(null, CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        valueString = value.ToString();
                    }

                    sb.Append(valueString);
                    if (x < mat.Cols - 1) //Tab delimit all except the last one
                    {
                        sb.Append('\t');
                    }
                }
                sb.Append(Environment.NewLine);
            }
            return sb.ToString();
        }

        public static double[,] ToDoubleArray(this Mat mat)
        {
            var result = new double[mat.Rows, mat.Cols];
            for (int y = 0; y < mat.Rows; y++)
            {
                for (int x = 0; x < mat.Cols; x++) {
                    result[y, x] = mat.GetValue(y, x);
                }
            }
            return result;
        }

        public static dynamic GetValue(this Mat mat, int row, int col)
        {
            var value = CreateElement(mat.Depth);
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, value, 0, 1);
            return value[0];
        }

        public static void SetValue(this Mat mat, int row, int col, dynamic value)
        {
            var target = CreateElement(mat.Depth, value);
            Marshal.Copy(target, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
        }
        private static dynamic CreateElement(DepthType depthType, dynamic value)
        {
            var element = CreateElement(depthType);
            element[0] = value;
            return element;
        }

        private static dynamic CreateElement(DepthType depthType)
        {
            if (depthType == DepthType.Cv8S)
            {
                return new sbyte[1];
            }
            if (depthType == DepthType.Cv8U)
            {
                return new byte[1];
            }
            if (depthType == DepthType.Cv16S)
            {
                return new short[1];
            }
            if (depthType == DepthType.Cv16U)
            {
                return new ushort[1];
            }
            if (depthType == DepthType.Cv32S)
            {
                return new int[1];
            }
            if (depthType == DepthType.Cv32F)
            {
                return new float[1];
            }
            if (depthType == DepthType.Cv64F)
            {
                return new double[1];
            }
            return new float[1];
        }
    }
}
