﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace LaserDriver
{
    public static class InputVerification
    {
        public static readonly Regex DoubleRegex = new Regex("[^0-9.-]+");
        public static bool IsDouble(string text)
        {
            return DoubleRegex.IsMatch(text);
        }

        public static readonly Regex PositiveDoubleRegex = new Regex("[^0-9.]+");
        public static bool IsPositiveDouble(string text)
        {
            return PositiveDoubleRegex.IsMatch(text);
        }

        public static readonly Regex PositiveIntRegex = new Regex("[^0-9]+");
        public static bool IsPositiveInt(string text)
        {
            return PositiveIntRegex.IsMatch(text);
        }

        internal static bool TryParseCheckerboard(string input, out System.Drawing.Size checkerboardGridSize)
        {
            checkerboardGridSize = System.Drawing.Size.Empty;
            try
            {
                var splitInput = input.Split(',');
                if (splitInput.Count() != 2)
                {
                    throw new ArgumentException("Incorrect number of input parameters. Format should be 'Width, Height'");
                }
                var width = int.Parse(splitInput[0].Trim());
                var height = int.Parse(splitInput[1].Trim());
                if (width <= 0 || height <= 0)
                {
                    throw new ArgumentException("Width and height must be positive non-zero integers.");
                }
                checkerboardGridSize = new System.Drawing.Size(width, height);
                return true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch
            {
                MessageBox.Show($"Size must be defined on the format 'Width, Height'.");
            }
            return false;
        }
    }
}
