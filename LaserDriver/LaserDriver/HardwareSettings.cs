﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LaserDriver.Camera;

namespace LaserDriver
{
    public static class HardwareSettings
    {
        //Projector
        public static readonly int PROJECTOR_WIDTH = 912;
        public static readonly int PROJECTOR_HEIGHT = 1140;

        //Camera
        public static readonly CameraTriggerType TRIGGER_TYPE = CameraTriggerType.Software;
        internal static string TRIGGER_SOURCE = "Line2"; //Red wire on BFS

        //Arduino
        internal static byte TRIGGER_PIN_ARDUINO = 7;
        internal static byte CAMERA_LEFT_POWER_ARDUINO = 2;
        internal static byte CAMERA_RIGHT_POWER_ARDUINO = 4;
        internal static byte ENCLOSURE_LIGHT_POWER = 3; //PWM
        internal static byte SCANNING_LIGHT_POWER = 5; //PWM

        //Filter
        public static int MINIMUM_WAVELENGTH = 504; //Can go down to 500nm but only >= 504nm is usable.
        public static int MAXIMUM_WAVELENGTH = 900;
    }
}
