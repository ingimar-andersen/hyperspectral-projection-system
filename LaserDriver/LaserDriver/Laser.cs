﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LaserDriver
{
    public class Laser
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private object openPortsLock = new object();
        private object portStatusLock = new object();

        private const byte SELECT_DEVICE_ID = 6;
        private const byte EXTREME_DEVICE_ID = 15;
        private string comPort;
        public string ComPort
        {
            get { return comPort; }
            set
            {
                try
                {
                    var portResult = OpenPorts(value);
                    //if(portResult == NKTPDLL.PortResultTypes.OPSuccess)
                    //{
                    //    comPort = value;
                    //}
                    //TODO: handle other results
                }
                catch (Exception ex)
                {
                    log.Error($"Error setting COM-port: {ex.Message}");
                }
                finally
                {
                    comPort = value;
                }
            }
        }
        public NktSelect Select = new NktSelect();
        public NktExtreme Extreme = new NktExtreme();
        public static PortStatus LatestPortStatus { get; private set; } = PortStatus.Disconnected;

        public Laser(string comPort) {
            this.ComPort = comPort;
        }

        public PortStatus GetPortStatus()
        {
            lock (portStatusLock)
            {
                try
                {
                    NKTPDLL.PortStatusTypes portStatus = NKTPDLL.PortStatusTypes.PortStatusUnknown;
                    var result = NKTPDLL.getPortStatus(ComPort, ref portStatus);
                    //Open port if closed
                    if (result == NKTPDLL.PortResultTypes.OPSuccess && portStatus == NKTPDLL.PortStatusTypes.PortClosed)
                    {
                        var openPortResult = OpenPorts(ComPort);
                        result = NKTPDLL.getPortStatus(ComPort, ref portStatus);
                        if (result == NKTPDLL.PortResultTypes.OPSuccess && portStatus == NKTPDLL.PortStatusTypes.PortReady)
                        {
                            LatestPortStatus = PortStatus.Connected;
                            return LatestPortStatus;
                        }
                    }
                    if (result == NKTPDLL.PortResultTypes.OPSuccess && portStatus == NKTPDLL.PortStatusTypes.PortReady)
                    {
                        LatestPortStatus = PortStatus.Connected;
                        return LatestPortStatus;
                    }

                    LatestPortStatus = PortStatus.Disconnected;
                    return LatestPortStatus;
                }
                catch (Exception e)
                {
                    log.Error("Error getting port status.", e);
                    LatestPortStatus = PortStatus.Error;
                    return LatestPortStatus;
                }
            }
        }


        private NKTPDLL.PortResultTypes OpenPorts(string portNames)
        {
            lock (openPortsLock)
            {
                byte autoMode = 0;
                //TODO: implement checkboxes
                //if (checkBoxAutoMode.Checked) autoMode = 1;
                byte liveMode = 0;
                //if (checkBoxLiveMode.Checked) liveMode = 1;
                NKTPDLL.PortResultTypes result = NKTPDLL.openPorts(portNames, autoMode, liveMode);
                LogPortResultTypes("openPorts(\"" + portNames + "\", " + autoMode.ToString() + ", " + liveMode.ToString() + ")", result);
                return result;
            }
        }

        internal NKTPDLL.PortResultTypes Reconnect()
        {
            return OpenPorts(ComPort);
        }

        private void LogPortResultTypes(string funcName, NKTPDLL.PortResultTypes result)
        {
            log.Info(funcName + ": " + NKTPDLL.getPortResultMsg(result));
        }

        public class NktSelect
        {
            public static readonly int MIN_WAVELENGTH = HardwareSettings.MINIMUM_WAVELENGTH * 1000; //Scaled
            public static readonly int MAX_WAVELENGTH = HardwareSettings.MAXIMUM_WAVELENGTH * 1000; //Scaled

            public static readonly int MIN_AMPLITUDE = 0;
            public static readonly int MAX_AMPLITUDE = 1000; // Scaled

            public readonly ControlDefinitionInt RfPower = new ControlDefinitionInt(registryAddress: 0x30, deviceId: SELECT_DEVICE_ID, unit: "0=Off;1=On", dataType: NktDataType.U8, scalingFactor: 1, minValue: 0, maxValue: 1);
            public readonly ControlDefinitionInt MinimumWavelength = new ControlDefinitionInt(registryAddress: 0x34, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, isReadOnly: true);
            public readonly ControlDefinitionInt MaximumWavelength = new ControlDefinitionInt(registryAddress: 0x35, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, isReadOnly: true);
            public readonly ControlDefinitionInt CrystalTemperature = new ControlDefinitionInt(registryAddress: 0x38, deviceId: SELECT_DEVICE_ID, unit: "°C", dataType: NktDataType.I16, scalingFactor: 0.1, isReadOnly: true);
            public readonly ControlDefinitionInt Wavelength0 = new ControlDefinitionInt(registryAddress: 0x90, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, minValue: MIN_WAVELENGTH, maxValue: MAX_WAVELENGTH);
            public readonly ControlDefinitionInt Wavelength1 = new ControlDefinitionInt(registryAddress: 0x91, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, minValue: MIN_WAVELENGTH, maxValue: MAX_WAVELENGTH);
            public readonly ControlDefinitionInt Wavelength2 = new ControlDefinitionInt(registryAddress: 0x92, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, minValue: MIN_WAVELENGTH, maxValue: MAX_WAVELENGTH);
            public readonly ControlDefinitionInt Wavelength3 = new ControlDefinitionInt(registryAddress: 0x93, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, minValue: MIN_WAVELENGTH, maxValue: MAX_WAVELENGTH);
            public readonly ControlDefinitionInt Wavelength4 = new ControlDefinitionInt(registryAddress: 0x94, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, minValue: MIN_WAVELENGTH, maxValue: MAX_WAVELENGTH);
            public readonly ControlDefinitionInt Wavelength5 = new ControlDefinitionInt(registryAddress: 0x95, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, minValue: MIN_WAVELENGTH, maxValue: MAX_WAVELENGTH);
            public readonly ControlDefinitionInt Wavelength6 = new ControlDefinitionInt(registryAddress: 0x96, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, minValue: MIN_WAVELENGTH, maxValue: MAX_WAVELENGTH);
            public readonly ControlDefinitionInt Wavelength7 = new ControlDefinitionInt(registryAddress: 0x97, deviceId: SELECT_DEVICE_ID, unit: "nm", dataType: NktDataType.U32, scalingFactor: 0.001, minValue: MIN_WAVELENGTH, maxValue: MAX_WAVELENGTH);
            public readonly ControlDefinitionInt Amplitude0 = new ControlDefinitionInt(registryAddress: 0xB0, deviceId: SELECT_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1, minValue: MIN_AMPLITUDE, maxValue: MAX_AMPLITUDE);
            public readonly ControlDefinitionInt Amplitude1 = new ControlDefinitionInt(registryAddress: 0xB1, deviceId: SELECT_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1, minValue: MIN_AMPLITUDE, maxValue: MAX_AMPLITUDE);
            public readonly ControlDefinitionInt Amplitude2 = new ControlDefinitionInt(registryAddress: 0xB2, deviceId: SELECT_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1, minValue: MIN_AMPLITUDE, maxValue: MAX_AMPLITUDE);
            public readonly ControlDefinitionInt Amplitude3 = new ControlDefinitionInt(registryAddress: 0xB3, deviceId: SELECT_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1, minValue: MIN_AMPLITUDE, maxValue: MAX_AMPLITUDE);
            public readonly ControlDefinitionInt Amplitude4 = new ControlDefinitionInt(registryAddress: 0xB4, deviceId: SELECT_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1, minValue: MIN_AMPLITUDE, maxValue: MAX_AMPLITUDE);
            public readonly ControlDefinitionInt Amplitude5 = new ControlDefinitionInt(registryAddress: 0xB5, deviceId: SELECT_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1, minValue: MIN_AMPLITUDE, maxValue: MAX_AMPLITUDE);
            public readonly ControlDefinitionInt Amplitude6 = new ControlDefinitionInt(registryAddress: 0xB6, deviceId: SELECT_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1, minValue: MIN_AMPLITUDE, maxValue: MAX_AMPLITUDE);
            public readonly ControlDefinitionInt Amplitude7 = new ControlDefinitionInt(registryAddress: 0xB7, deviceId: SELECT_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1, minValue: MIN_AMPLITUDE, maxValue: MAX_AMPLITUDE);
        }

        public class NktExtreme
        {
            public readonly ControlDefinitionInt Emission = new ControlDefinitionInt(registryAddress: 0x30, deviceId: EXTREME_DEVICE_ID, unit: "0=Off;3=On", dataType: NktDataType.U8, scalingFactor: 1);
            public readonly ControlDefinitionInt Interlock = new ControlDefinitionInt(registryAddress: 0x32, deviceId: EXTREME_DEVICE_ID, unit: "(>0=reset interlock)", dataType: NktDataType.U16, scalingFactor: 1);
            public readonly ControlDefinitionInt PowerLevel = new ControlDefinitionInt(registryAddress: 0x37, deviceId: EXTREME_DEVICE_ID, unit: "%", dataType: NktDataType.U16, scalingFactor: 0.1);
        }
    }

    public enum PortStatus
    {
        Error = 0,
        Connected = 1,
        Disconnected = 2,
    }

    public enum NktDataType
    {
        U8 = 0,     // Unsigned 8-bit Integer
        U16 = 1,    // Unsigned 16-bit Integer
        U32 = 2,    // Unsigned 32-bit Integer
        I8 = 3,     // Signed 8-bit Integer
        I16 = 4,    // Signed 16-bit Integer
        I32 = 5,    // Signed 32-bit Integer
        H8 = 6,     // Hexadecimal 8-bit Integer
        H16 = 7,    // Hexadecimal 16-bit Integer
        H32 = 8,    // Hexadecimal 32-bit Integer
        String = 9, // Text string
    }


    public class ControlDefinition
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public readonly byte RegistryAddress;
        public readonly byte DeviceId;
        public readonly string Unit;
        public readonly NktDataType DataType;
        public readonly double ScalingFactor;
        public readonly bool IsReadOnly;

        public ControlDefinition(byte registryAddress, byte deviceId, string unit, NktDataType dataType, double scalingFactor, bool isReadOnly = false)
        {
            this.RegistryAddress = registryAddress;
            this.DeviceId = deviceId;
            this.Unit = unit;
            this.DataType = dataType;
            this.ScalingFactor = scalingFactor;
            this.IsReadOnly = isReadOnly;
        }

        public void LogRegisterResultTypes(string funcName, NKTPDLL.RegisterResultTypes result)
        {
            log.Info(funcName + ": " + NKTPDLL.getRegisterResultMsg(result));
        }
    }

    public class ControlDefinitionInt : ControlDefinition
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int? MinValue;
        public int? MaxValue;

        public ControlDefinitionInt(byte registryAddress, byte deviceId, string unit, NktDataType dataType, double scalingFactor, bool isReadOnly = false, int? minValue = null, int? maxValue = null) : base(registryAddress, deviceId, unit, dataType, scalingFactor, isReadOnly)
        {
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }

        public bool TrySetValue(double value, string comPort)
        {
            return TrySetValueScaled((int)Math.Round(value / ScalingFactor), comPort);
        }

        private object setValueLock = new object();
        public bool TrySetValueScaled(int value, string comPort)
        {
            lock (setValueLock)
            {
                if (Laser.LatestPortStatus != PortStatus.Connected) return false; //Avoid issues when laser is turned off but connected
                try {
                    if (IsReadOnly)
                    {
                        MessageBox.Show("Value is read-only");
                        return false;
                    }
                    if (!IsValueWithinRange(value))
                    {
                        MessageBox.Show($"Value is not within range: {MinValue * ScalingFactor}-{MaxValue * ScalingFactor}");
                        return false;
                    }

                    NKTPDLL.RegisterResultTypes result;

                    Int16 index = Convert.ToInt16(-1);
                    switch (this.DataType)
                    {
                        case NktDataType.U8:
                            result = NKTPDLL.registerWriteU8(comPort, DeviceId, RegistryAddress, Convert.ToByte(value), index);
                            break;
                        case NktDataType.U16:
                            var val = Convert.ToUInt16(value);
                            result = NKTPDLL.registerWriteU16(comPort, DeviceId, RegistryAddress, val, index);
                            break;
                        case NktDataType.U32:
                            result = NKTPDLL.registerWriteU32(comPort, DeviceId, RegistryAddress, Convert.ToUInt32(value), index);
                            break;
                        default:
                            throw new NotImplementedException($"Setter for datatype {this.DataType} is not implemented");
                    }

                    LogRegisterResultTypes("registerWrite(\"" + comPort + "\", " + this.DeviceId.ToString() + ", 0x" + this.RegistryAddress.ToString("X2") + ", " + value.ToString() + ")", result);
                    return result == NKTPDLL.RegisterResultTypes.RegResultSuccess;
                }
                catch(Exception ex)
                {
                    log.Error($"NKTPDLL:Error writing value.", ex);
                    return false;
                }
            }
        }

        public bool TryGetStringValue(string comPort, out string value)
        {
            if (TryGetValue(comPort, out double valueDouble))
            {
                value = valueDouble.ToString("0.0", System.Globalization.CultureInfo.InvariantCulture);
                return true;
            }
            value = "";
            return false;
        }

        public bool TryGetValue(string comPort, out double value)
        {
            if (TryGetValueScaled(comPort, out int scaledValue))
            {
                value = scaledValue * ScalingFactor;
                return true;
            }
            value = 0;
            return false;
        }

        object getValueLock = new object();
        public bool TryGetValueScaled(string comPort, out int value)
        {
            lock (getValueLock)
            {
                if (Laser.LatestPortStatus != PortStatus.Connected)
                {
                    value = int.MinValue;
                    return false; //Avoid issues when laser is turned off but connected
                }
                try {
                    NKTPDLL.RegisterResultTypes result;
                    Int16 index = Convert.ToInt16(-1);
                    switch (this.DataType)
                    {
                        case NktDataType.U8:
                            byte valueU8 = byte.MinValue;
                            result = NKTPDLL.registerReadU8(comPort, DeviceId, RegistryAddress, ref valueU8, index);
                            value = Convert.ToInt32(valueU8);
                            break;
                        case NktDataType.U16:
                            ushort valueU16 = ushort.MinValue;
                            result = NKTPDLL.registerReadU16(comPort, DeviceId, RegistryAddress, ref valueU16, index);
                            value = Convert.ToInt32(valueU16);
                            break;
                        case NktDataType.U32:
                            UInt32 valueU32 = UInt32.MinValue;
                            result = NKTPDLL.registerReadU32(comPort, DeviceId, RegistryAddress, ref valueU32, index);
                            value = Convert.ToInt32(valueU32);
                            break;
                        default:
                            throw new NotImplementedException($"Setter for datatype {this.DataType} is not implemented");
                    }
                    //LogRegisterResultTypes("registerRead(\"" + comPort + "\", " + this.DeviceId.ToString() + ", 0x" + this.RegistryAddress.ToString("X2") + ", " + value.ToString() + ")", result);
                    return result == NKTPDLL.RegisterResultTypes.RegResultSuccess;
                }
                catch(Exception ex)
                {
                    log.Error($"NKTPDLL:Error reading valu.", ex);
                    value = int.MinValue;
                    return false;
                }
            }
        }

        private bool IsValueWithinRange(int value)
        {
            bool isOk = true;
            if (MinValue != null)
            {
                isOk = isOk && value >= MinValue;
            }
            if(MaxValue != null)
            {
                isOk = isOk && value <= MaxValue;
            }
            return isOk;
        }
    }

    public class ControlDefinitionString : ControlDefinition
    {

        public ControlDefinitionString(byte registryAddress, byte deviceId, string unit, bool isReadOnly = false) : base(registryAddress, deviceId, unit, dataType: NktDataType.String, scalingFactor: 0, isReadOnly: isReadOnly)
        {

        }

        public void SetValue(string value, string comPort)
        {
            if (IsReadOnly)
            {
                MessageBox.Show("Value is read-only");
                return;
            }
            NKTPDLL.registerWriteAscii(comPort, this.DeviceId, this.RegistryAddress, value, writeEOL: 0, index: -1);
        }

        public string GetValue(string comPort)
        {
            byte maxLen = 255;
            var valueSb = new StringBuilder(maxLen);
            var result = NKTPDLL.registerReadAscii(comPort, this.DeviceId, this.RegistryAddress, valueSb, ref maxLen, index: -1);
            var value = valueSb.ToString();
            LogRegisterResultTypes("registerReadAscii(\"" + comPort + "\", " + this.DeviceId.ToString() + ", 0x" + this.RegistryAddress.ToString("X2") + ", " + "\"\"" + ") : " + value, result);
            return value.ToString();
        }
    }
}
