﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver.Dialogs
{
    public class AcquireImagesDialogVM : INotifyPropertyChanged
    {
        public AcquireImagesDialogVM(double exposureTimeMs)
        {
            this.ExposureTimeMsTxt = exposureTimeMs.ToString(CultureInfo.InvariantCulture);
        }

        private int imageCount = 0;
        public int ImageCount
        {
            get { return this.imageCount; }
            set
            {
                if(this.imageCount != value)
                {
                    this.imageCount = value;
                    NotifyPropertyChanged("ImageCount");
                }
            }
        }

        internal bool isUiEnabled = true;
        public bool IsUiEnabled
        {
            get { return this.isUiEnabled; }
            set
            {
                if (this.isUiEnabled != value)
                {
                    this.isUiEnabled = value;
                    this.NotifyPropertyChanged("IsUiEnabled");
                }
            }
        }

        public double? ExposureTimeMs
        {
            get
            {
                double value;
                return double.TryParse(ExposureTimeMsTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string exposureTimeMsTxt;
        public string ExposureTimeMsTxt
        {
            get { return this.exposureTimeMsTxt; }
            set
            {
                if (this.exposureTimeMsTxt != value)
                {
                    this.exposureTimeMsTxt = value;
                    this.NotifyPropertyChanged("ExposureTimeMsTxt");
                }
            }
        }

        private byte enclosureLightIntensity;
        public byte EnclosureLightIntensity
        {
            get => enclosureLightIntensity;
            set
            {
                if (this.enclosureLightIntensity != value)
                {
                    this.enclosureLightIntensity = value;
                    this.NotifyPropertyChanged("EnclosureLightIntensity");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
