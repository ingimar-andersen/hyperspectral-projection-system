﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver.Dialogs
{
    public class SettingsDialogVM : INotifyPropertyChanged
    {
        public SettingsDialogVM()
        {
            ArduinoComPort = HW.Properties.Settings.Default.ComPort;
            ChessboardInnerGridsizeTxt = Utils.CheckerboardSizeToString(Properties.Settings.Default.ChessboardInnerGridsize);
            ChessboardSquareSizeTxt = Properties.Settings.Default.ChessboardSquareSize.ToString(CultureInfo.InvariantCulture);
            ExposureScalingFilepath = Properties.Settings.Default.ExposureScalingFilepath;
            HomogenizationImage = Properties.Settings.Default.HomogenizationImage;
            LaserComPort = Properties.Settings.Default.LaserComPort;
            MatlabScriptsFolder = Properties.Settings.Default.MatlabScriptsFolder;
            OutputFolder = Properties.Settings.Default.OutputFolder;
            StereoCalibParamsFolder = Properties.Settings.Default.StereoCalibParamsFolder;
        }

        private string arduinoComPort;
        public string ArduinoComPort
        {
            get { return this.arduinoComPort; }
            set
            {
                if (this.arduinoComPort != value)
                {
                    this.arduinoComPort = value;
                    this.NotifyPropertyChanged("ArduinoComPort");
                }
            }
        }

        public double? ChessboardSquareSize
        {
            get
            {
                double value;
                return double.TryParse(ChessboardSquareSizeTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string chessboardSquareSizeTxt;
        public string ChessboardSquareSizeTxt
        {
            get { return this.chessboardSquareSizeTxt; }
            set
            {
                if (this.chessboardSquareSizeTxt != value)
                {
                    this.chessboardSquareSizeTxt = value;
                    this.NotifyPropertyChanged("ChessboardSquareSizeTxt");
                }
            }
        }

        private string chessboardInnerGridsizeTxt;
        public string ChessboardInnerGridsizeTxt
        {
            get { return this.chessboardInnerGridsizeTxt; }
            set
            {
                if (this.chessboardInnerGridsizeTxt != value)
                {
                    this.chessboardInnerGridsizeTxt = value;
                    this.NotifyPropertyChanged("ChessboardInnerGridsizeTxt");
                }
            }
        }

        private string exposureScalingFilepath;
        public string ExposureScalingFilepath
        {
            get { return this.exposureScalingFilepath; }
            set
            {
                if (this.exposureScalingFilepath != value)
                {
                    this.exposureScalingFilepath = value;
                    this.NotifyPropertyChanged("ExposureScalingFilepath");
                }
            }
        }

        private string homogenizationImage;
        public string HomogenizationImage
        {
            get { return this.homogenizationImage; }
            set
            {
                if (this.homogenizationImage != value)
                {
                    this.homogenizationImage = value;
                    this.NotifyPropertyChanged("HomogenizationImage");
                }
            }
        }

        private string laserComPort;
        public string LaserComPort
        {
            get { return this.laserComPort; }
            set
            {
                if (this.laserComPort != value)
                {
                    this.laserComPort = value;
                    this.NotifyPropertyChanged("LaserComPort");
                }
            }
        }

        private string matlabScriptsFolder;
        public string MatlabScriptsFolder
        {
            get { return this.matlabScriptsFolder; }
            set
            {
                if (this.matlabScriptsFolder != value)
                {
                    this.matlabScriptsFolder = value;
                    this.NotifyPropertyChanged("MatlabScriptsFolder");
                }
            }
        }

        private string outputFolder;
        public string OutputFolder
        {
            get { return this.outputFolder; }
            set
            {
                if (this.outputFolder != value)
                {
                    this.outputFolder = value;
                    this.NotifyPropertyChanged("OutputFolder");
                }
            }
        }

        private string stereoCalibParamsFolder;
        public string StereoCalibParamsFolder
        {
            get { return this.stereoCalibParamsFolder; }
            set
            {
                if (this.stereoCalibParamsFolder != value)
                {
                    this.stereoCalibParamsFolder = value;
                    this.NotifyPropertyChanged("StereoCalibParamsFolder");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
