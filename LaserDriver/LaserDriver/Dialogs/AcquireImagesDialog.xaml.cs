﻿using HW;
using SpinnakerNET.GUI;
using SpinnakerNET.GUI.WPFControls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using static SpinnakerNET.GUI.WPFControls.PersistentSettings;

namespace LaserDriver.Dialogs
{
    /// <summary>
    /// Interaction logic for AcquireImagesDialog.xaml
    /// </summary>
    public partial class AcquireImagesDialog : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Camera camera;
        private ImageDrawingControl leftCameraControl;
        private ImageDrawingControl rightCameraControl;
        public List<string> OutputPaths = new List<string>();

        public AcquireImagesDialog(AcquireImagesDialogVM viewModel, Camera camera, string outputFolderPath)
        {
            this.outputFolderPath = outputFolderPath;
            this.camera = camera;
            InitializeComponent();
            this.DataContext = viewModel;
        }

        private AcquireImagesDialogVM ViewModel
        {
            get { return this.DataContext as AcquireImagesDialogVM; }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.EnclosureLightIntensity = byte.MaxValue;
            ArduinoHelper.TurnOnEnclosureLight(ViewModel.EnclosureLightIntensity);

            camera.SetExposureTimeForCameras((double)ViewModel.ExposureTimeMs);
            LeftCameraGrid.Children.Clear();
            RightCameraGrid.Children.Clear();

            var leftCamera = camera.CamList.SingleOrDefault(x => x.SerialNumber == Camera.LEFT_CAMERA_SERIAL_NO);
            if (leftCamera == null)
            {
                MessageBox.Show("Left camera not connected");
                return;
            }
            var cameraGuiLeft = new GUIFactory();
            leftCamera.Camera.Init();
            cameraGuiLeft.ConnectGUILibrary(leftCamera.Camera);
            leftCameraControl = cameraGuiLeft.GetImageDrawingControl() as ImageDrawingControl;
            leftCameraControl.HorizontalAlignment = HorizontalAlignment.Stretch;
            leftCameraControl.VerticalAlignment = VerticalAlignment.Stretch;
            leftCameraControl.Connect(leftCamera.Camera, leftCamera.NodeMap);
            leftCameraControl.Start();
            LeftCameraGrid.Children.Add(leftCameraControl);


            var rightCamera = camera.CamList.SingleOrDefault(x => x.SerialNumber == Camera.RIGHT_CAMERA_SERIAL_NO);
            if (rightCamera == null)
            {
                MessageBox.Show("Right camera not connected");
                return;
            }
            var cameraGuiRight = new GUIFactory();
            rightCamera.Camera.Init();
            cameraGuiRight.ConnectGUILibrary(rightCamera.Camera);
            rightCameraControl = cameraGuiRight.GetImageDrawingControl() as ImageDrawingControl;
            rightCameraControl.Connect(rightCamera.Camera, rightCamera.NodeMap);
            rightCameraControl.Start();
            rightCameraControl.HorizontalAlignment = HorizontalAlignment.Stretch;
            rightCameraControl.VerticalAlignment = VerticalAlignment.Stretch;
            RightCameraGrid.Children.Add(rightCameraControl);

            CenterWindowOnScreen();
        }

        private static readonly Regex positiveDoubleRegex = new Regex("[^0-9.]+");
        private void PositiveDouble_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = positiveDoubleRegex.IsMatch(e.Text); // Don't use value if it does not match regex
        }

        private string timeStamp = "";
        private string outputFolderPath = "";
        private async void TakeImage_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.IsUiEnabled = false;
            try
            {
                Utils.PlayCameraFocusSound();
                var exposureTimeMs = (double)ViewModel.ExposureTimeMs;

                camera.SetExposureTimeForCameras(exposureTimeMs);

                //Todo: optimally this timestamp should also be used for metadata filename.
                if (string.IsNullOrWhiteSpace(timeStamp))
                {
                    timeStamp = DateTime.Now.ToString("HH-mm-ss");
                }
                camera.BeginAquisitionAll();
                //var result = MessageBox.Show($"Take picture: {pictureCount}", "Calibration", MessageBoxButton.YesNo, MessageBoxImage.Question);
                //if (result == MessageBoxResult.No)
                //{
                //    break;
                //}
                var imageCount = ViewModel.ImageCount + 1;
                var id = imageCount.ToString().PadLeft(3, '0') + "_" + timeStamp;
                var images = await camera.AcquireSingleImageAllCameras(Camera.CameraTriggerType.Off);
                var tasks = images.Select(x => x.Save(Path.Combine(outputFolderPath, x.CameraId), id));
                var filePaths = await Task.WhenAll(tasks.ToArray());
                ViewModel.ImageCount = imageCount;
                OutputPaths.AddRange(filePaths);
                Utils.PlayCameraShutterSound();
            }
            catch (Exception ex)
            {
                log.Error("Error during camera calibration images acquisition.", ex);   
            }
            finally
            {
                ViewModel.IsUiEnabled = true;
            }
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            leftCameraControl.Disconnect();
            leftCameraControl.Dispose();

            rightCameraControl.Disconnect();
            rightCameraControl.Dispose();
        }

        private void OpenOutputFolder_Click(object sender, RoutedEventArgs e)
        {
            if (!Utils.TryOpenFolder(outputFolderPath))
            {
                MessageBox.Show("Unable to open output folder path");
            }
        }

        private void ToggleEnclosureLight_Click(object sender, RoutedEventArgs e)
        {
            if (Arduino.OutputStateByPinNumber.TryGetValue(HardwareSettings.ENCLOSURE_LIGHT_POWER, out Tuple<byte, Arduino.WriteMode> value))
            {
                if (value.Item1 >= 1)
                {
                    ArduinoHelper.TurnOffEnclosureLight();
                }
                else
                {
                    ArduinoHelper.TurnOnEnclosureLight(ViewModel.EnclosureLightIntensity);
                }
            }
        }

        private void EnclosureLightSlider_Drop(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                Arduino.Instance.AnalogWrite(HardwareSettings.ENCLOSURE_LIGHT_POWER, ViewModel.EnclosureLightIntensity);
            }
            catch { }
        }

        private void ExposureTime_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    camera.SetExposureTimeForCameras((double)ViewModel.ExposureTimeMs);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ExposureTime_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                camera.SetExposureTimeForCameras((double)ViewModel.ExposureTimeMs);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Finish_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }
    }
}
