﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver.Dialogs
{
    public class WaitingDialogVM : INotifyPropertyChanged
    {
        private string message;
        public string Message
        {
            get { return this.message; }
            set
            {
                if (this.message != value)
                {
                    this.message = value;
                    this.NotifyPropertyChanged("Message");
                }
            }
        }

        private string header;
        public string Header
        {
            get { return this.header; }
            set
            {
                if (this.header != value)
                {
                    this.header = value;
                    this.NotifyPropertyChanged("Header");
                }
            }
        }

        public WaitingDialogVM(string message, string header)
        {
            this.Message = message;
            this.Header = header;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
