﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LaserDriver.Dialogs
{
    /// <summary>
    /// Interaction logic for CameraSettingsDialog.xaml
    /// </summary>
    public partial class CameraSettingsDialog : Window
    {
        public List<Tuple<string, bool>> ActiveCameraIds = new List<Tuple<string, bool>>();

        public CameraSettingsDialogVM ViewModel
        {
            get { return this.DataContext as CameraSettingsDialogVM; }
        }

        public CameraSettingsDialog(CameraSettingsDialogVM viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            foreach(var cam in ViewModel.Cameras)
            {
                ActiveCameraIds.Add(new Tuple<string, bool>(cam.Id, cam.IsActive));
            }
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
