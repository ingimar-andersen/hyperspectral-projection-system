﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver.Dialogs
{
    public class CameraSettingsDialogVM : INotifyPropertyChanged
    {
        private ObservableCollection<ViewCameraSetting> cameras;
        public ObservableCollection<ViewCameraSetting> Cameras
        {
            get { return cameras; }
            set
            {
                if (value != cameras)
                {
                    cameras = value;
                    NotifyPropertyChanged("Cameras");
                }
            }
        }

        public CameraSettingsDialogVM(List<ManagedCamera> cameras)
        {
            var camerasTemp = new ObservableCollection<ViewCameraSetting>();
            if (cameras != null)
            {
                foreach (var cam in cameras)
                {
                    camerasTemp.Add(new ViewCameraSetting("", cam.DeviceId, cam.UseForAcquisition));
                }
            }
            Cameras = camerasTemp;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public class ViewCameraSetting
        {
            public string Name { get; set; }
            public string Id { get; set; }

            public bool isActive;
            public bool IsActive {
                get { return isActive; }
                set
                {
                    if(value != isActive)
                    {
                        isActive = value;
                        NotifyPropertyChanged("IsActive");
                    }
                }
            }

            public ViewCameraSetting(string name, string id, bool isActive)
            {
                this.Name = name;
                this.Id = id;
                this.IsActive = isActive;
            }

            public event PropertyChangedEventHandler PropertyChanged;

            public void NotifyPropertyChanged(string propName)
            {
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }
        }
    }

   
}
