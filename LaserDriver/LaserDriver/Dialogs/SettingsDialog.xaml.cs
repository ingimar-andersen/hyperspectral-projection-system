﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LaserDriver.Dialogs
{
    /// <summary>
    /// Interaction logic for SettingsDialog.xaml
    /// </summary>
    public partial class SettingsDialog : Window
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int LabelWidth = 200;
        private bool IsRestartRequired = false;

        public SettingsDialogVM ViewModel
        {
            get { return this.DataContext as SettingsDialogVM; }
        }

        public System.Drawing.Size ChessboardInnerGridsize = System.Drawing.Size.Empty;

        public SettingsDialog()
        {
            this.DataContext = new SettingsDialogVM();
            InitializeComponent();
        }

        public void SaveAll()
        {
            SaveArduinoComport();
            SaveChessboardInnerGridsize();
            SaveChessboardSquareSize();
            SaveExposureScalingFilePath();
            SaveHomogenizationImage();
            SaveLaserComPort();
            SaveMatlabsScriptFolder();
            SaveOutputFolder();
            SaveStereoCalibParamsFolder();

            if (IsRestartRequired)
            {
                var dialogResult = MessageBox.Show("Some of saved settings require a program restart before taking effect.\nRestart the program?", "Restart the program?", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if(dialogResult == MessageBoxResult.Yes)
                {
                    Utils.RestartApplication();
                }
            }
        }

        private void SaveChessboardSquareSize()
        {
            try
            {
                if (Properties.Settings.Default.ChessboardSquareSize == (double)ViewModel.ChessboardSquareSize) return;
                Properties.Settings.Default.ChessboardSquareSize = (double)ViewModel.ChessboardSquareSize;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                log.Error("Error saving new settings for ChessboardSquareSize.", ex);
            }
        }

        private void SaveArduinoComport()
        {
            try
            {
                if (HW.Properties.Settings.Default.ComPort == ViewModel.ArduinoComPort) return;
                HW.Properties.Settings.Default.ComPort= ViewModel.ArduinoComPort;
                HW.Properties.Settings.Default.Save();
                IsRestartRequired = true;
            }
            catch (Exception ex)
            {
                log.Error("Error saving new settings for ComPort (Arduino).", ex);
            }
        }

        private void SaveStereoCalibParamsFolder()
        {
            try
            {
                if (Properties.Settings.Default.StereoCalibParamsFolder == ViewModel.StereoCalibParamsFolder) return;
                Properties.Settings.Default.StereoCalibParamsFolder = ViewModel.StereoCalibParamsFolder;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                log.Error("Error saving new settings for StereoCalibParamsFolder.", ex);
            }
        }

        private void SaveOutputFolder()
        {
            try
            {
                if (Properties.Settings.Default.OutputFolder == ViewModel.OutputFolder) return;
                Properties.Settings.Default.OutputFolder = ViewModel.OutputFolder;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                log.Error("Error saving new settings for OutputFolder.", ex);
            }
        }

        private void SaveMatlabsScriptFolder()
        {
            try
            {
                if (Properties.Settings.Default.MatlabScriptsFolder == ViewModel.MatlabScriptsFolder) return;
                Properties.Settings.Default.MatlabScriptsFolder = ViewModel.MatlabScriptsFolder;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                log.Error("Error saving new settings for LaserComPort.", ex);
            }
        }

        private void SaveLaserComPort()
        {
            try
            {
                if (Properties.Settings.Default.LaserComPort == ViewModel.LaserComPort) return;
                Properties.Settings.Default.LaserComPort = ViewModel.LaserComPort;
                Properties.Settings.Default.Save();
                IsRestartRequired = true;
            }
            catch (Exception ex)
            {
                log.Error("Error saving new settings for LaserComPort.", ex);
            }
        }

        private void SaveExposureScalingFilePath()
        {
            try
            {
                if (Properties.Settings.Default.ExposureScalingFilepath == ViewModel.ExposureScalingFilepath) return;
                Properties.Settings.Default.ExposureScalingFilepath = ViewModel.ExposureScalingFilepath;
                Properties.Settings.Default.Save();
                IsRestartRequired = true;
            }
            catch (Exception ex)
            {
                log.Error("Error saving new settings for ExposureScalingFilepath.", ex);
            }
        }

        private void SaveChessboardInnerGridsize()
        {
            if(!ChessboardInnerGridsize.IsEmpty && Properties.Settings.Default.ChessboardInnerGridsize != ChessboardInnerGridsize)
            {
                try
                {
                    Properties.Settings.Default.ChessboardInnerGridsize = ChessboardInnerGridsize;
                    Properties.Settings.Default.Save();
                }
                catch (Exception ex)
                {
                    log.Error("Error saving new settings for ChessboardInnerGridsize.", ex);
                }
            }
        }

        private void SaveHomogenizationImage()
        {
            try
            {
                if (Properties.Settings.Default.HomogenizationImage == ViewModel.HomogenizationImage) return;
                Properties.Settings.Default.HomogenizationImage = ViewModel.HomogenizationImage;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                log.Error("Error saving new settings for HomogenizationImage.", ex);
            }
        }

        private void ChessboardInnerGridsize_LostFocus(object sender, RoutedEventArgs e)
        {
            var input = (sender as TextBox).Text;
            if(InputVerification.TryParseCheckerboard(input, out System.Drawing.Size newGridSize))
            {
                ChessboardInnerGridsize = newGridSize;
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveAll();
            this.DialogResult = true;
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            Close();
        }

        private void ChessboardSquareSize_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = InputVerification.IsPositiveDouble(e.Text);
        }
    }
}
