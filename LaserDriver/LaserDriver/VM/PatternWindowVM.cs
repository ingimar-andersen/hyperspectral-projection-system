﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static LaserDriver.PatternGenerator;

namespace LaserDriver.VM
{
    public class PatternWindowVM : INotifyPropertyChanged
    {
        public PatternType PatternType;

        private bool isToolbarVisible;
        public bool IsToolbarVisible
        {
            get { return this.isToolbarVisible; }
            set
            {
                if (this.isToolbarVisible != value)
                {
                    this.isToolbarVisible = value;
                    this.NotifyPropertyChanged("IsToolbarVisible");
                }
            }
        }

        private string windowHeight;
        public string WindowHeight
        {
            get { return this.windowHeight; }
            set
            {
                if (this.windowHeight != value)
                {
                    this.windowHeight = value;
                    this.NotifyPropertyChanged("WindowHeight");
                }
            }
        }

        public bool IsRotated45 { get; set; }

        private string windowWidth;
        public string WindowWidth
        {
            get { return this.windowWidth; }
            set
            {
                if (this.windowWidth != value)
                {
                    this.windowWidth = value;
                    this.NotifyPropertyChanged("WindowWidth");
                }
            }
        }

        public PatternWindowVM(int width, int height, PatternType patternType, bool isRotated45)
        {
            SetDefaultValues();
            this.PatternType = patternType;
            this.WindowWidth = width.ToString();
            this.WindowHeight = height.ToString();
            this.IsRotated45 = isRotated45;
        }

        private void SetDefaultValues()
        {
            this.IsToolbarVisible = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
