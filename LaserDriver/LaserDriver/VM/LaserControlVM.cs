﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver.VM
{
    class LaserControlVM : INotifyPropertyChanged
    {
        private string tabItemHeight;
        public string TabItemHeight
        {
            get { return this.tabItemHeight; }
            set
            {
                if (this.tabItemHeight != value)
                {
                    this.tabItemHeight = value;
                    this.NotifyPropertyChanged("TabItemHeight");
                }
            }
        }

        private string tabItemWidth;
        public string TabItemWidth
        {
            get { return this.tabItemWidth; }
            set
            {
                if (this.tabItemWidth != value)
                {
                    this.tabItemWidth = value;
                    this.NotifyPropertyChanged("TabItemWidth");
                }
            }
        }

        private DeviceStatus cameraStatus;
        public DeviceStatus CamerasStatus
        {
            get
            {
                return cameraStatus;
            }
            set
            {
                this.cameraStatus = value;
                this.NotifyPropertyChanged("CamerasStatus");
            }
        }

        private DeviceStatus laserStatus;
        public DeviceStatus LaserStatus
        {
            get
            {
                return laserStatus;
            }
            set
            {
                this.laserStatus = value;
                this.NotifyPropertyChanged("LaserStatus");
            }
        }

        private DeviceStatus spectrometerStatus;
        public DeviceStatus SpectrometerStatus
        {
            get
            {
                return spectrometerStatus;
            }
            set
            {
                this.spectrometerStatus = value;
                this.NotifyPropertyChanged("SpectrometerStatus");
            }
        }

        private string notes;
        public string Notes
        {
            get { return this.notes; }
            set
            {
                if (this.notes != value)
                {
                    this.notes = value;
                    this.NotifyPropertyChanged("Notes");
                }
            }
        }

        public int? MeasurementsNo
        {
            get
            {
                int value;
                return int.TryParse(MeasurementsNoTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (int?)value : null;
            }
        }

        private string measurementsNoTxt;
        public string MeasurementsNoTxt
        {
            get { return this.measurementsNoTxt; }
            set
            {
                if (this.measurementsNoTxt != value)
                {
                    this.measurementsNoTxt = value;
                    this.NotifyPropertyChanged("MeasurementsNoTxt");
                }
            }
        }

        public double? ExposureTimeMs
        {
            get
            {
                double value;
                return double.TryParse(ExposureTimeMsTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string exposureTimeMsTxt;
        public string ExposureTimeMsTxt
        {
            get { return this.exposureTimeMsTxt; }
            set
            {
                if (this.exposureTimeMsTxt != value)
                {
                    this.exposureTimeMsTxt = value;
                    this.NotifyPropertyChanged("ExposureTimeMsTxt");
                    MinExposureTimeMsTxt = value;
                }
            }
        }

        //Variable exposure
        private bool isVariableExposure;
        public bool IsVariableExposure
        {
            get { return this.isVariableExposure; }
            set
            {
                if (this.isVariableExposure != value)
                {
                    this.isVariableExposure = value;
                    this.NotifyPropertyChanged("IsVariableExposure");
                }
            }
        }

        private bool useEnclosureLight;
        public bool UseEnclosureLight
        {
            get { return this.useEnclosureLight; }
            set
            {
                if (this.useEnclosureLight != value)
                {
                    this.useEnclosureLight = value;
                    this.NotifyPropertyChanged("UseEnclosureLight");
                }
            }
        }

        private byte enclosureLightIntensity;
        public byte EnclosureLightIntensity
        {
            get => enclosureLightIntensity;
            set
            {
                if(this.enclosureLightIntensity != value)
                {
                    this.enclosureLightIntensity = value;
                    this.NotifyPropertyChanged("EnclosureLightIntensity");
                }
            }
        }

        public double? MinExposureTimeMs
        {
            get
            {
                double value;
                return double.TryParse(MinExposureTimeMsTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string minExposureTimeMsTxt;
        public string MinExposureTimeMsTxt
        {
            get { return this.minExposureTimeMsTxt; }
            set
            {
                if (this.minExposureTimeMsTxt != value)
                {
                    this.minExposureTimeMsTxt = value;
                    this.NotifyPropertyChanged("MinExposureTimeMsTxt");
                }
            }
        }

        public double? MaxExposureTimeMs
        {
            get
            {
                double value;
                return double.TryParse(MaxExposureTimeMsTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string maxExposureTimeMsTxt;
        public string MaxExposureTimeMsTxt
        {
            get { return this.maxExposureTimeMsTxt; }
            set
            {
                if (this.maxExposureTimeMsTxt != value)
                {
                    this.maxExposureTimeMsTxt = value;
                    this.NotifyPropertyChanged("MaxExposureTimeMsTxt");
                }
            }
        }

        public double? IntegrationTimeMs
        {
            get
            {
                double value;
                return double.TryParse(IntegrationTimeMsTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string integrationTimeMsTxt;
        public string IntegrationTimeMsTxt
        {
            get { return this.integrationTimeMsTxt; }
            set
            {
                if (this.integrationTimeMsTxt != value)
                {
                    this.integrationTimeMsTxt = value;
                    this.NotifyPropertyChanged("IntegrationTimeMsTxt");
                }
            }
        }

        public double? Wavelength
        {
            get
            {
                double value;
                return double.TryParse(WavelengthTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?) value : null;
            }
        }

        private string wavelengthTxt;
        public string WavelengthTxt
        {
            get { return this.wavelengthTxt; }
            set
            {
                if (this.wavelengthTxt != value)
                {
                    this.wavelengthTxt = value;
                    this.NotifyPropertyChanged("WavelengthTxt");
                }
            }
        }
        
        public double? Amplitude
        {
            get
            {
                double value;
                return double.TryParse(AmplitudeTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string amplitudeTxt;
        public string AmplitudeTxt
        {
            get { return this.amplitudeTxt; }
            set
            {
                if (this.amplitudeTxt != value)
                {
                    this.amplitudeTxt = value;
                    this.NotifyPropertyChanged("AmplitudeTxt");
                }
            }
        }

        public double? LaserPower
        {
            get
            {
                double value;
                return double.TryParse(LaserPowerTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string laserPowerTxt;
        public string LaserPowerTxt
        {
            get { return this.laserPowerTxt; }
            set
            {
                if (this.laserPowerTxt != value)
                {
                    this.laserPowerTxt = value;
                    this.NotifyPropertyChanged("LaserPowerTxt");
                }
            }
        }

        private string startWavelengthTxt;
        public string StartWavelengthTxt
        {
            get { return this.startWavelengthTxt; }
            set
            {
                if (this.startWavelengthTxt != value)
                {
                    this.startWavelengthTxt = value;
                    this.NotifyPropertyChanged("StartWavelengthTxt");
                }
            }
        }

        public double? StartWavelength
        {
            get
            {
                double value;
                return double.TryParse(StartWavelengthTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string endWavelengthTxt;
        public string EndWavelengthTxt
        {
            get { return this.endWavelengthTxt; }
            set
            {
                if (this.endWavelengthTxt != value)
                {
                    this.endWavelengthTxt = value;
                    this.NotifyPropertyChanged("EndWavelengthTxt");
                }
            }
        }

        public double? EndWavelength
        {
            get
            {
                double value;
                return double.TryParse(EndWavelengthTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        public double? StepSize
        {
            get
            {
                double value;
                return double.TryParse(StepSizeTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string stepSizeTxt;
        public string StepSizeTxt
        {
            get { return this.stepSizeTxt; }
            set
            {
                if(this.stepSizeTxt != value)
                {
                    this.stepSizeTxt = value;
                    this.NotifyPropertyChanged("StepSizeTxt");
                }
            }
        }

        private bool isExposureSweep;
        public bool IsExposureSweep
        {
            get { return this.isExposureSweep; }
            set
        {
                if (this.isExposureSweep != value)
                {
                    this.isExposureSweep = value;
                    this.NotifyPropertyChanged("IsExposureSweep");
                }
            }
        }

        private bool isStepMode;
        public bool IsStepMode
        {
            get { return this.isStepMode; }
            set
            {
                if (this.isStepMode != value)
                {
                    this.isStepMode = value;
                    this.NotifyPropertyChanged("IsStepMode");
                }
            }
        }

        private bool isScanAndReconstruct;
        public bool IsScanAndReconstruct
        {
            get { return this.isScanAndReconstruct; }
            set
            {
                if(this.isScanAndReconstruct != value)
                {
                    this.isScanAndReconstruct = value;
                    this.NotifyPropertyChanged("IsScanAndReconstruct");
                }
            }
        }

        private bool isPatternActive;
        public bool IsPatternActive
        {
            get { return this.isPatternActive; }
            set
            {
                if (this.isPatternActive != value)
                {
                    this.isPatternActive = value;
                    this.NotifyPropertyChanged("IsPatternActive");
                }
            }
        }

        private bool isPatternWindowOpen;
        public bool IsPatternWindowOpen
        {
            get { return this.isPatternWindowOpen; }
            set
            {
                if (this.isPatternWindowOpen != value)
                {
                    this.isPatternWindowOpen = value;
                    this.NotifyPropertyChanged("IsPatternWindowOpen");
                }
            }
        }

        private bool isPatternRotated45;
        public bool IsPatternRotated45
        {
            get { return this.isPatternRotated45; }
            set
            {
                if (this.isPatternRotated45 != value)
                {
                    this.isPatternRotated45 = value;
                    this.NotifyPropertyChanged("IsPatternRotated45");
                }
            }
        }

        private bool isRfPowerOn;
        public bool IsRfPowerOn
        {
            get { return this.isRfPowerOn; }
            set
            {
                if (this.isRfPowerOn != value)
                {
                    this.isRfPowerOn = value;
                    this.NotifyPropertyChanged("IsRfPowerOn");
                }
            }
        }

        private bool isEmissionOn;
        public bool IsEmissionOn
        {
            get { return this.isEmissionOn; }
            set
            {
                if (this.isEmissionOn != value)
                {
                    this.isEmissionOn = value;
                    this.NotifyPropertyChanged("IsEmissionOn");
                }
            }
        }

        private bool useSpectrometer;
        public bool UseSpectrometer
        {
            get { return this.useSpectrometer; }
            set
            {
                if (this.useSpectrometer != value)
                {
                    this.useSpectrometer = value;
                    this.NotifyPropertyChanged("UseSpectrometer");
                }
            }
        }

        internal bool isUiEnabled;
        public bool IsUiEnabled
        {
            get { return this.isUiEnabled; }
            set
            {
                if (this.isUiEnabled != value)
                {
                    this.isUiEnabled = value;
                    this.NotifyPropertyChanged("IsUiEnabled");
                }
            }
        }

        public int? PatternWidth
        {
            get
            {
                int value;
                return int.TryParse(PatternWidthTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (int?)value : null;
            }
        }

        private string patternWidthTxt;
        public string PatternWidthTxt
        {
            get { return this.patternWidthTxt; }
            set
            {
                if (this.patternWidthTxt != value)
                {
                    this.patternWidthTxt = value;
                    this.NotifyPropertyChanged("PatternWidthTxt");
                }
            }
        }

        public int? PatternHeight
        {
            get
            {
                int value;
                return int.TryParse(PatternHeightTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (int?)value : null;
            }
        }

        private string patternHeightTxt;
        public string PatternHeightTxt
        {
            get { return this.patternHeightTxt; }
            set
            {
                if (this.patternHeightTxt != value)
                {
                    this.patternHeightTxt = value;
                    this.NotifyPropertyChanged("PatternHeightTxt");
                }
            }
        }

        public int? NumberOfImagesToAverage
        {
            get
            {
                int value;
                return int.TryParse(NumberOfImagesToAverageTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (int?)value : null;
            }
        }

        private string numberOfImagesToAverageTxt;
        public string NumberOfImagesToAverageTxt
        {
            get { return this.numberOfImagesToAverageTxt; }
            set
            {
                if (this.numberOfImagesToAverageTxt != value)
                {
                    this.numberOfImagesToAverageTxt = value;
                    this.NotifyPropertyChanged("NumberOfImagesToAverageTxt");
                }
            }
        }

        private string userDefinedPatternFolder;
        public string UserDefinedPatternFolder
        {
            get { return this.userDefinedPatternFolder; }
            set
            {
                if (this.userDefinedPatternFolder != value)
                {
                    this.userDefinedPatternFolder = value;
                    this.NotifyPropertyChanged("UserDefinedPatternFolder");
                }
            }
        }

        private bool useUserDefinedHomogenizationImage;
        public bool UseUserDefinedHomogenizationImage
        {
            get { return this.useUserDefinedHomogenizationImage; }
            set
            {
                if (this.useUserDefinedHomogenizationImage != value)
                {
                    this.useUserDefinedHomogenizationImage = value;
                    this.NotifyPropertyChanged("UseUserDefinedHomogenizationImage");
                }
            }
        }

        private string userDefinedHomogenizationImage;
        public string UserDefinedHomogenizationImage
        {
            get { return this.userDefinedHomogenizationImage; }
            set
            {
                if (this.userDefinedHomogenizationImage != value)
                {
                    this.userDefinedHomogenizationImage = value;
                    this.NotifyPropertyChanged("UserDefinedHomogenizationImage");
                }
            }
        }

        private string leftCameraCalibrationFolder;
        public string LeftCameraCalibrationFolder
        {
            get { return this.leftCameraCalibrationFolder; }
            set
            {
                if (this.leftCameraCalibrationFolder != value)
                {
                    this.leftCameraCalibrationFolder = value;
                    this.NotifyPropertyChanged("LeftCameraCalibrationFolder");
                }
            }
        }

        private string rightCameraCalibrationFolder;
        public string RightCameraCalibrationFolder
        {
            get { return this.rightCameraCalibrationFolder; }
            set
            {
                if (this.rightCameraCalibrationFolder != value)
                {
                    this.rightCameraCalibrationFolder = value;
                    this.NotifyPropertyChanged("RightCameraCalibrationFolder");
                }
            }
        }

        private bool canRunCameraCalibration;
        public bool CanRunCameraCalibration
        {
            get { return this.canRunCameraCalibration; }
            set
            {
                if (this.canRunCameraCalibration != value)
                {
                    this.canRunCameraCalibration = value;
                    this.NotifyPropertyChanged("CanRunCameraCalibration");
                }
            }
        }

        public double? SquareSizeMm
        {
            get
            {
                double value;
                return double.TryParse(SquareSizeMmTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (double?)value : null;
            }
        }

        private string squareSizeMmTxt;
        public string SquareSizeMmTxt
        {
            get { return this.squareSizeMmTxt; }
            set
            {
                if (this.squareSizeMmTxt != value)
                {
                    this.squareSizeMmTxt = value;
                    this.NotifyPropertyChanged("SquareSizeMmTxt");
                }
            }
        }

        public System.Drawing.Size ChessboardInnerGridsize
        {
            get
            {
                InputVerification.TryParseCheckerboard(ChessboardInnerGridsizeTxt, out System.Drawing.Size gridsize);
                return gridsize;
            }
        }

        private string chessboardInnerGridsizeTxt;
        public string ChessboardInnerGridsizeTxt
        {
            get { return this.chessboardInnerGridsizeTxt; }
            set
            {
                if (this.chessboardInnerGridsizeTxt != value)
                {
                    this.chessboardInnerGridsizeTxt = value;
                    this.NotifyPropertyChanged("ChessboardInnerGridsizeTxt");
                }
            }
        }

        private PatternGenerator.PatternType selectedPatternType;
        public PatternGenerator.PatternType SelectedPatternType
        {
            get => selectedPatternType;
            set
            {
                if (this.selectedPatternType != value)
                {
                    selectedPatternType = value;
                    IsPhaseShifting = selectedPatternType == PatternGenerator.PatternType.PhaseShifting;
                    this.NotifyPropertyChanged("SelectedPatternType");
                }
            }
        }

        #region Phase shifting

        private bool isPhaseShifting;
        public bool IsPhaseShifting
        {
            get => isPhaseShifting;
            set
            {
                if (this.isPhaseShifting != value)
                {
                    this.isPhaseShifting = value;
                    this.NotifyPropertyChanged("IsPhaseShifting");
                }
            }
        }

        public int PS_CueSteps
        {
            get
            {
                return int.Parse(PS_CueStepsTxt, NumberStyles.Any, CultureInfo.InvariantCulture);
            }
        }

        private string ps_CueStepsTxt;
        public string PS_CueStepsTxt
        {
            get { return this.ps_CueStepsTxt; }
            set
            {
                if (this.ps_CueStepsTxt != value)
                {
                    this.ps_CueStepsTxt = value;
                    this.NotifyPropertyChanged("PS_CueStepsTxt");
                }
            }
        }

        public int PS_PrimarySteps
        {
            get
            {
                return int.Parse(PS_PrimaryStepsTxt, NumberStyles.Any, CultureInfo.InvariantCulture);
            }
        }

        private string ps_PrimaryStepsTxt;
        public string PS_PrimaryStepsTxt
        {
            get { return this.ps_PrimaryStepsTxt; }
            set
            {
                if (this.ps_PrimaryStepsTxt != value)
                {
                    this.ps_PrimaryStepsTxt = value;
                    this.NotifyPropertyChanged("PS_PrimarySteps");
                }
            }
        }

        public int PS_NPeriods
        {
            get
            {
                return int.Parse(PS_NPeriodsTxt, NumberStyles.Any, CultureInfo.InvariantCulture);
            }
        }

        private string ps_NPeriodsTxt;
        public string PS_NPeriodsTxt
        {
            get { return this.ps_NPeriodsTxt; }
            set
            {
                if (this.ps_NPeriodsTxt != value)
                {
                    this.ps_NPeriodsTxt = value;
                    this.NotifyPropertyChanged("PS_NPeriodsTxt");
                }
            }
        }

        #endregion

        public LaserControlVM()
        {
            SetDefaultValues();
        }

        private void SetDefaultValues()
        {
            this.IsStepMode = true;
            this.IsScanAndReconstruct = false;
            this.StartWavelengthTxt = "505.0";
            this.EndWavelengthTxt = "650.0";
            this.StepSizeTxt = "60.0";
            this.ExposureTimeMsTxt = "850.0";
            this.WavelengthTxt = "630.0";
            this.LaserPowerTxt = "100.0";
            this.AmplitudeTxt = "100.0";
            this.IntegrationTimeMsTxt = "100.0";
            this.MeasurementsNoTxt = "1";
            this.IsUiEnabled = true;
            this.IsPatternActive = true;
            this.PatternHeightTxt = "1140";
            this.PatternWidthTxt = "896";
            this.IsPatternRotated45 = false;
            this.NumberOfImagesToAverageTxt = "1";
            this.IsVariableExposure = true;
            this.MinExposureTimeMsTxt = "1350";
            this.MaxExposureTimeMsTxt = "29999.99";
            this.UserDefinedHomogenizationImage = Properties.Settings.Default.HomogenizationImage;
            this.LeftCameraCalibrationFolder = ""; //C:\Users\grena\OneDrive\DTU\Masters Project\Test\Grayscale Cameras\2019-02-01 Camera calibration\L
            this.RightCameraCalibrationFolder = ""; //C:\Users\grena\OneDrive\DTU\Masters Project\Test\Grayscale Cameras\2019-02-01 Camera calibration\R
            this.UseUserDefinedHomogenizationImage = true;
            this.SquareSizeMmTxt = "4.0";
            this.PS_PrimaryStepsTxt = "8";
            this.PS_CueStepsTxt = "4";
            this.PS_NPeriodsTxt = "16";
            this.UseEnclosureLight = false;
            this.EnclosureLightIntensity = byte.MaxValue;
            this.ChessboardInnerGridsizeTxt = Utils.CheckerboardSizeToString(Properties.Settings.Default.ChessboardInnerGridsize);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
