﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver.Plots
{
    public class VariableExposurePlotVM : INotifyPropertyChanged
    {
        public VariableExposurePlotVM(List<DataPoint> factorPoints, List<DataPoint> exposurePoints)
        {
            this.Title = "Variable exposure profile";
            this.FactorPoints = factorPoints;
            this.ExposurePoints = exposurePoints;
            this.NumberOfImagesPerWavelengthTxt = "1";
        }

        public string Title { get; private set; }

        public IList<DataPoint> FactorPoints { get; private set; }

        private IList<DataPoint> exposurePoints;
        public IList<DataPoint> ExposurePoints {
            get { return this.exposurePoints; }
            set
            {
                this.exposurePoints = value;
                this.NotifyPropertyChanged("ExposurePoints");
                var maxExposure = exposurePoints.Max<DataPoint, double>(dp => dp.Y);
                ExposureYAxisMax = Math.Ceiling(maxExposure / 1000) * 1000; //Round to closes second
            }
        }

        private double exposureYAxisMax;
        public double ExposureYAxisMax
        {
            get { return this.exposureYAxisMax; }
            set
            {
                if (this.exposureYAxisMax != value)
                {
                    this.exposureYAxisMax = value;
                    this.NotifyPropertyChanged("ExposureYAxisMax");
                }
            }
        }

        private string totalExposureTimeTxt;
        public string TotalExposureTimeTxt
        {
            get { return this.totalExposureTimeTxt; }
            set
            {
                if (this.totalExposureTimeTxt != value)
                {
                    this.totalExposureTimeTxt = value;
                    this.NotifyPropertyChanged("TotalExposureTimeTxt");
                }
            }
        }

        private double minExposureTimeMs;
        public double MinExposureTimeMs
        {
            get { return this.minExposureTimeMs; }
            set
            {
                if (this.minExposureTimeMs != value)
                {
                    this.minExposureTimeMs = value;
                    this.NotifyPropertyChanged("MinExposureTimeMs");
                }
            }
        }

        private double maxExposureTimeMs;
        public double MaxExposureTimeMs
        {
            get { return this.maxExposureTimeMs; }
            set
            {
                if (this.maxExposureTimeMs != value)
                {
                    this.maxExposureTimeMs = value;
                    this.NotifyPropertyChanged("MaxExposureTimeMs");
                }
            }
        }

        public int? NumberOfImagesPerWavelength
        {
            get
            {
                int value;
                return int.TryParse(NumberOfImagesPerWavelengthTxt, NumberStyles.Any, CultureInfo.InvariantCulture, out value) ? (int?)value : null;
            }
        }

        private string numberOfImagesPerWavelengthTxt;
        public string NumberOfImagesPerWavelengthTxt
        {
            get { return this.numberOfImagesPerWavelengthTxt; }
            set
            {
                if (this.numberOfImagesPerWavelengthTxt != value)
                {
                    this.numberOfImagesPerWavelengthTxt = value;
                    this.NotifyPropertyChanged("NumberOfImagesPerWavelengthTxt");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
