﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LaserDriver.Plots
{
    /// <summary>
    /// Interaction logic for VariableExposurePlot.xaml
    /// </summary>
    public partial class VariableExposurePlot : Window
    {
        public static bool IsOpen { get; private set; }
        public double ExposureTimePerImageMs;

        public VariableExposurePlotVM ViewModel
        {
            get
            {
                return this.DataContext as VariableExposurePlotVM;
            }
        }

        public VariableExposurePlot(VariableExposurePlotVM viewModel, double exposureTimePerImageMs)
        {
            this.ExposureTimePerImageMs = exposureTimePerImageMs;
            InitializeComponent();
            this.DataContext = viewModel;
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            IsOpen = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IsOpen = true;
        }

        private void NumberOfImages_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                UpdateTotalExposureTime();
            }
        }

        private void NumberOfImages_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = InputVerification.IsPositiveInt(e.Text);
        }

        private void NumberOfImages_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateTotalExposureTime();
        }

        private void UpdateTotalExposureTime()
        {
            var totalExposureTime = ExposureTimePerImageMs * (int)ViewModel.NumberOfImagesPerWavelength;
            ViewModel.TotalExposureTimeTxt = Utils.TimeMsToTimeSpanString(totalExposureTime);
        }
    }
}
