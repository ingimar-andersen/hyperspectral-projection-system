﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver.Plots
{
    public class SpectrometerResultPlotVM
    {
        public SpectrometerResultPlotVM(List<DataPoint> wavelengthIntensities)
        {
            this.Title = "Spectrometer measurement result";
            this.WavelengthIntensities = wavelengthIntensities;

            var maxIntensity = wavelengthIntensities.Max<DataPoint, double>(dp => dp.Y);
            YAxisMax = Math.Ceiling((maxIntensity + 1) / 1000) * 1000; //Round to closest thousand
            var minWavelength = wavelengthIntensities.Min<DataPoint, double>(dp => dp.X);
            var maxWavelength = wavelengthIntensities.Max<DataPoint, double>(dp => dp.X);
            XAxisMax = Math.Floor((maxWavelength - 1) / 10) * 10; //Round to closest ten
            XAxisMax = Math.Ceiling((maxWavelength + 1) / 10) * 10; //Round to closest ten
        }

        public string Title { get; private set; }

        public double XAxisMin { get; private set; }
        public double XAxisMax { get; private set; }
        public double YAxisMax { get; private set; }

        public IList<DataPoint> WavelengthIntensities { get; private set; }
    }
}
