﻿using Emgu.CV;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver
{
    public class Matlab : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private MLApp.MLApp mlApp;
        public Matlab(string executionPath = "")
        {
            mlApp = new MLApp.MLApp();
            if (string.IsNullOrWhiteSpace(executionPath))
            {
                SetDefaultExecutionPath();
            }
            else
            {
                SetExecutionPath(executionPath);
            }
        }

        private static class Functions
        {
            internal static readonly string HomogenizeAuto = "getHomoImage_checkerboard";
            internal static readonly string Homogenize = "getHomoImage";
            internal static readonly string SaveArrayAsMat = "saveArrayAsMat";
            internal static readonly string StereoCalibrate = "stereoCameraCalibration";
            internal static readonly string CalcVariableExposure = "calcVariableExposure";
            internal static readonly string PhaseShiftingReconstruction = "phaseShifting_v3";
            internal static readonly string SaveHspsAsPly = "save_hsps_as_ply";
            internal static readonly string AddFolderAndSubfolders = "addFolderAndSubfolders";
            internal static readonly string MergePointClouds = "pc_merge";
        }

        public HomogenizationAutoResult HomogenizeAuto(string leftImagePath, string leftCheckerboardPath, string rightImagePath, string rightCheckerboardPath, string checkerboardPatternPath, string calibrationParamsPath, double lossPercentage, double maxPercentile, bool showFigures, double[,] transformationL = null, double[,] transformationR = null)
        {
            //function [homoImage, transformL_out, transformR_out] = getHomoImage_checkerboard(leftImPath,leftCheckerPath,rightImPath,rightCheckerPath,checkerboardPatternPath,bitDepth,paramPath,lossPercentage, showFigures, maxPercentile, transformL, transformR)
            object result;
            if (transformationL == null || transformationR == null)
            {
                mlApp.Feval(Functions.HomogenizeAuto, 3, out result, leftImagePath, leftCheckerboardPath, rightImagePath, rightCheckerboardPath, checkerboardPatternPath, calibrationParamsPath, lossPercentage, showFigures, maxPercentile);
            }
            else
            {
                mlApp.Feval(Functions.HomogenizeAuto, 3, out result, leftImagePath, leftCheckerboardPath, rightImagePath, rightCheckerboardPath, checkerboardPatternPath, calibrationParamsPath, lossPercentage, showFigures, maxPercentile, transformationL, transformationR);
            }
            var resultArray = result as object[];
            var homoImageData = resultArray[0] as double[,];
            var transL = resultArray[1] as double[,];
            var transR = resultArray[2] as double[,];
            return new HomogenizationAutoResult(homoImageData, transL, transR);
        }

        public HomogenizationResult Homogenize(string leftImagePath, string rightImagePath, string calibrationParamsPath, double lossPercentage, bool autoModeEnabled, int patternWidth, int patternHeight, double[,] boundingBoxCoordinates = null)
        {
            object result;
            if (boundingBoxCoordinates == null)
            {
                mlApp.Feval(Functions.Homogenize, 2, out result, leftImagePath, rightImagePath, calibrationParamsPath, lossPercentage, autoModeEnabled, patternWidth, patternHeight);
            }
            else
            {
                mlApp.Feval(Functions.Homogenize, 2, out result, leftImagePath, rightImagePath, calibrationParamsPath, lossPercentage, autoModeEnabled, patternWidth, patternHeight, boundingBoxCoordinates);
            }
            var resultArray = result as object[];
            var homoImageData = resultArray[0] as double[,];
            var newBoundingBoxCoordinates = resultArray[1] as double[,];
            return new HomogenizationResult(homoImageData, newBoundingBoxCoordinates);
        }

        public double[,] CalcVariableExposure(string inputFolder)
        {
            mlApp.Feval(Functions.CalcVariableExposure, 1, out object result, inputFolder);
            var resultArray = result as object[];
            return resultArray[0] as double[,];
        }

        public void SaveArrayAsMat(double[,] array, string outputFilePath)
        {
            mlApp.Feval(Functions.SaveArrayAsMat, 0, out object resultObjectSave, array, outputFilePath);
        }

        public StereoCalibrationResult StereoCalibrate(string[] leftImagesFilepaths, string[] rightImagesFilepaths, double squareSizeMm, string stereoParamsOutputPath)
        {
            object resultObject = null;

            //function [cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, R, T] = stereoCameraCalibration(imageFileNamesL, imageFileNamesR, squareSizeMm,cameraParamsLeft, cameraParamsRight)
            mlApp.Feval(Functions.StereoCalibrate, 6, out resultObject, leftImagesFilepaths, rightImagesFilepaths, squareSizeMm, stereoParamsOutputPath);

            // Display result 
            var result = resultObject as object[];
            var camMatrix1 = result[0] as double[,];
            var distCoeffs1 = result[1] as double[,];
            var camMatrix2 = result[2] as double[,];
            var distCoeffs2 = result[3] as double[,];
            var r = result[4] as double[,];
            var t = result[5] as double[,];
            return new StereoCalibrationResult(camMatrix1, camMatrix2, distCoeffs1, distCoeffs2, r, t);
        }

        public void PhaseShiftingReconstruction(string leftImgsPath, string rightImgsPath, string outputFilename, int periods, int primary_steps, int cue_steps, bool plotEnabled = false)
        {
            object resultObject = null;
            //function phaseShifting_v3(left_path, right_path, output_filename)
            mlApp.Feval(Functions.PhaseShiftingReconstruction, 0, out resultObject, leftImgsPath + "\\", rightImgsPath + "\\", Properties.Settings.Default.StereoCalibParamsFolder, outputFilename, (double)periods, (double)primary_steps, (double)cue_steps, plotEnabled);
        }

        public string SaveHspsAsPly(string hspsFilepath, bool plotEnabled = false)
        {
            object resultObject = null;
            //function phaseShifting_v3(left_path, right_path, output_filename)
            mlApp.Feval(Functions.SaveHspsAsPly, 1, out resultObject, hspsFilepath, plotEnabled);
            var result = resultObject as object[];
            return result[0] as string;
        }

        public void MergePointclouds(string[] filePaths, double[,] colors, string outputPath, bool denoise)
        {
            object resultObject = null;
            //pc_merge(paths, colors, output_path, denoise)
            mlApp.Feval(Functions.MergePointClouds, 0, out resultObject, filePaths, colors, outputPath, denoise);
        }

        private void SetDefaultExecutionPath()
        {
            SetExecutionPath(Properties.Settings.Default.MatlabScriptsFolder);
        }

        private void SetExecutionPath(string exePath, bool includeSubfolders = true)
        {
            // Change to the directory where the function is located 
            var exeResult = mlApp.Execute($"cd '{exePath}'");

            //Execution error
            if (!string.IsNullOrWhiteSpace(exeResult))
            {
                throw new Exception($"Error executing matlab script: {exeResult}");
            }
            else if(includeSubfolders)
            {
                var includeSubfoldersScriptPath = Path.Combine(exePath, Functions.AddFolderAndSubfolders + ".m");
                if (File.Exists(includeSubfoldersScriptPath))
                {
                    try{
                        var result = new object();
                        mlApp.Feval(Functions.AddFolderAndSubfolders, 0, out result, exePath);
                    }
                    catch(Exception ex)
                    {
                        log.Error("Error adding folders and subfolders.", ex);
                    }
                }
                else
                {
                    log.Error($"Could not find '{Functions.AddFolderAndSubfolders}' script");
                }
                
            }
        }

        public void Quit()
        {
            mlApp.Quit();
        }

        public void Dispose()
        {
            mlApp.Quit();
        }
    }

    public class HomogenizationResult
    {
        public double[,] HomogenizationImageData;
        public double[,] BoundingBoxCoordinates;

        public HomogenizationResult(double[,] homogenizationImageData, double[,] boundingBoxCoordinates)
        {
            this.HomogenizationImageData = homogenizationImageData;
            this.BoundingBoxCoordinates = boundingBoxCoordinates;
        }
    }

    public class HomogenizationAutoResult
    {
        public double[,] HomogenizationImageData;
        public double[,] TransformationL;
        public double[,] TransformationR;

        public HomogenizationAutoResult(double[,] homogenizationImageData, double[,] transformationL, double[,] transformationR)
        {
            this.HomogenizationImageData = homogenizationImageData;
            this.TransformationL = transformationL;
            this.TransformationR = transformationR;
        }
    }

    public class StereoCalibrationResult
    {
        public Matrix<double> CameraMatrixLeft;
        public Matrix<double> CameraMatrixRight;
        public Matrix<double> DistCoeffsLeft;
        public Matrix<double> DistCoeffsRight;
        public Matrix<double> R;
        public Matrix<double> T;

        public StereoCalibrationResult(double[,] cameraMatrixLeft, double[,] cameraMatrixRight, double[,] distCoeffsLeft, double[,] distCoeffsRight, double[,] r, double[,] t)
        {
            //TODO: check that dimensions match
            this.CameraMatrixLeft = new Matrix<double>(cameraMatrixLeft);
            this.CameraMatrixRight = new Matrix<double>(cameraMatrixRight);
            this.DistCoeffsLeft = new Matrix<double>(distCoeffsLeft);
            this.DistCoeffsRight= new Matrix<double>(distCoeffsRight);
            this.R = new Matrix<double>(r);
            this.T = new Matrix<double>(t);
        }

    }
}
