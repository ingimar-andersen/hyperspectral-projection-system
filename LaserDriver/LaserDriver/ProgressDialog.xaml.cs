﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Shell;
using System.Windows.Threading;

namespace LaserDriver
{
    /// <summary>
    /// Interaction logic for ProgressDialog.xaml
    /// </summary>
    public partial class ProgressDialog : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ProgressDialogContext ViewModel
        {
            get { return this.DataContext as ProgressDialogContext; }
        }

        public bool IsClosed { get; private set; }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            IsClosed = true;
        }

        public ProgressDialog()
        {
            InitializeComponent();
            this.DataContext = new ProgressDialogContext();
            CenterWindowOnScreen();
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
        }

        public class ProgressDialogContext : INotifyPropertyChanged
        {
            private string message;
            public string Message
            {
                get { return message; }
                set
                {
                    if (value != message)
                    {
                        message = value;
                        NotifyPropertyChanged("Message");
                    }
                }
            }

            private string progressTxt;
            public string ProgressTxt
            {
                get { return progressTxt; }
                set
                {
                    if (value != progressTxt)
                    {
                        progressTxt = value;
                        NotifyPropertyChanged("ProgressTxt");
                    }
                }
            }

            private TaskbarItemProgressState progressState;
            public TaskbarItemProgressState ProgressState
            {
                get { return progressState; }
                set
                {
                    if (value != progressState)
                    {
                        progressState = value;
                        NotifyPropertyChanged("ProgressState");
                    }
                }
            }

            private bool doNotifyViaEmail;
            public bool DoNotifyViaEmail
            {
                get { return doNotifyViaEmail; }
                set
                {
                    if (value != doNotifyViaEmail)
                    {
                        doNotifyViaEmail = value;
                        NotifyPropertyChanged("DoNotifyViaEmail");
                    }
                }
            }

            private string notifyEmail;
            public string NotifyEmail
            {
                get { return notifyEmail; }
                set
                {
                    if(value != notifyEmail)
                    {
                        notifyEmail = value;
                        NotifyPropertyChanged("NotifyEmail");
                    }
                }
            }

            private double progress;
            public double Progress
            {
                get { return progress; }
                set
                {
                    if(value >= 0 && value <= 1 && value != progress)
                    {
                        progress = value;
                        var progressPercentage = progress * 100;
                        ProgressTxt = progressPercentage.ToString("0.#", System.Globalization.CultureInfo.InvariantCulture) + " %";
                        calculateRemainingTime = true;
                        NotifyPropertyChanged("Progress");
                    }
                }
            }

            public string TimeElapsed { get; set; }
            public string TimeRemaining { get; set; }

            private DispatcherTimer timer;
            private Stopwatch stopWatch;
            int timerIntervalMs = 1000;
            public void StartTimer()
            {
                timer = new DispatcherTimer();
                timer.Tick += dispatcherTimerTick_;
                timer.Interval = new TimeSpan(0, 0, 0, 0, timerIntervalMs);
                stopWatch = new Stopwatch();
                stopWatch.Start();
                timer.Start();
            }

            bool calculateRemainingTime = true;
            double previousRemainingMs;
            private void dispatcherTimerTick_(object sender, EventArgs e)
            {
                try
                {
                    TimeElapsed = stopWatch.Elapsed.ToString("hh':'mm':'ss"); // Format as you wish
                    if(progress > 0) {
                        double remainingMs;
                        if(calculateRemainingTime)
                        {
                            remainingMs = stopWatch.ElapsedMilliseconds / Progress - stopWatch.ElapsedMilliseconds;
                            calculateRemainingTime = false;
                        }
                        else
                        {
                            remainingMs = Math.Max(previousRemainingMs - timerIntervalMs, 0);
                        }
                        previousRemainingMs = remainingMs;
                        TimeRemaining = TimeSpan.FromMilliseconds(remainingMs).ToString("hh':'mm':'ss");
                    }
                    else
                    {
                        TimeRemaining = "-";
                    }

                    PropertyChanged(this, new PropertyChangedEventArgs("TimeElapsed"));
                    PropertyChanged(this, new PropertyChangedEventArgs("TimeRemaining"));
                }
                catch (Exception ex)
                {
                    log.Error($"Error on timer tick. Stopping timer and stopwatch.", ex);
                    timer.Stop();
                    stopWatch.Stop();
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;
            public void NotifyPropertyChanged(string propName)
            {
                this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
            }

            internal void OnExit()
            {
                if(timer != null)
                {
                    timer.Stop();
                }
                if(stopWatch != null)
                {
                    stopWatch.Stop();
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.StartTimer();
            ViewModel.ProgressState = TaskbarItemProgressState.Normal;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            ViewModel.OnExit();
        }
    }
}
