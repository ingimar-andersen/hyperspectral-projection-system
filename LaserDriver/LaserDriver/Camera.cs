﻿//=============================================================================
// Copyright © 2018 FLIR Integrated Imaging Solutions, Inc. All Rights Reserved.
//
// This software is the confidential and proprietary information of FLIR
// Integrated Imaging Solutions, Inc. ("Confidential Information"). You
// shall not disclose such Confidential Information and shall use it only in
// accordance with the terms of the license agreement you entered into
// with FLIR Integrated Imaging Solutions, Inc. (FLIR).
//
// FLIR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
// SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE, OR NON-INFRINGEMENT. FLIR SHALL NOT BE LIABLE FOR ANY DAMAGES
// SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
// THIS SOFTWARE OR ITS DERIVATIVES.
//=============================================================================*/
/**
 *  @example Acquisition_CSharp.cs
 *
 *  @brief Acquisition_CSharp.cs demonstrates the acquisition of images. It 
 *  relies on information provided in the Enumeration_CSharp example. Following 
 *  this, check out the ExceptionHandling_CSharp and NodeMapInfo_CSharp 
 *  examples if you haven't already. ExceptionHandling_CSharp shows how to 
 *  handle standard and Spinnaker exceptions while NodeMapInfo_CSharp explores 
 *  retrieving information from various node types.  
 *
 *  This example touches on the preparation and cleanup of a camera just before
 *  and just after the acquisition of images. Image retrieval and conversion,
 *  grabbing image data, and saving images are all covered.
 *
 *  Once comfortable with Acquisition_CSharp, ExceptionHandling_CSharp, and 
 *  NodeMapInfo_CSharp, we suggest checking out AcquisitionMultipleCamera_CSharp, 
 *  NodeMapCallback_CSharp, or SaveToAvi_CSharp. AcquisitionMultipleCamera_CSharp
 *  demonstrates simultaneously acquiring images from a number of cameras, 
 *  NodeMapCallback_CSharp acts as a good introduction to programming with 
 *  callbacks and events, and SaveToAvi_CSharp exhibits video creation.
 */

using System;
using System.IO;
using System.Collections.Generic;
using SpinnakerNET;
using SpinnakerNET.GenApi;
using System.Windows.Forms;
using SpinnakerNET.GUI;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;
using HW;
using log4net;

namespace LaserDriver
{
    public class Camera : IDisposable
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public enum CameraTriggerType
        {
            Software = 0,
            Hardware = 1,
            Off = 2,
        }

        public List<ManagedCamera> CamList;
        private ManagedSystem system;

        // Serial number is used to identify Left and Right cameras

        //public const string LEFT_CAMERA_SERIAL_NO = "14194507"; //Grasshopper 3
        //public const string RIGHT_CAMERA_SERIAL_NO = "14253982"; //Grasshopper 3
        public const string LEFT_CAMERA_SERIAL_NO = "16375866"; //Blackfly
        public const string RIGHT_CAMERA_SERIAL_NO = "16375873"; //Blackfly

        public static readonly double CAMERA_MIN_EXPOSURE_TIME_MS = 0.4; //Blackfly
        public static readonly double CAMERA_MAX_EXPOSURE_TIME_MS = 29999.999; //Blackfly

        private ImageDrawingWindow leftCameraWindow;
        private ImageDrawingWindow rightCameraWindow;

        public Camera()
        {
            Initialize();
        }

        private object camlistLock = new object();
        public bool TryResetCamlist()
        {
            lock (camlistLock)
            {
                try
                {
                    var inActiveIds = CamList.Where(x => !x.UseForAcquisition).Select(x => x.DeviceId);
                    CamList = system.GetCameras().Select(x => new ManagedCamera(x)).ToList();
                    foreach (var cam in CamList)
                    {
                        if (inActiveIds.Contains(cam.DeviceId))
                        {
                            cam.UseForAcquisition = false;
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error($"An error occured while getting camera list.", e);
                    return false;
                }

                return true;
            }
        }

        public void Initialize()
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();
                // Retrieve singleton reference to system object
                system = new ManagedSystem();
                log.Debug($"Camera initialization (ManagedSystem): {sw.ElapsedMilliseconds} [ms]");
                sw.Restart();
                // Print out current library version
                LibraryVersion spinVersion = system.GetLibraryVersion();
                log.Info($"Spinnaker library version: {spinVersion.major}.{spinVersion.minor}.{spinVersion.type}.{spinVersion.build}\n\n");
                log.Info($"Camera initialization (GetLibraryVersion): {sw.ElapsedMilliseconds} [ms]");
                sw.Restart();

                // Retrieve list of cameras from the system
                CamList = system.GetCameras().Select(x => new ManagedCamera(x)).ToList();
                log.Debug($"Camera initialization (GetCameras): {sw.ElapsedMilliseconds} [ms]");
                sw.Restart();

                log.Debug($"Number of cameras detected: {CamList.Count}\n\n");

                // Finish if there are no cameras
                if (CamList.Count == 0)
                {
                    log.Error("No cameras!");
                }

                foreach (var cam in CamList)
                {
                    INodeMap nodeMapTLDevice = cam.Camera.GetTLDeviceNodeMap();
                    SetDefaultStreamBufferHandlingMode(cam.Camera);
                    PrintDeviceInfo(nodeMapTLDevice);
                    log.Debug($"RunSingleCamera (PrintDeviceInfo): {sw.ElapsedMilliseconds} [ms]");
                    sw.Restart();
                    if (!TrySetDefaultSettings(cam.NodeMap))
                    {
                        log.Error("Could not set default settings for camera");
                    }
                    log.Debug($"RunSingleCamera (TrySetDefaultSettings): {sw.ElapsedMilliseconds} [ms]");

                    sw.Restart();
                }
            }
            catch (Exception ex)
            {
                log.Fatal($"Error camera initialization", ex);
            }
        }

        private bool TryConfigureTrigger(INodeMap nodeMap, CameraTriggerType trigger)
        {
            log.Info("*** CONFIGURING TRIGGER ***");

            var iTriggerSelector = nodeMap.GetNode<IEnum>("TriggerSelector");
            if (iTriggerSelector != null && iTriggerSelector.IsWritable)
            {
                var triggerSelectorEntry = iTriggerSelector.GetEntryByName(TriggerSelectorEnums.FrameStart.ToString());
                if (triggerSelectorEntry != null && triggerSelectorEntry.IsReadable) {
                    iTriggerSelector.Value = triggerSelectorEntry.Value;
                }
            }

            log.Info($"{trigger.ToString()} trigger chosen...");
            try
            {
                //
                // Ensure trigger mode off
                //
                // *** NOTES ***
                // The trigger must be disabled in order to configure whether the source
                // is software or hardware.
                //
                var iTriggerMode = nodeMap.GetNode<IEnum>("TriggerMode");
                if (!iTriggerMode.IsAvailable || !iTriggerMode.IsReadable)
                {
                   log.Error("Unable to disable trigger mode (node retrieval). Aborting...");
                    return false;
                }

                var iTriggerModeOff = iTriggerMode.GetEntryByName("Off");
                if (!iTriggerModeOff.IsAvailable || !iTriggerModeOff.IsReadable)
                {
                    log.Error("Unable to disable trigger mode (enum entry retrieval). Aborting...");
                    return false;
                }
                iTriggerMode.Value = iTriggerModeOff.Value;

                log.Info("Trigger mode disabled...");
                //
                // Select trigger source
                //
                // *** NOTES ***
                // The trigger source must be set to hardware or software while trigger 
                // mode is off.
                //

                if(trigger == CameraTriggerType.Off)
                {
                    return true;
                }

                var iTriggerSource = nodeMap.GetNode<IEnum>("TriggerSource");
                if (!iTriggerSource.IsAvailable || !iTriggerSource.IsWritable)
                {
                    log.Error("Unable to set trigger mode (node retrieval). Aborting...");
                    return false;
                }
                if (trigger == CameraTriggerType.Software)
                {
                    // Set trigger mode to software
                    var iTriggerSourceSoftware = iTriggerSource.GetEntryByName("Software");
                    if (!iTriggerSourceSoftware.IsAvailable || !iTriggerSourceSoftware.IsReadable)
                    {
                        log.Error("Unable to set trigger mode (enum entry retrieval). Aborting...");
                        return false;
                    }
                    iTriggerSource.Value = iTriggerSourceSoftware.Value;

                    log.Info("Trigger source set to software...");
                }
                else if (trigger == CameraTriggerType.Hardware)
                {
                    var iTriggerSourceHardware = iTriggerSource.GetEntryByName(HardwareSettings.TRIGGER_SOURCE);
                    if (!iTriggerSourceHardware.IsAvailable|| !iTriggerSourceHardware.IsReadable)
                    {
                        log.Error("Unable to set trigger mode (enum entry retrieval). Aborting...");
                        return false;
                    }
                    iTriggerSource.Value = iTriggerSourceHardware.Value;

                    log.Info($"Trigger source set to hardware ({HardwareSettings.TRIGGER_SOURCE})...");
                }
                //
                // Turn trigger mode on
                //
                // *** LATER ***
                // Once the appropriate trigger source has been set, turn trigger mode 
                // on in order to retrieve images using the trigger.
                //
                var ptrTriggerModeOn = iTriggerMode.GetEntryByName("On");
                if (!ptrTriggerModeOn.IsAvailable|| !ptrTriggerModeOn.IsReadable)
                {
                    log.Error("Unable to enable trigger mode (enum entry retrieval). Aborting...");
                    return false;
                }

                iTriggerMode.Value = ptrTriggerModeOn.Value;

                log.Info("Trigger mode turned back on...");
            }
            catch (Exception ex)
            {
                log.Fatal($"Error setting trigger mode", ex);
                return false;
            }
            return true;
        }

        private void SetDefaultStreamBufferHandlingMode(IManagedCamera cam)
        {
            var nodeMapTLStream = cam.GetTLStreamNodeMap();
            var iStreamBufferHandlingMode = nodeMapTLStream.GetNode<IEnum>("StreamBufferHandlingMode");
            if (iStreamBufferHandlingMode == null || !iStreamBufferHandlingMode.IsWritable)
            {
                log.Error("Unable to set iStreamBufferHandlingMode (enum retrieval). Aborting...");
            }
            else
            {
                IEnumEntry iStreamBufferHandlingModeEntry = iStreamBufferHandlingMode.GetEntryByName(StreamBufferHandlingModeEnum.NewestOnly.ToString());
                if (iStreamBufferHandlingModeEntry == null || !iStreamBufferHandlingModeEntry.IsReadable)
                {
                    log.Error("Unable to read NewestFirst enum entry (entry retrieval). Aborting...");
                }
                else
                {
                    iStreamBufferHandlingMode.Value = iStreamBufferHandlingModeEntry.Value;
                }
            }
        }

        private double exposureTimeMs;
        public void SetExposureTimeForCameras(double exposureTimeMs)
        {

            foreach (var cam in CamList.Where(x => x.UseForAcquisition))
            {
                if (!TrySetExposureTime(cam.NodeMap, exposureTimeMs))
                {
                   throw new Exception("Could not set exposure time for camera");
                }
                this.exposureTimeMs = exposureTimeMs;
            }
        }

        public Task<List<ManagedImage>> AcquireSingleImageAllCameras(CameraTriggerType trigger)
        {
            return Task.Run(() =>
            {
                var sw = new Stopwatch();
                sw.Restart();
                var result = new List<ManagedImage>();
                var activeCameras = CamList.Where(x => x.UseForAcquisition);

                if (trigger != CameraTriggerType.Off)
                {
                    foreach (var cam in activeCameras)
                    {
                        GrabImageByTrigger(trigger, cam.NodeMap);
                    }
                }
                //Minimum 5000ms timeout
                var timeoutMs = (ulong)Math.Max(Math.Round(exposureTimeMs * 2), 5000);
                foreach (var managedCamera in activeCameras)
                {
                    result.Add(RunSingleCamera(managedCamera, timeoutMs));
                }
                log.Debug($"AcquireSingleImage (All cameras): {sw.ElapsedMilliseconds} [ms]");
                sw.Restart();
                return result;
            });
        }
        
#if DEBUG
        // Disables heartbeat on GEV cameras so debugging does not incur timeout errors
        static int DisableHeartbeat(IManagedCamera cam, INodeMap nodeMap, INodeMap nodeMapTLDevice)
        {
            log.Info("Checking device type to see if we need to disable the camera's heartbeat...\n\n");

            //
            // Write to boolean node controlling the camera's heartbeat
            // 
            // *** NOTES ***
            // This applies only to GEV cameras and only applies when in DEBUG mode.
            // GEV cameras have a heartbeat built in, but when debugging applications the
            // camera may time out due to its heartbeat. Disabling the heartbeat prevents 
            // this timeout from occurring, enabling us to continue with any necessary debugging.
            // This procedure does not affect other types of cameras and will prematurely exit
            // if it determines the device in question is not a GEV camera. 
            //
            // *** LATER ***
            // Since we only disable the heartbeat on GEV cameras during debug mode, it is better
            // to power cycle the camera after debugging. A power cycle will reset the camera 
            // to its default settings. 
            // 
            IEnum iDeviceType = nodeMapTLDevice.GetNode<IEnum>("DeviceType");
            IEnumEntry iDeviceTypeGEV = iDeviceType.GetEntryByName("GEV");
            // We first need to confirm that we're working with a GEV camera 
            if (iDeviceType != null && iDeviceType.IsReadable)
            {
                if (iDeviceType.Value == iDeviceTypeGEV.Value)
                {
                    log.Info("Working with a GigE camera. Attempting to disable heartbeat before continuing...\n\n");
                    IBool iGEVHeartbeatDisable = nodeMap.GetNode<IBool>("GevGVCPHeartbeatDisable");
                    if (iGEVHeartbeatDisable == null || !iGEVHeartbeatDisable.IsWritable)
                    {
                        log.Warn("Unable to disable heartbeat on camera. Continuing with execution as this may be non-fatal...");
                    }
                    else
                    {
                        iGEVHeartbeatDisable.Value = true;
                        log.Warn("WARNING: Heartbeat on GigE camera disabled for the rest of Debug Mode. \nPower cycle camera when done debugging to re-enable the heartbeat...");
                    }
                }
                else
                {
                    log.Info("Camera does not use GigE interface. Resuming normal execution...\n\n");
                }
            }
            else
            {
                log.Error("Unable to access TL device nodemap. Aborting...");
                return -1;
            }

            return 0;
        }

#endif
        static ManagedImage AcquireImage(ManagedCamera cam, INodeMap nodeMapTLDevice, ulong timeoutMs)
        {
            var sw = new Stopwatch();
            sw.Start();
            log.Info("\n*** IMAGE ACQUISITION ***\n");
            try
            {
#if DEBUG
                log.Debug("\n\n*** DEBUG ***\n\n");
                // If using a GEV camera and debugging, should disable heartbeat first to prevent further issues

                if (DisableHeartbeat(cam.Camera, cam.NodeMap, nodeMapTLDevice) != 0)
                {
                    return null;
                }

                log.Debug("\n\n*** END OF DEBUG ***\n\n");
#endif
                log.Info($"AcquireImages (DisableHeartbeat): {sw.ElapsedMilliseconds} [ms]");
                sw.Restart();
            }
            catch (SpinnakerException ex)
            {
                log.Fatal("Error disabling heartbeat.", ex);
                return null;
            }

            try
            {
                //
                // Begin acquiring images
                //
                // *** NOTES ***
                // What happens when the camera begins acquiring images depends 
                // on which acquisition mode has been set. Single frame captures 
                // only a single image, multi frame catures a set number of 
                // images, and continuous captures a continuous stream of images.
                // Because the example calls for the retrieval of 10 images, 
                // continuous mode has been set for the example.
                // 
                // *** LATER ***
                // Image acquisition must be ended when no more images are needed.
                //
                //cam.Camera.BeginAcquisition();
                log.Info($"AcquireImages (BeginAcquisition): {sw.ElapsedMilliseconds} [ms]");
                sw.Restart();

                log.Info("Acquiring images...");

                // Retrieve, convert, and save images

                //
                // Retrieve next received image
                //
                // *** NOTES ***
                // Capturing an image houses images on the camera buffer. 
                // Trying to capture an image that does not exist will 
                // hang the camera.
                //
                // Using-statements help ensure that images are released.
                // If too many images remain unreleased, the buffer will
                // fill, causing the camera to hang. Images can also be
                // released manually by calling Release().
                // 
                //using (IManagedImage rawImage = cam.Camera.GetNextImage())
                var rawImage = cam.Camera.GetNextImage(timeoutMs);
                //rawImage.Release();
                log.Info($"AcquireImages (GetNextImage): {sw.ElapsedMilliseconds} [ms]");
                sw.Restart();
                //
                // Ensure image completion
                //
                // *** NOTES ***
                // Images can easily be checked for completion. This 
                // should be done whenever a complete image is 
                // expected or required. Alternatively, check image
                // status for a little more insight into what 
                // happened.
                //
                if (rawImage.IsIncomplete)
                {
                    log.Info($"Image incomplete with image status {rawImage.ImageStatus}...");
                    return null;
                }
                else
                {
                    uint width = rawImage.Width;

                    uint height = rawImage.Height;

                    log.Info($"Grabbed image. width = {width}, height = {height}");

                    return new ManagedImage(rawImage, cameraId: cam.DeviceId, cameraSerial: cam.SerialNumber);
                }
            }
            catch (SpinnakerException ex)
            {
                log.Fatal("Error acquiring image.", ex);
                return null;
            }
        }

        internal void ClearBufferAll(double exposureTimeMs)
        {
            foreach (var cam in CamList.Where(x => x.UseForAcquisition))
            {
                try
                {
                    var timeOut = (ulong)Math.Round(Math.Max(2000, 1.5 * exposureTimeMs));
                    var image = cam.Camera.GetNextImage(timeOut);
                    while (image != null)
                    {
                        image.Dispose();
                        image = cam.Camera.GetNextImage(timeOut);
                    }
                }
                catch (Exception ex)
                {
                    if(ex is SpinnakerException && (ex as SpinnakerException).ErrorCode == Error.SPINNAKER_ERR_TIMEOUT)
                    {
                        continue;
                    }
                    log.Error($"Error clearing buffer for camera '{cam.DeviceId}': {ex.Message}");
                }
            }
        }

        internal void SetTriggerAll(CameraTriggerType trigger)
        {
            foreach(var cam in CamList.Where(x => x.UseForAcquisition))
            {
                TryConfigureTrigger(cam.NodeMap, trigger);
            }
        }

        private async void GrabImageByTrigger(CameraTriggerType trigger, INodeMap nodeMap)
        {
            try
            {
                // 
                // Use trigger to capture image
                //
                // *** NOTES ***
                // The software trigger only feigns being executed by the Enter key;
                // what might not be immediately apparent is that there is not a
                // continuous stream of images being captured; in other examples that 
                // acquire images, the camera captures a continuous stream of images. 
                // When an image is retrieved, it is plucked from the stream.
                //
                if (trigger == CameraTriggerType.Software)
                {
                    // Execute software trigger
                    var iSoftwareTriggerCommand = nodeMap.GetNode<ICommand>("TriggerSoftware");
                    if (!iSoftwareTriggerCommand.IsAvailable || !iSoftwareTriggerCommand.IsWritable)
                    {
                        throw new Exception("Unable to execute software trigger.");
                    }
                    iSoftwareTriggerCommand.Execute();
                }
                else if (trigger == CameraTriggerType.Hardware)
                {
                    var maxRetry = 10;
                    var retryCount = 0;
                    while (retryCount < maxRetry) {
                        if(await Arduino.Instance.TryTriggerCameras(HardwareSettings.TRIGGER_PIN_ARDUINO))
                        {
                            break; //Successful trigger
                        }
                        await Utils.WaitAsync(waitTimeMs: 50);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Error grabbing image using trigger.", ex);
            }
        }

        internal void BeginAquisitionAll()
        {
            foreach (var cam in CamList)
            {
                cam.DisableLed();
                if (cam.UseForAcquisition)
                {
                    try
                    {
                        if(!cam.Camera.IsStreaming()) //Will throw an exception if acquisition is begun while its streaming
                        {
                            cam.Camera.BeginAcquisition();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Could not begin camera aquistiion for camera '{cam.DeviceId}'", ex);
                    }
                }
            }
        }

        internal void EndAquisitionAll()
        {
            foreach (var cam in CamList)
            {
                cam.ActivateLed();
                try
                {
                    cam.Camera.EndAcquisition();
                }
                catch (Exception ex)
                {
                    log.Error($"Could not end camera aquistiion for camera '{cam.DeviceId}': {ex.Message}");
                }
            }
        }

        // This function prints the device information of the camera from the 
        // transport layer; please see NodeMapInfo_CSharp example for more 
        // in-depth comments on printing device information from the nodemap.
        static int PrintDeviceInfo(INodeMap nodeMap)
        {
            int result = 0;

            try
            {
                log.Info("\n*** DEVICE INFORMATION ***\n");

                ICategory category = nodeMap.GetNode<ICategory>("DeviceInformation");
                if (category != null && category.IsReadable)
                {
                    for (int i = 0; i < category.Children.Length; i++)
                    {
                        log.Info($"{category.Children[i].Name}: {(category.Children[i].IsReadable ? category.Children[i].ToString() : "Node not available")}");
                    }
                }
                else
                {
                    log.Warn("Device control information not available.");
                }
            }
            catch (SpinnakerException ex)
            {
                log.Fatal("Error getting device info.", ex);
                result = -1;
            }

            return result;
        }


        // This function acts as the body of the example; please see 
        // NodeMapInfo_CSharp example for more in-depth comments on setting up 
        // cameras.
        public static ManagedImage RunSingleCamera(ManagedCamera cam, ulong timeoutMs)
        {
            try
            {
                // Retrieve TL device nodemap and print device information
                INodeMap nodeMapTLDevice = cam.Camera.GetTLDeviceNodeMap();

                // Acquire images
                return AcquireImage(cam, nodeMapTLDevice, timeoutMs);
            }
            catch (SpinnakerException ex)
            {
                log.Fatal("Error running single camera", ex);
                return null;
            }
        }

        private static bool TrySetExposureTime(INodeMap nodeMap, double exposureTimeMs)
        {
            try
            {
                // Exposure Auto
                var exposureAuto = nodeMap.GetNode<IEnum>("ExposureAuto");
                if (exposureAuto == null || !exposureAuto.IsWritable)
                {
                    log.Error("Unable to set ExposureAuto (enum retrieval). Aborting...\n");
                    return false;
                }
                else
                {
                    IEnumEntry exposureAutoEntry = exposureAuto.GetEntryByName("Off");
                    if (exposureAutoEntry == null || !exposureAutoEntry.IsReadable)
                    {
                        log.Error("Unable to read ExposureAuto enum entry (entry retrieval). Aborting...\n");
                        return false;
                    }
                    else
                    {
                        exposureAuto.Value = exposureAutoEntry.Value;
                    }
                }

                //Exposure time
                var exposureTime = nodeMap.GetNode<IFloat>("ExposureTime");
                if (exposureTime == null || !exposureTime.IsWritable)
                {
                    log.Error("Unable to set ExposureTime. Aborting...\n");
                    return false;
                }
                exposureTime.Value = exposureTimeMs * 1000; // [us]
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show($"Error setting exposure time: {ex.Message}");
                log.Error("Error setting exposure time.", ex);
                return false;
            }
        }

        private static bool TrySetDefaultSettings(INodeMap nodeMap) //Todo retun 
        {
            TrySetPixelFormat(nodeMap, PixelFormatEnums.Mono16);
            TrySetGainAuto(nodeMap, GainAutoEnums.Off);
            TrySetGain(nodeMap, 0);
            TrySetAcquisitionFrameRateEnabled(nodeMap, false);
            TrySetGammaEnabled(nodeMap, false);
            TrySetSharpnessEnabled(nodeMap, true);
            TryTurnOffExposureCompensation(nodeMap);

            //Bypass the color core
            //OnBoardColorProcessing
            //var onBoardColorProcessing = nodeMap.GetNode<IBool>("OnBoardColorProcessing");
            //if (onBoardColorProcessing == null || !onBoardColorProcessing.IsWritable)
            //{
            //    log.Error("Unable to set onBoardColorProcessing. Aborting...\n");
            //    return false;
            //}
            //onBoardColorProcessing.Value = false;

            TrySetBlackLevelEnabled(nodeMap, false);

            TrySetBlackLevel(nodeMap, blackLevelPercentage: 0.0);

            TrySetHueEnabled(nodeMap, false);

            TrySetAcquisitionMode(nodeMap, AcquisitionModeEnums.Continuous);
            TrySetWhiteBalance(nodeMap, BalanceWhiteAutoEnums.Off, blueRatio: 1, redRatio: 1);
            TrySetChunkMode(nodeMap);
            TrySetSharpening(nodeMap, sharpeningEnabled: false);

            return true;
        }

        private static bool TrySetAcquisitionFrameRateEnabled(INodeMap nodeMap, bool enabled)
        {
            var result = true;
            //Framrate enabled (for Grasshopper)
            var framerateEnabled = nodeMap.GetNode<IBool>("AcquisitionFrameRateEnabled");
            if (framerateEnabled == null || !framerateEnabled.IsWritable)
            {
                result = false;
            }
            if(result == false)
            {
                //For Blackfly cameras
                framerateEnabled = nodeMap.GetNode<IBool>("AcquisitionFrameRateEnable");
                if (framerateEnabled == null || !framerateEnabled.IsWritable)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            if (result == true)
            {
                framerateEnabled.Value = enabled;
            }
            else
            {
                log.Error("Unable to set AcquisitionFrameRateEnabled. Aborting...\n");
            }
            return result;
        }

        private static bool TrySetGammaEnabled(INodeMap nodeMap, bool enabled)
        {
            var result = true;
            //GammaEnable (for Grasshopper)
            var gammaEnabled = nodeMap.GetNode<IBool>("GammaEnabled");
            if (gammaEnabled == null || !gammaEnabled.IsWritable)
            {
                result = false;
            }
            if (result == false)
            {
                //For Blackfly cameras
                gammaEnabled = nodeMap.GetNode<IBool>("GammaEnable");
                if (gammaEnabled == null || !gammaEnabled.IsWritable)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            if (result == true)
            {
                gammaEnabled.Value = enabled;
            }
            else
            {
                log.Error("Unable to set GammaEnabled. Aborting...\n");
            }
            return result;
        }

        private static bool TryTurnOffExposureCompensation(INodeMap nodeMap)
        {
            //pgrExposureCompensationAuto
            IEnum pgrExposureCompensationAuto = nodeMap.GetNode<IEnum>("pgrExposureCompensationAuto");
            if (pgrExposureCompensationAuto == null || !pgrExposureCompensationAuto.IsWritable)
            {
                log.Error("Unable to set pgrExposureCompensationAuto to Off (node retrieval). Aborting...");
                return false;
            }

            // Retrieve entry node from enumeration node
            IEnumEntry pgrExposureCompensationAutoEntry = pgrExposureCompensationAuto.GetEntryByName("Off");
            if (pgrExposureCompensationAutoEntry == null || !pgrExposureCompensationAutoEntry.IsReadable)
            {
                log.Error("Unable to set pgrExposureCompensationAutoEntry to Off (enum entry retrieval). Aborting...");
                return false;
            }
            pgrExposureCompensationAuto.Value = pgrExposureCompensationAutoEntry.Value;
            return true;
        }

        private static bool TrySetHueEnabled(INodeMap nodeMap, bool enabled)
        {
            //HueEnabled
            var hueEnabled = nodeMap.GetNode<IBool>("HueEnabled");
            if (hueEnabled == null || !hueEnabled.IsWritable)
            {
                log.Warn("Unable to set hueEnabled. Aborting...");
                return false;
            }
            hueEnabled.Value = enabled;
            return true;
        }

        private static bool TrySetBlackLevelEnabled(INodeMap nodeMap, bool enabled)
        {
            //BlacLevelEnabled
            var blackLevelEnabled = nodeMap.GetNode<IBool>("BlackLevelEnabled");
            if (blackLevelEnabled == null || !blackLevelEnabled.IsWritable)
            {
                log.Warn("Unable to set BlackLevelEnabled. Aborting...");
                return false;
            }
            blackLevelEnabled.Value = enabled;
            return true;
        }

        private static bool TrySetSharpnessEnabled(INodeMap nodeMap, bool enabled)
        {
            //SharpnessEnabled
            var sharpnessEnabled = nodeMap.GetNode<IBool>("SharpnessEnabled");
            if (sharpnessEnabled == null || !sharpnessEnabled.IsWritable)
            {
                log.Warn("Unable to set SharpnessEnabled. Aborting...");
                return false;
            }
            sharpnessEnabled.Value = enabled;
            return true;
        }

        private static bool TrySetBlackLevel(INodeMap nodeMap, double blackLevelPercentage)
        {

            var blackLevelSelector = nodeMap.GetNode<IEnum>("BlackLevelSelector");
            if (blackLevelSelector == null || !blackLevelSelector.IsWritable)
            {
                log.Warn("Unable to find node BlackLevelSelector");
                return false;
            }
            else
            {
                IEnumEntry blackLevelSelectorEntry = blackLevelSelector.GetEntryByName(BlackLevelSelectorEnums.All.ToString());
                if (blackLevelSelectorEntry == null || !blackLevelSelectorEntry.IsReadable)
                {
                    log.Warn("Unable to set pgrExposureCompensationAutoEntry to Off (enum entry retrieval). Aborting...");
                    return false;
                }
                blackLevelSelector.Value = blackLevelSelectorEntry.Value;

                var blackLevel = nodeMap.GetNode<IFloat>("BlackLevel");
                if (blackLevel == null || !blackLevel.IsWritable)
                {
                    log.Warn("Unable to set BlackLevelEnabled. Aborting...");
                    return false;
                }
                blackLevel.Value = blackLevelPercentage;
            }
            return true;
        }

        private static bool TrySetSharpening(INodeMap nodeMap, bool sharpeningEnabled, bool? sharpeningAutoEnabled = null, double? sharpening = null, double? sharpeningThreshold = null)
        {
            var isSharpeningEnabled = nodeMap.GetNode<IBool>("SharpeningEnabled");
            if (isSharpeningEnabled == null || !isSharpeningEnabled.IsWritable)
            {
                log.Warn("Unable to set SharpeningEnabled (node retrieval). Aborting...");
                return false;
            }
            else
            {
                isSharpeningEnabled.Value = sharpeningEnabled;
            }

            if (sharpeningAutoEnabled != null)
            {
                var isharpeningAuto = nodeMap.GetNode<IBool>("SharpeningAuto");
                if (isharpeningAuto == null || !isharpeningAuto.IsWritable)
                {
                    log.Warn("Unable to set SharpeningAuto (node retrieval). Aborting...");
                    return false;
                }
                else
                {
                    isharpeningAuto.Value = (bool)sharpeningAutoEnabled;
                }
            }

            if (sharpening != null)
            {
                var iSharpening = nodeMap.GetNode<IFloat>("Sharpening");
                if (iSharpening == null || !iSharpening.IsWritable)
                {
                    log.Warn("Unable to set Sharpening (node retrieval). Aborting...\n");
                    return false;
                }
                //Range of sharpening. See technical reference for BlackFly camera
                if (sharpening <= 8 && sharpening >= -1)
                {
                    iSharpening.Value = (double)sharpening;
                }
                else
                {
                    log.Warn("Sharpening value outside allowed range (-1 ... 8). Aborting...\n");
                    return false;
                }
            }

            if (sharpeningThreshold != null)
            {
                var iSharpeningThreshold = nodeMap.GetNode<IFloat>("SharpeningThreshold");
                if (iSharpeningThreshold == null || !iSharpeningThreshold.IsWritable)
                {
                    log.Warn("Unable to set SharpeningThreshold (node retrieval). Aborting...\n");
                    return false;
                }
                //Range of sharpening threshold. See technical reference for BlackFly camera
                if (sharpeningThreshold >= 0 && sharpeningThreshold <= 0.25)
                {
                    iSharpeningThreshold.Value = (double)sharpeningThreshold;
                }
                else
                {
                    log.Error("Sharpening threshold value outside allowed range (0 - 0.25). Aborting...\n");
                    return false;
                }
            }

            return true;
        }

        private static bool TrySetAcquisitionMode(INodeMap nodeMap, AcquisitionModeEnums acquisitionMode)
        {

            //
            // Set acquisition mode to continuous
            //
            // *** NOTES ***
            // Because the example acquires and saves 10 images, setting 
            // acquisition mode to continuous lets the example finish. If 
            // set to single frame or multiframe (at a lower number of 
            // images), the example would just hang. This is because the 
            // example has been written to acquire 10 images while the 
            // camera would have been programmed to retrieve less than that.
            // 
            // Setting the value of an enumeration node is slightly more 
            // complicated than other node types. Two nodes are required: 
            // first, the enumeration node is retrieved from the nodemap and 
            // second, the entry node is retrieved from the enumeration node. 
            // The symbolic of the entry node is then set as the new value 
            // of the enumeration node.
            //
            // Notice that both the enumeration and entry nodes are checked 
            // for availability and readability/writability. Enumeration 
            // nodes are generally readable and writable whereas entry 
            // nodes are only ever readable.
            // 
            // Retrieve enumeration node from nodemap
            IEnum iAcquisitionMode = nodeMap.GetNode<IEnum>("AcquisitionMode");
            if (iAcquisitionMode == null || !iAcquisitionMode.IsWritable)
            {
                log.Error("Unable to set acquisition mode to continuous (node retrieval). Aborting...\n");
                return false;
            }

            // Retrieve entry node from enumeration node
            IEnumEntry iAcquisitionModeEntry = iAcquisitionMode.GetEntryByName(acquisitionMode.ToString());
            if (iAcquisitionModeEntry == null || !iAcquisitionModeEntry.IsReadable)
            {
                log.Error("Unable to set acquisition mode to SingleFrame (enum entry retrieval). Aborting...\n");
                return false;
            }

            // Set symbolic from entry node as new value for enumeration node
            iAcquisitionMode.Value = iAcquisitionModeEntry.Symbolic;
            log.Info($"Acquisition mode set to {acquisitionMode}...");
            return true;
        }

        private static bool TrySetGain(INodeMap nodeMap, double gainValue)
        {
            var gain = nodeMap.GetNode<IFloat>("Gain");
            if (gain == null || !gain.IsWritable)
            {
                log.Error("Unable to set Gain. Aborting...\n");
                return false;
            }
            gain.Value = gainValue;
            return true;
        }

        private static bool TrySetGainAuto(INodeMap nodeMap, GainAutoEnums value)
        {
            // Gain Auto
            var gainAuto = nodeMap.GetNode<IEnum>("GainAuto");
            if (gainAuto == null || !gainAuto.IsWritable)
            {
                log.Error("Unable to set GainAuto (enum retrieval). Aborting...\n");
                return false;
            }
            else
            {
                IEnumEntry gainAutoEntry = gainAuto.GetEntryByName(value.ToString());
                if (gainAutoEntry == null || !gainAutoEntry.IsReadable)
                {
                    log.Error("Unable to read GainAuto enum entry (entry retrieval). Aborting...\n");
                    return false;
                }
                else
                {
                    gainAuto.Value = gainAutoEntry.Value;
                }
            }
            return true;
        }

        private static bool TrySetPixelFormat(INodeMap nodeMap, PixelFormatEnums pixelFormat)
        {
            //Color
            var iPixelFormat = nodeMap.GetNode<IEnum>("PixelFormat");
            if (iPixelFormat == null || !iPixelFormat.IsWritable)
            {
                log.Error("Unable to set PixelFormat (enum retrieval). Aborting...\n");
                return false;
            }
            else
            {
                IEnumEntry iPixelFormatEntry = iPixelFormat.GetEntryByName(pixelFormat.ToString());
                if (iPixelFormatEntry == null || !iPixelFormatEntry.IsReadable)
                {
                    log.Error("Unable to read PixelFormat enum entry (entry retrieval). Aborting...\n");
                    return false;
                }
                else
                {
                    iPixelFormat.Value = iPixelFormatEntry.Value;
                }
            }
            return true;
        }

        private static bool TrySetChunkMode(INodeMap nodeMap)
        {
            //Chunk mode (image metadata)
            IBool iChunkModeActive = nodeMap.GetNode<IBool>("ChunkModeActive");
            if (iChunkModeActive == null || !iChunkModeActive.IsWritable)
            {
                log.Error("Cannot activate chunk mode. Aborting...");
                return false;
            }
            iChunkModeActive.Value = true;
            log.Info("Chunk mode activated...");

            IEnum iChunkSelector = nodeMap.GetNode<IEnum>("ChunkSelector");
            if (iChunkSelector == null || !iChunkSelector.IsReadable)
            {
                log.Error("Chunk selector not available. Aborting...");
                return false;
            }

            // Retrieve entries
            EnumEntry[] entries = iChunkSelector.Entries;

            log.Info("Enabling entries...");

            for (int i = 0; i < entries.Length; i++)
            {
                // Select entry to be enabled
                IEnumEntry iChunkSelectorEntry = entries[i];

                // Go to next node if problem occurs
                if (!iChunkSelectorEntry.IsAvailable || !iChunkSelectorEntry.IsReadable)
                {
                    continue;
                }

                iChunkSelector.Value = iChunkSelectorEntry.Value;

                log.Info($"\t{iChunkSelectorEntry.Symbolic}: ");

                // Retrieve corresponding boolean
                IBool iChunkEnable = nodeMap.GetNode<IBool>("ChunkEnable");

                // Enable the boolean, thus enabling the corresponding chunk
                // data
                if (iChunkEnable == null)
                {
                    log.Warn("\t\tnot available");
                    return false;
                }
                else if (iChunkEnable.Value)
                {
                    log.Info("\t\tenabled");
                }
                else if (iChunkEnable.IsWritable)
                {
                    iChunkEnable.Value = true;
                    log.Info("\t\tenabled");
                }
                else
                {
                    log.Warn("\t\tnot writable");
                    return false;
                }
            }
            return true;
        }

        private static bool TrySetWhiteBalance(INodeMap nodeMap, BalanceWhiteAutoEnums whiteBalance, double? blueRatio = null, double? redRatio = null)
        {
            // White balance auto off and ratio = 1
            IEnum iBalanceWhiteAuto = nodeMap.GetNode<IEnum>("BalanceWhiteAuto");
            if (iBalanceWhiteAuto == null || !iBalanceWhiteAuto.IsWritable)
            {
                log.Warn("Unable to set BalanceWhiteAuto (node retrieval). Aborting...\n");
                return false;
            }
            else
            {
                // Retrieve entry node from enumeration node
                IEnumEntry iBalanceWhiteAutoEntry = iBalanceWhiteAuto.GetEntryByName(whiteBalance.ToString());
                if (iBalanceWhiteAutoEntry == null || !iBalanceWhiteAutoEntry.IsReadable)
                {
                    log.Error("Unable to set BalanceWhiteAuto to Off (enum entry retrieval). Aborting...");
                    return false;
                }
                iBalanceWhiteAuto.Value = iBalanceWhiteAutoEntry.Value;

                IEnum balanceRatioSelector = nodeMap.GetNode<IEnum>("BalanceRatioSelector");
                if (balanceRatioSelector == null || !balanceRatioSelector.IsWritable)
                {
                    log.Error("Unable to set BalanceRatioSelector (node retrieval). Aborting...");
                    return false;
                }
                else
                {
                    if (redRatio != null)
                    {
                        //Red ratio
                        IEnumEntry balanceRatioSelectorEntry = balanceRatioSelector.GetEntryByName("Red");
                        if (balanceRatioSelectorEntry == null || !balanceRatioSelectorEntry.IsReadable)
                        {
                            log.Error("Unable to set BalanceRatioSelector to Red (enum entry retrieval). Aborting...");
                            return false;
                        }
                        balanceRatioSelector.Value = balanceRatioSelectorEntry.Value;

                        var balanceRatio = nodeMap.GetNode<IFloat>("BalanceRatio");
                        if (balanceRatio == null || !balanceRatio.IsWritable)
                        {
                            log.Error("Unable to set BalanceRatio. Aborting...");
                            return false;
                        }
                        balanceRatio.Value = (double)redRatio;
                    }

                    if (blueRatio != null)
                    {
                        //Blue ratio
                        var balanceRatioSelectorEntry = balanceRatioSelector.GetEntryByName("Blue");
                        if (balanceRatioSelectorEntry == null || !balanceRatioSelectorEntry.IsReadable)
                        {
                            log.Error("Unable to set BalanceRatioSelector to Red (enum entry retrieval). Aborting...");
                            return false;
                        }
                        balanceRatioSelector.Value = balanceRatioSelectorEntry.Value;

                        var balanceRatio = nodeMap.GetNode<IFloat>("BalanceRatio");
                        if (balanceRatio == null || !balanceRatio.IsWritable)
                        {
                            log.Error("Unable to set BalanceRatio. Aborting...");
                            return false;
                        }
                        balanceRatio.Value = (double)blueRatio;
                    }
                }
            }
            return true;
        }

        public bool TryGetCameraTemperatures(INodeMap nodeMap, ref double deviceTemperature)
        {
            IFloat sensorTemperature = nodeMap.GetNode<IFloat>("DeviceTemperature");
            deviceTemperature = sensorTemperature.Value;
            return true;
        }

        public async void OpenLeftCameraGui()
        {
            var leftCamera = CamList.SingleOrDefault(x => x.SerialNumber == LEFT_CAMERA_SERIAL_NO);
            if(leftCamera == null)
            {
                var msg = "Left camera not connected";
                MessageBox.Show(msg);
                log.Info(msg);
                return;
            }
            leftCameraWindow = await OpenCameraGui(leftCamera, leftCameraWindow, CameraWindow_Closing);
        }

        private void CameraWindow_Closing(object sender, CancelEventArgs e)
        {
            (sender as ImageDrawingWindow).Disconnect();
            (sender as ImageDrawingWindow).Stop();
        }

        public async void OpenRightCameraGui()
        {
            var rightCamera = CamList.SingleOrDefault(x => x.SerialNumber == RIGHT_CAMERA_SERIAL_NO);
            if (rightCamera == null)
            {
                MessageBox.Show("Right camera not connected");
                return;
            }

            rightCameraWindow = await OpenCameraGui(rightCamera, rightCameraWindow, CameraWindow_Closing);
        }

        private async Task<ImageDrawingWindow> OpenCameraGui(ManagedCamera camera, ImageDrawingWindow cameraWindow, CancelEventHandler closingHandler)
        {
            try
            {
                if (cameraWindow != null)
                {
                    if (cameraWindow.IsStreaming() && cameraWindow.IsConnected())
                    {
                        //Retry on failure
                        var retryCount = 5;
                        for (int i = 0; i < retryCount; i++)
                        {
                            if (cameraWindow.Focus())
                            {
                                break;
                            }
                            await Utils.WaitAsync(10);
                        }
                        return cameraWindow;
                    }
                    cameraWindow.Close();
                }

                var cameraGui = new GUIFactory();
                camera.Camera.Init();
                cameraGui.ConnectGUILibrary(camera.Camera);
                cameraWindow = cameraGui.GetImageDrawingWindow();
                cameraWindow.Closing += closingHandler;
                cameraWindow.SizeChanged += CameraWindow_SizeChanged;
                cameraWindow.SetTitle(camera.DeviceId);
                cameraWindow.Connect(camera.Camera);
                cameraWindow.Start();
                cameraWindow.Show();
                return cameraWindow;
            }
            catch (Exception ex)
            {
                log.Error($"Error opening Camera GUI.", ex);
                MessageBox.Show($"Error opening Camera GUI. See log for details.");
                return null;
            }
        }

        private async void CameraWindow_SizeChanged(object sender, System.Windows.RoutedEventArgs e)
        {
            //Wait for Spinnaker to expand the window with the camera feed and then resize it to the desired size
            var cameraWindow = (sender as ImageDrawingWindow);
            if (cameraWindow.IsStreaming() && cameraWindow.RenderSize.Width > 800)
            {
                //Unsubscribe as this should only happen on initialization
                cameraWindow.SizeChanged -= CameraWindow_SizeChanged;
                cameraWindow.RenderSize = new System.Windows.Size(800, 640);
                await Task.Run(() => Thread.Sleep(1)); //Wait for the resizing to kick in
                cameraWindow.SizeToContent = System.Windows.SizeToContent.Manual;
                cameraWindow.InvalidateVisual();
            }
        }

        public void Dispose()
        {
            log.Info("Camera container disposal started.");
            CloseCameraWindows();

            foreach (var cam in CamList)
            {
                try
                {
                    cam.Camera.DeInit();
                }
                catch(Exception ex)
                {
                    log.Error($"Could not DeInit camera: {ex.Message}");
                }
            }
            // Clear camera list before releasing system
            CamList.Clear();

            // Release system
            system.Dispose();
        }

        public void CloseCameraWindows()
        {
            if (leftCameraWindow != null)
            {
                leftCameraWindow.Close();
            }
            if (rightCameraWindow != null)
            {
                rightCameraWindow.Close();
            }
        }
    }
}