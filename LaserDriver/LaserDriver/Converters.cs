﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace LaserDriver.Converters
{
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                if ((bool)value)
                {
                    return Visibility.Visible;
                }
                return Visibility.Collapsed;
            }
            else
            {
                throw new NotSupportedException("Value should be bool");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility)
            {
                if ((Visibility)value == Visibility.Visible)
                {
                    return true;
                }
                return false;
            }
            else
            {
                throw new NotSupportedException("Value should be of type Visibility");
            }

        }
    }

    public class BooleanToVisibilityInverseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                if (!(bool)value)
                {
                    return Visibility.Visible;
                }
                return Visibility.Collapsed;
            }
            else
            {
                throw new NotSupportedException("Value should be bool");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility)
            {
                if ((Visibility)value != Visibility.Visible)
                {
                    return true;
                }
                return false;
            }
            else
            {
                throw new NotSupportedException("Value should be of type Visibility");
            }

        }
    }

    public class InverseBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                return !(bool)value;
            }
            else
            {
                throw new NotSupportedException("Value should be bool");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                return !(bool)value;
            }
            else
            {
                throw new NotSupportedException("Value should be bool");
            }
        }
    }

    public class DeviceStatusToBrush : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is DeviceStatus)
            {
                var status = (DeviceStatus)value;
                if (status.IsConnected && status.IsReady)
                {
                    return new SolidColorBrush(Color.FromRgb(86, 130, 3)); //Avocado green
                }
                else if(status.IsConnected && !status.IsReady){
                    return new SolidColorBrush(Color.FromRgb(255, 211, 0)); //Yellow (NCS)
                }
                else
                {
                    return new SolidColorBrush(Color.FromRgb(206, 32, 41)); //Fire engine red
                }
            }
            if(value is null)
            {
                return new SolidColorBrush(Color.FromRgb(206, 32, 41)); //Fire engine red
            }
            else
            {
                throw new NotSupportedException("Value should be DeviceStatus");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class DeviceStatusToMessage : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DeviceStatus)
            {
                var status = (DeviceStatus)value;
                return status.Message;
            }
            if(value is null)
            {
                return "";
            }
            else
            {
                throw new NotSupportedException("Value should be DeviceStatus");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class PatternTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is PatternGenerator.PatternType)
            {
                if (Enum.TryParse(parameter as string, out PatternGenerator.PatternType comparisonValue))
                {
                    var currentValue = (PatternGenerator.PatternType)value;
                    if(currentValue == comparisonValue)
                    {
                        return Visibility.Visible;
                    }
                    return Visibility.Collapsed;
                }
                else
                {
                    throw new NotSupportedException("Converter parameter should be a parsable string for type PatternType");
                }
            }
            else
            {
                throw new NotSupportedException("Value should be of type PatternType");
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
