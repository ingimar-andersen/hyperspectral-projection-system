﻿using Emgu.CV;
using Emgu.CV.Structure;
using LaserDriver.Extensions;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaserDriver
{
    public class PhaseShiftingParameters
    {
        public int NPeriods;
        public int PrimarySteps;
        public int CueSteps;
        public int PhaseMultiplesCount;

        public PhaseShiftingParameters(int nPeriods, int primarySteps, int cueSteps, int phaseMultiplesCount)
        {
            this.NPeriods = nPeriods;
            this.PrimarySteps = primarySteps;
            this.CueSteps = cueSteps;
            this.PhaseMultiplesCount = phaseMultiplesCount; //Not implemented correctly...yet
        }
    }

    public static class PatternGenerator
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public enum PatternType
        {
            GrayCodeHorizontal = 0,
            GrayCodeVertical = 1,
            LoadFromFolder = 2,
            PhaseShifting = 3,
            WhiteOutput = 4,
        }

        public static List<Image<Gray, float>> GenerateGrayEncodingPattern(int width, int height, PatternType patternType, bool isRotated45, int minThickness, bool hasHomogenizationImage)
        {
            if (width <= 0)
            {
                throw new Exception("Width must be a positive integer");
            }
            if (height <= 0)
            {
                throw new Exception("Height must be a positive integer");
            }
            if (minThickness < 1)
            {
                throw new Exception("Min width must be a positive integer");
            }

            var pattern = new List<Image<Gray, float>>();
            //First frame is white
            var whiteImage = new Image<Gray, float>(width, height, new Gray(byte.MaxValue));
            pattern.Add(whiteImage);

            var interval = patternType == PatternType.GrayCodeHorizontal ? width : height;
            while (interval >= minThickness && interval % 2 == 0)
            {
                var isWhite = false;
                //Initialize image as black
                var image = new Image<Gray, float>(width, height, new Gray(0));

                // Horizontal
                if (patternType == PatternType.GrayCodeHorizontal)
                {
                    for (int x = interval / 2; x < width; x++)
                    {
                        // Toggle color after each interval
                        if ((x + interval / 2) % interval == 0)
                        {
                            isWhite = !isWhite;
                        }
                        for (int y = 0; y < height; y++)
                        {
                            image.Data[y, x, 0] = isWhite ? byte.MaxValue : 0;
                        }
                    }
                }
                // Vertical
                else if (patternType == PatternType.GrayCodeVertical)
                {
                    for (int y = interval / 2; y < height; y++)
                    {
                        // Toggle color after each interval
                        if ((y + interval / 2) % interval == 0)
                        {
                            isWhite = !isWhite;
                        }
                        for (int x = 0; x < width; x++)
                        {
                            image.Data[y, x, 0] = isWhite ? byte.MaxValue : 0;
                        }
                    }
                }
                else
                {
                    throw new Exception("Gray encoding not supported for pattern type");
                }

                if (isRotated45)
                {
                    var tempImage = image.Copy();
                    var newImage = new Image<Gray, float>(width, height, new Gray(byte.MaxValue));
                    var xStartIndex = 0;
                    for (int y = 0; y < height; y++)
                    {
                        xStartIndex = y % 2 != 0 ? xStartIndex + 1 : xStartIndex;
                        for (int x = 0; x < width; x++)
                        {
                            var xIndex = xStartIndex + x;
                            if (xIndex >= width)
                            {
                                xIndex -= width;
                            }
                            newImage.Data[y, xIndex, 0] = tempImage.Data[y, x, 0];
                        }
                    }
                    pattern.Add(newImage);
                }
                else
                {
                    pattern.Add(image);
                }
                
                interval /= 2;
            }
            //Add inverse pattern
            var inversePattern = new List<Image<Gray, float>>();
            var combinedPattern = new List<Image<Gray, float>>();
            foreach (var frame in pattern)
            {
                var inverse = frame.Clone();
                inverse *= -1;
                inverse += byte.MaxValue;
                inversePattern.Add(inverse);

                combinedPattern.Add(frame);
                combinedPattern.Add(inverse);
            }
            return combinedPattern;
        }

        public static bool TryExportPattern<TColor, TDepth>(IEnumerable<Image<TColor, TDepth>> Pattern, string outputFolderPath)
        where TColor : struct, IColor
        where TDepth : new()
        {
            try
            {
                if (!Directory.Exists(outputFolderPath))
                {
                    Directory.CreateDirectory(outputFolderPath);
                }
                var number = 0;
                foreach (var frame in Pattern)
                {
                    var filename = Path.Combine(outputFolderPath, $"P{number.ToString().PadLeft(3, '0')}.png");
                    frame.Save(filename);
                    number++;
                }
            }
            catch (Exception ex)
            {
                log.Error($"Error while exporting pattern.", ex);
                return false;
            }
            return true;
        }

        private static readonly string[] allowedImageExtensions = { ".jpg", ".tiff", ".png", ".bmp", ".jpeg", ".tif", ".pbm", ".pgm", ".ppm" };
        internal static List<Image<Gray, float>> PatternFromFolder(string userDefinedPatternFolder)
        {
            if (!Directory.Exists(userDefinedPatternFolder)) throw new Exception("Pattern folder does not exist.");
            var dirInfo = new DirectoryInfo(userDefinedPatternFolder);
            var imageFiles = dirInfo.GetFilesByExtensions(allowedImageExtensions);
            return imageFiles.Select(x => new Image<Gray, float>(x.FullName)).ToList();
        }

        internal static List<Image<Gray, float>> GeneratePhaseShiftingPattern(PhaseShiftingParameters parameters, int width, int height, bool hasHomogenizationImage)
        {
            var pattern = new List<Image<Gray, float>>
            {
                //White and black images
                new Image<Gray, float>(width, height, new Gray(255)),
                new Image<Gray, float>(width, height, new Gray(0)),
            };

            var primaryPeriodWidth = (double)width / parameters.NPeriods;

            for (int step = 0; step < parameters.PrimarySteps; step++) { 
                var startPhase = 2.0 * Math.PI / parameters.PrimarySteps * step;
                pattern.Add(GeneratePhaseImage(width, height, startPhase, primaryPeriodWidth, step, parameters.PhaseMultiplesCount));
            }

            var cuePeriodWidth = (double)width / (parameters.NPeriods + 1);
            for (int step = 0; step < parameters.CueSteps; step++)
            {
                var phase = 2.0 * Math.PI / parameters.CueSteps * step;
                pattern.Add(GeneratePhaseImage(width, height, phase, cuePeriodWidth, step));
            }

            return pattern;
        }

        internal static List<Image<Gray, float>> GetPatternFromPatternType(PatternType patternType, int width, int height, string userDefinedPatternFolder, bool isRotated45, Image<Gray, float> homogenizationImage, PhaseShiftingParameters phaseShiftingParameters = null)
        {
            switch (patternType)
            {
                case PatternType.WhiteOutput:
                    return new List<Image<Gray, float>>
                    {
                        new Image<Gray, float>(width, height, new Gray(byte.MaxValue))
                    };
                case PatternType.LoadFromFolder:
                    return PatternFromFolder(userDefinedPatternFolder);
                case PatternType.GrayCodeHorizontal:
                case PatternType.GrayCodeVertical:
                    return GenerateGrayEncodingPattern(width, height, patternType, isRotated45, minThickness: 4, hasHomogenizationImage: homogenizationImage != null);
                case PatternType.PhaseShifting:
                    return GeneratePhaseShiftingPattern(phaseShiftingParameters, width, height, hasHomogenizationImage: homogenizationImage != null);
                default:
                    throw new NotImplementedException($"Support for pattern type: '{patternType}' not implemented.");
            }
        }

        private static Image<Gray, float> GeneratePhaseImage(int width, int height, double startPhase, double periodWidth, int step, int phaseMultiplesCount = 0)
        {
            var image = new Image<Gray, float>(width, height, new Gray(0));
            for (int x = 0; x < width; x++) {
                float amp = (float)(0.5 * byte.MaxValue * (1 + Math.Cos(2 * Math.PI * x / periodWidth - startPhase)));
                if (step > 0)
                {
                    for (int phaseMultiple = 2; phaseMultiple < 2 + phaseMultiplesCount; phaseMultiple++)
                    {
                        //var phaseMultiplier = phaseMultiple % 2 == 0 ? -phaseMultiple : phaseMultiple;
                        amp += (float)(0.5 * byte.MaxValue * (1 + Math.Cos(2 * Math.PI * x / periodWidth - startPhase * phaseMultiple)));
                    }
                }
                if(phaseMultiplesCount > 1)
                {
                    amp /= phaseMultiplesCount;
                }
                for (int y = 0; y < height; y++)
                {
                    image.Data[y, x, 0] = amp;
                }
            }
            return image;
        }
    }
}
