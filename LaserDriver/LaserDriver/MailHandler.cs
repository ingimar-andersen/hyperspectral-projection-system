﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LaserDriver
{
    public class MailHandler
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static async Task<bool> TrySend(string subject, string body, string to, string cc = "", string bcc = "", string[] attachments = null, int retryCount = 5)
        {
            return await Task.Run(() =>
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("noreply.hsps@gmail.com");
                if (!string.IsNullOrWhiteSpace(to))
                {
                    mail.To.Add(to);
                }
                if (!string.IsNullOrWhiteSpace(cc))
                {
                    mail.CC.Add(cc);
                }
                if (!string.IsNullOrWhiteSpace(bcc))
                {
                    mail.Bcc.Add(bcc);
                }
                if (attachments != null)
                {
                    foreach (var attachmentPath in attachments)
                    {
                        System.Net.Mail.Attachment attachment;
                        attachment = new System.Net.Mail.Attachment(attachmentPath);
                        mail.Attachments.Add(attachment);
                    }
                }
                mail.IsBodyHtml = true;
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("noreply.hsps@gmail.com", "osbismpssszasqwz");
                client.Host = "smtp.gmail.com";
                mail.Subject = subject;
                mail.Body = body;
                for (int i = 0; i < retryCount; i++)
                {
                    try
                    {
                        client.Send(mail);
                        //Success
                        return true;
                    }
                    catch (SmtpException ex)
                    {
                        log.Warn($"Mail could not be sent due to SMTP exception. Retrying in 10 sec", ex);
                        Thread.Sleep(10000);
                    }
                    catch (Exception ex)
                    {
                        log.Warn($"Mail could not be sent. Retrying in 10 sek", ex);
                        Thread.Sleep(10000);
                    }
                }
                //Failed
                return false;
            });
        }
    }
}
