﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace LaserDriver
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static Window SplashScreenWindow;
        private static FileStream fs;
        private static StreamWriter sw;

        private static Mutex _mutex = null;

        protected override void OnStartup(StartupEventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            log.Info("        =============  Started Logging  =============        ");
            const string appName = "HspsControl";
            bool createdNew;

            _mutex = new Mutex(true, appName, out createdNew);
            var isRestarting = e.Args.Contains(StartupArguments.Restarting);
            if (!createdNew && !isRestarting)
            {
                MessageBox.Show("Another instance is already running!", "", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                log.Error("Another instance running. Exiting application.");
                //app is already running! Exiting the application  
                Application.Current.Shutdown();
                return;
            }
            base.OnStartup(e);
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            SplashScreenWindow = new SplashScreenWindow();
            SplashScreenWindow.Show();
        }

        private void App_Exit(object sender, ExitEventArgs e)
        {
            sw?.Dispose();
            fs?.Dispose();
        }

        public static class StartupArguments
        {
            public const string Restarting = "\restarting";
        }
    }
}
