﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace LaserDriver
{
    public static class Utils
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool TryOpenFolder(string folderPath)
        {
            try
            {
                Process.Start(new ProcessStartInfo()
                {
                    FileName = folderPath,
                    UseShellExecute = true,
                    Verb = "open"
                });
                return true;
            }
            catch(Exception ex)
            {
                log.Error("Unable to open folder.", ex);
                return false;
            }
        }

        internal static string CheckerboardSizeToString(System.Drawing.Size gridSize)
        {
            return $"{gridSize.Width}, {gridSize.Height}";
        }

        public static async Task WaitAsync(int waitTimeMs)
        {
            await Task.Run(() => Thread.Sleep(waitTimeMs));
        }

        public static void PlaySuccessSound()
        {
            try
            {
                using (var sp = new SoundPlayer(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Media", "SuperMarioLevelComplete.wav")))
                {
                    sp.Play();
                }
            }
            catch (Exception ex)
            {
                log.Error($"Could not play sound.", ex);
            }
        }

        public static void PlayCameraFocusSound()
        {
            try
            {
                using (var sp = new SoundPlayer(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Media", "camera-focus-beep.wav")))
                {
                    sp.Play();
                }
            }
            catch (Exception ex)
            {
                log.Error($"Could not play sound.", ex);
            }
        }

        public static void PlayCameraShutterSound()
        {
            try
            {
                using (var sp = new SoundPlayer(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Media", "camera-shutter-click.wav")))
                {
                    sp.Play();
                }
            }
            catch (Exception ex)
            {
                log.Error($"Could not play sound.", ex);
            }
        }

        public static bool HasWritePermission(string outputPath)
        {
            // Since this application saves images in the current folder
            // we must ensure that we have permission to write to this folder.
            // If we do not have permission, fail right away.
            FileStream fileStream;
            try
            {
                var guidString = Guid.NewGuid().ToString();
                var testFilePath = Path.Combine(outputPath, $"{guidString}.txt");
                fileStream = new FileStream(testFilePath, FileMode.Create);
                fileStream.Close();
                File.Delete(testFilePath);
                return true;
            }
            catch (Exception ex)
            {
                log.Error("Failed to create file in current folder. Please check permissions.", ex);
                return false;
            }
        }

        public static void OpenPointCloudsInMeshlab(string[] pointCloudPath)
        {
            string pathArgument = "";
            foreach (var path in pointCloudPath)
            {
                pathArgument = $"\"{path}\" ";
            }
            pathArgument.TrimEnd();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = @"C:\Program Files\VCG\MeshLab\meshlab.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = pathArgument;

            try
            {
                Process.Start(startInfo);
            }
            catch(Exception ex)
            {
                log.Error("Error starting Meshlab instance.", ex);
            }
        }

        public static void RestartApplication()
        {
            Process.Start(Application.ResourceAssembly.Location, App.StartupArguments.Restarting);
            Application.Current.Shutdown();
        }

        public static string TimeMsToTimeSpanString(double timeMs)
        {
            try
            {
                var timespan = TimeSpan.FromMilliseconds(timeMs);
                if (timespan.Days > 0)
                {
                    return timespan.ToString("dd':'hh':'mm':'ss") + " (dd:hh:mm:ss)";
                }
                if (timespan.Hours > 0)
                {
                    return timespan.ToString("hh':'mm':'ss") + " (hh:mm:ss)";
                }
                return timespan.ToString("mm':'ss") + " (mm:ss)";
            }
            catch
            {
                return "error";
            }
        }
    }
}
